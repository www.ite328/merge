import "antd/dist/antd.css";
import "../styles/globals.css";
import "../styles/style.scss";
import "leaflet/dist/leaflet.css";
import "suneditor/dist/css/suneditor.min.css";
import "@fortawesome/fontawesome-free/js/fontawesome";
import "@fortawesome/fontawesome-free/js/solid";
import "@fortawesome/fontawesome-free/js/regular";
import "@fortawesome/fontawesome-free/js/brands";
import "react-phone-input-2/lib/style.css";
import type { AppProps } from "next/app";
import NextRouter from "next/router";
import { useRouter } from "next/router";
import { QueryClient, QueryClientProvider } from "react-query";
import "react-leaflet-search/lib/react-leaflet-search.css";
import { useEffect, useLayoutEffect, useState } from "react";
import React from "react";

export let Context = React.createContext({
  isLogin: false,
  setLoginHandler: (login: boolean) => {},
});

function MyApp({ Component, pageProps }: any) {
  const [login, setLoginHandler] = useState<boolean>(false);

  const setLoginHandlers = (login: boolean) => {
    console.log(login, "login");
    setLoginHandler(login);
  };

  const queryclient = new QueryClient();
  useEffect(() => {
    console.log("jhsj");
    if (
      localStorage.getItem("MERGE_ADMIN") == null ||
      localStorage.getItem("MERGE_ADMIN") == undefined ||
      localStorage.getItem("MERGE_ADMIN") == ""
    ) {
      NextRouter.push("/login");
      setLoginHandlers(false);
    } else {
      setLoginHandlers(true);
    }
  }, []);

  const route = useRouter();

  return (
    <QueryClientProvider client={queryclient}>
      <Context.Provider
        value={{ isLogin: login, setLoginHandler: setLoginHandlers }}
      >
        <Component {...pageProps} />
      </Context.Provider>
    </QueryClientProvider>
  );
}
export default MyApp;
