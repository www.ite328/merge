import Sidebare from "../src/components/Sidebar";
import Index from "../src/components/home";
import SharedComponents from "../src/components/sharedTopHeader";
import { Row, Col } from "antd";
import Content from "../src/components/sharedTopHeader/content";
import { UserOutlined } from "@ant-design/icons";
import GlobalStaticties from "../src/components/home/globalStaticties";
import ClientTable from "../src/components/GenericTable/clientTable";
import { Context } from "./_app";
import { useContext, useState } from "react";
const Users = () => {
  const context = useContext(Context);
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };
  return (
    <Row style={{ width: "100%" }}>
      {context.isLogin ? (
        <>
          <Col xs={24} sm={24} md={8} lg={5}>
            <Sidebare activeKey={4}></Sidebare>
          </Col>
          <Col xs={24} sm={24} md={24} lg={19}>
            <SharedComponents activeKey={4} searchHandler={setSearchHandler}>
              <Content
                name="CLIENTS"
                content=""
                icon={
                  <UserOutlined style={{ color: "white", fontSize: "20px" }} />
                }
              ></Content>

              <Row style={{ visibility: "hidden" }}>
                <GlobalStaticties></GlobalStaticties>
              </Row>
            </SharedComponents>
            <Row
              justify="center"
              style={{
                position: "absolute",
                top: 130,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <ClientTable search={search}></ClientTable>
            </Row>
          </Col>
        </>
      ) : (
        <></>
      )}
    </Row>
  );
};

export default Users;
