import Sidebare from "../src/components/Sidebar";
import Index from "../src/components/home";
import SharedComponents from "../src/components/sharedTopHeader";
import Content from "../src/components/sharedTopHeader/content";
import { NotificationOutlined } from "@ant-design/icons";
import GlobalStaticties from "../src/components/home/globalStaticties";
import PostForms from "../src/components/Forms/PostForm";
import { Row, Col } from "antd";
import { UserAddOutlined } from "@ant-design/icons";
import Notifications from "../src/components/Forms/Partner";
import { useContext, useState } from "react";
import { Context } from "./_app";

const AddPartner = () => {
  const context = useContext(Context);
  const [search, searchHandler] = useState<any>("");
  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };
  return (
    <Row style={{ width: "100%" }}>
      {context.isLogin ? (
        <>
          <Col
            xs={24}
            sm={24}
            md={8}
            lg={5}
            style={{ display: "flex", flexGrow: 4 }}
          >
            <Sidebare activeKey={2}></Sidebare>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={19}
            style={{ position: "relative", flexShrink: 4 }}
          >
            <SharedComponents activeKey={2} searchHandler={setSearchHandler}>
              <Content
                name="ADD PARTNER"
                content="Default"
                icon={
                  <UserAddOutlined
                    style={{ color: "white", fontSize: "20px" }}
                  />
                }
              ></Content>
              <Row style={{ visibility: "hidden" }}>
                <GlobalStaticties></GlobalStaticties>
              </Row>
            </SharedComponents>

            <Row
              justify="center"
              style={{
                position: "absolute",
                top: 130,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <Notifications />
            </Row>
          </Col>
        </>
      ) : (
        <> </>
      )}
    </Row>
  );
};

export default AddPartner;
