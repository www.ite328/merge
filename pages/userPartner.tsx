import Sidebare from "../src/components/Sidebar";
import Index from "../src/components/home";
import SharedComponents from "../src/components/sharedTopHeader";
import { Row, Col } from "antd";
import Content from "../src/components/sharedTopHeader/content";
import { UserOutlined } from "@ant-design/icons";
import GlobalStaticties from "../src/components/home/globalStaticties";
import UserTable from "../src/components/GenericTable/UserPartnerTable";
import { useState } from "react";
const Users = () => {
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };
  return (
    <Row style={{ width: "100%" }}>
      <Col xs={24} sm={24} md={8} lg={5}>
        <Sidebare activeKey={4}></Sidebare>
      </Col>
      <Col xs={24} sm={24} md={24} lg={19}>
        <SharedComponents activeKey={4} searchHandler={setSearchHandler}>
          <Content
            name="PARTNER USERS"
            content=""
            icon={<UserOutlined style={{ color: "white", fontSize: "20px" }} />}
          ></Content>

          <Row style={{ visibility: "hidden" }}>
            <GlobalStaticties></GlobalStaticties>
          </Row>
        </SharedComponents>
        <Row
          justify="center"
          style={{
            position: "absolute",
            top: 130,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          <UserTable search={search}></UserTable>
        </Row>
      </Col>
    </Row>
  );
};

export default Users;
