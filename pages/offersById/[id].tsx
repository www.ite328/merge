import Sidebare from "../../src/components/Sidebar";
import Index from "../../src/components/home";
import SharedComponents from "../../src/components/sharedTopHeader";
import Content from "../../src/components/sharedTopHeader/content";
import { NotificationOutlined } from "@ant-design/icons";
import GlobalStaticties from "../../src/components/home/globalStaticties";
import PostForms from "../../src/components/Forms/PostForm";
import { Row, Col } from "antd";
import { UserAddOutlined } from "@ant-design/icons";
import Notifications from "../../src/components/GenericTable/offerTable";
import { useRouter } from "next/router";
import { useState } from "react";
const AddPartner = () => {
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };
  return (
    <Row style={{ width: "100%" }}>
      <Col xs={24} sm={24} md={8} lg={5}>
        <Sidebare activeKey={2}></Sidebare>
      </Col>
      <Col xs={24} sm={24} md={24} lg={19} style={{ position: "relative" }}>
        <SharedComponents activeKey={2} searchHandler={setSearchHandler}>
          <Content
            name="PARTNER OFFERS"
            content="Default"
            icon={
              <UserAddOutlined style={{ color: "white", fontSize: "20px" }} />
            }
          ></Content>
          <Row style={{ visibility: "hidden" }}>
            <GlobalStaticties></GlobalStaticties>
          </Row>
        </SharedComponents>

        <Row
          justify="center"
          style={{
            position: "absolute",
            top: 130,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          <Notifications search={search} />
        </Row>
      </Col>
    </Row>
  );
};

export default AddPartner;
