import Sidebare from "../src/components/Sidebar";
import SharedComponents from "../src/components/sharedTopHeader";
import Content from "../src/components/sharedTopHeader/content";
import GlobalStaticties from "../src/components/home/globalStaticties";
import { Row, Col } from "antd";
import { DropboxOutlined } from "@ant-design/icons";
import TablePartner from "../src/components/GenericTable/tablePartner";
import { useContext, useEffect, useLayoutEffect, useState } from "react";
import { Context } from "./_app";
const Partner = () => {
  const context = useContext(Context);
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };

  return (
    <Row style={{ width: "100%" }}>
      {context.isLogin ? (
        <>
          <Col xs={24} sm={24} md={8} lg={5}>
            <Sidebare activeKey={2}></Sidebare>
          </Col>
          <Col xs={24} sm={24} md={24} lg={19} style={{ position: "relative" }}>
            <SharedComponents activeKey={2} searchHandler={setSearchHandler}>
              <Content
                name=" PARTNERS"
                content="Default"
                icon={
                  <DropboxOutlined
                    style={{ color: "white", fontSize: "20px" }}
                  />
                }
              ></Content>
              <Row style={{ visibility: "hidden" }}>
                <GlobalStaticties></GlobalStaticties>
              </Row>
            </SharedComponents>
            <Row
              justify="center"
              style={{
                position: "absolute",
                top: 130,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <TablePartner search={search}></TablePartner>
            </Row>
          </Col>
        </>
      ) : (
        <> </>
      )}
      ;
    </Row>
  );
};

export default Partner;
{
  /* <Row style={{ visibility: "hidden" }}>
<GlobalStaticties></GlobalStaticties>
</Row> */
}
