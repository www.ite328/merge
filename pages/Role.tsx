import Sidebare from "../src/components/Sidebar";
import Index from "../src/components/home";
import SharedComponents from "../src/components/sharedTopHeader";
import { Row, Col } from "antd";
import Content from "../src/components/sharedTopHeader/content";
import { HomeOutlined } from "@ant-design/icons";
import GlobalStaticties from "../src/components/home/globalStaticties";
import RoleTable from "../src/components/GenericTable/RoleTable";
import { useState } from "react";
const Role = () => {
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };

  return (
    <Row style={{ width: "100%" }}>
      <Col xs={24} sm={24} md={8} lg={5}>
        <Sidebare activeKey={20}></Sidebare>
      </Col>
      <Col xs={24} sm={24} md={24} lg={19}>
        <SharedComponents activeKey={20} searchHandler={setSearchHandler}>
          <Content
            name="Roles"
            content="Default"
            icon={<HomeOutlined style={{ color: "white", fontSize: "20px" }} />}
          ></Content>
          <Row style={{ visibility: "hidden" }}>
            <GlobalStaticties></GlobalStaticties>
          </Row>
        </SharedComponents>
        {/* <Index></Index> */}
        <Row
          justify="center"
          style={{
            position: "absolute",
            top: 150,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          <RoleTable search={search}></RoleTable>
        </Row>
      </Col>
    </Row>
  );
};

export default Role;
