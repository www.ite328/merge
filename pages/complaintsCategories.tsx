import Sidebare from "../src/components/Sidebar";
import Index from "../src/components/home";
import SharedComponents from "../src/components/sharedTopHeader";
import Content from "../src/components/sharedTopHeader/content";
import { PaperClipOutlined } from "@ant-design/icons";
import GlobalStaticties from "../src/components/home/globalStaticties";
import { Row, Col } from "antd";
import ComplaintsCategoriesTable from "../src/components/GenericTable/ComplaintsCategoriesTable";
import { useState } from "react";
const PostCategories = () => {
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };
  return (
    <Row style={{ width: "100%" }}>
      <Col xs={24} sm={24} md={8} lg={5}>
        <Sidebare activeKey={16}></Sidebare>
      </Col>
      <Col xs={24} sm={24} md={24} lg={19}>
        <SharedComponents activeKey={16} searchHandler={setSearchHandler}>
          <Content
            name="COMPLAINT CATEGORIES"
            content="Default"
            icon={
              <PaperClipOutlined style={{ color: "white", fontSize: "20px" }} />
            }
          ></Content>
          <Row style={{ visibility: "hidden" }}>
            <GlobalStaticties></GlobalStaticties>
          </Row>
        </SharedComponents>
        <Row
          justify="center"
          style={{
            position: "absolute",
            top: 130,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          <ComplaintsCategoriesTable
            search={search}
          ></ComplaintsCategoriesTable>
        </Row>
      </Col>
    </Row>
  );
};

export default PostCategories;
