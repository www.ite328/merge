import type { NextPage } from "next";

import HomePage from "../src/components/home";
import SidePar from "../src//components/Sidebar";
import SharedComponents from "../src/components/sharedTopHeader";
import { Row, Col } from "antd";
import { ShopOutlined } from "@ant-design/icons";
import Content from "../src/components/sharedTopHeader/content";
import GlobalStaticties from "../src/components/home/globalStaticties";
import { useEffect, useLayoutEffect, useState } from "react";
import NextRouter from "next/router";
import { Context } from "./_app";
import { useContext } from "react";
const Home: NextPage = () => {
  const context = useContext(Context);
  const [search, searchHandler] = useState<any>("");

  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };

  return (
    <Row style={{ width: "100%", display: "flex" }}>
      {context.isLogin ? (
        <>
          <Col xs={24} sm={24} md={8} lg={5} className="ParentSidebar">
            <SidePar activeKey={1}></SidePar>
          </Col>

          <Col xs={24} sm={24} md={24} lg={19} style={{ display: "block" }}>
            <SharedComponents activeKey={1} searchHandler={setSearchHandler}>
              <Content
                name="DASHBOARD"
                content="Default"
                icon={
                  <ShopOutlined style={{ color: "white", fontSize: "20px" }} />
                }
              ></Content>
              <GlobalStaticties />
            </SharedComponents>
            <HomePage></HomePage>
          </Col>
        </>
      ) : (
        <></>
      )}
    </Row>
  );
};

export default Home;
