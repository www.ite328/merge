import Sidebare from "../src/components/Sidebar";
import Index from "../src/components/home";
import SharedComponents from "../src/components/sharedTopHeader";
import Content from "../src/components/sharedTopHeader/content";
import { NotificationOutlined } from "@ant-design/icons";
import GlobalStaticties from "../src/components/home/globalStaticties";
import PostCategoriesForms from "../src/components/Forms/PostCategoriesForm";
import { Row, Col } from "antd";
import { DropboxOutlined } from "@ant-design/icons";
import { useMutation } from "react-query";
import { AddPostCategories } from "../src/react-query-services/Post-gategories";
import { PostCategories_Req } from "../src/model/post-categories";
import { useState } from "react";
const AddPost = () => {
  const [search, searchHandler] = useState<any>("");
  const setSearchHandler = (value: any) => {
    searchHandler(value);
  };
  return (
    <Row style={{ width: "100%" }}>
      <Col xs={24} sm={24} md={8} lg={5}>
        <Sidebare activeKey={8}></Sidebare>
      </Col>
      <Col xs={24} sm={24} md={24} lg={19} style={{ position: "relative" }}>
        <SharedComponents activeKey={8} searchHandler={setSearchHandler}>
          <Content
            name="ADD POST CATEGORIES"
            content="Default"
            icon={
              <DropboxOutlined style={{ color: "white", fontSize: "20px" }} />
            }
          ></Content>
          <Row style={{ visibility: "hidden" }}>
            <GlobalStaticties></GlobalStaticties>
          </Row>
        </SharedComponents>

        <Row
          justify="center"
          style={{
            position: "absolute",
            top: 130,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
          }}
        >
          <PostCategoriesForms />
        </Row>
      </Col>
    </Row>
  );
};

export default AddPost;
