
import axios, { AxiosInstance, AxiosRequestConfig, AxiosError } from 'axios'

export default class ApiProvider {
  private api: AxiosInstance;
  public constructor(config: any) {
    this.api = axios.create(config)
    this.api.interceptors.request.use((param: AxiosRequestConfig) => ({
      ...param,
      headers: {
        ...param.headers,
        Authorization: `Bearer ${localStorage.getItem('MERGE_ADMIN')}`
        
      }
    }))
  }

  public async request<T>(config: any): Promise<any> {
    let result: any;
    try {
      const response = await this.api.request<any>(config)
    
        return response.data
    }
    catch (error:any)
     {
         let result = error;
         console.log(error.response.data);
         return error.response.data;
    }
  
}
}

//   private handleError(
//     error: AxiosError<ApiError>
//   ): ApiError {
//     if (error.response) {
//       let type: ApiErrorType
//       switch (error.response.status) {
//         case 400:
//           type = ApiErrorType.BAD_REQUEST
//           break
//         case 401:
//           type = ApiErrorType.UNAUTHORIZED
//           eventManager.emit(EVENT_UNAUTHORIZED)
//           break
//         case 403:
//           type = ApiErrorType.FORBIDDEN
//           break
//         case 404:
//           type = ApiErrorType.NOT_FOUND
//           break
//         case 409:
//           type = ApiErrorType.CONFLICT
//           break
//         case 422:
//           type = ApiErrorType.DATA_VALIDATION_FAILED
//           break
//         case 500:
//           type = ApiErrorType.INTERNAL_SERVER_ERROR
//           break
//         default:
//           type = ApiErrorType.UNKNOWN
//           break
//       }
//       return { ...error.response.data, errorType: type }
//     } else if (error.request) {
//       // The request was made but no response was received.
//       return { errorType: ApiErrorType.CONNECTION }
//     } else {
//       return { errorType: ApiErrorType.UNKNOWN }
//     }
//   }
// }


