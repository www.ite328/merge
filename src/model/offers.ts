
  export interface Translation {
        title: string;
        description: string;
        language: string;
    }

    export interface Offer_Req {
        translations: Translation[];
        partnerId: number;
        attachment: number;
        amount: string;
        link: string;
    }



