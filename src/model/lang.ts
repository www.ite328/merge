
    export interface Translation {
        displayName: string;
        language: string;
    }

    export interface lang_req {
        translations: Translation[];
        code: string;
    }



