export interface PartnerCategories_Req {
    translations: Translation[];
    attachment:number
}


export interface Translation {
    name: string;
    language: string;
}

export interface Item {
    name: string;
    isActive: boolean;
    id: number;
}

export interface PartnerCategories_Res {
    totalCount: number;
    items: Item[];
}