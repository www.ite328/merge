    export interface Item {
        id: number;
        title: string;
        description: string;
        attachments: Attachment[];
        postCategory: PostCategory;
        creationTime: Date;
        isActive: boolean;
    }

    export interface Post_Res {
        items: Item[];
        totalCount: number;
    }

    
    export interface Attachment {
        id: number;
        url: string;
    }

    export interface PostCategory {
        id: number;
        name: string;
        isActive: boolean;
    }



 // Post Req
export interface Translation {
    title: string;
    description: string;
    language: string;
}

export interface Post_Req {
    translations: Translation[];
    postCategoryId: number;
    attachments: number[];
}



        // Update post Req
        export interface UpdatePost_Req {
            translations: TranslationUpdate[];
            postCategoryId: number;
            attachments: number[];
            id: number;
        }
    
    
            export interface TranslationUpdate {
                title: string;
                description: string;
                language: string;
            }


        export interface SwitchActivationPost {
            id:number,
            isActive:boolean
        }    
  