

    export interface Translation {
        title: string;
        description: string;
        language: string;
    }

    export interface Post_Req {
        translations: Translation[];
        postCategoryId: number;
        attachments: number[];
    }



    export interface PostCategory {
        name: string;
        isActive: boolean;
        id: number;
    }

    export interface Attachment {
        url: string;
        id: number;
    }

    export interface Post_Res {
        title: string;
        description: string;
        isActive: boolean;
        postCategory: PostCategory;
        creationTime: Date;
        attachments: Attachment[];
        id: number;
    }

   



   
 



