
    export interface Login_Req {
        userNameOrEmailAddress: string;
        password: string;
        rememberClient: boolean;
    }