export interface Translation {
    name: string;
    language: string;
}

export interface  Services_Req {
    translations: Translation[];
    partnerId: number;
    attachment: number;
    link: string;
}
