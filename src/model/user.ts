


export interface User_Req {
    userName: string;
    phoneNumber: string;
    name: string;
    userType: number;
    surname: string;
    isActive: boolean;
    attachments: number[];
    roleNames?: string[];
    dialCode?:string;
        password: string;
    cityId:number;
    gender:number
    partnerId:number | null ;
}

export interface Result {
    userName: string;
    name: string;
    surname: string;
    emailAddress: string;
    isActive: boolean;
    availabilityStatus: number;
    fullName: string;
    lastLoginTime?: any;
    creationTime: Date;
    userType: number;
    roleNames: any[];
    id: number;

}

export interface Add_User_Res{
    result: Result;
    targetUrl?: any;
    success: boolean;
    error?: any;
    unAuthorizedRequest: boolean;
    __abp: boolean;
}