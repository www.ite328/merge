export interface Translation {
    name: string;
    description: string;
    language: string;
    
}

export interface Parent_Partner_Req {
    translations: Translation[];
    partnerCategoryId: number;
    attachments: number[];
    clientLanguageId:string;
    parentId?:any;
}