
export interface Report {

        cityExport?: boolean,
        agentUserExport?: boolean,
        partnerUserExport?: boolean,
        partnersExport?: boolean,
        videoMeetingExport?: boolean
      
}