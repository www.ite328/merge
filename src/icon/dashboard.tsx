const Dashboard = () => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 26 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1.75 1.75H10.5V13H1.75V1.75ZM15.5 1.75H24.25V8H15.5V1.75ZM15.5 13H24.25V24.25H15.5V13ZM1.75 18H10.5V24.25H1.75V18Z"
        stroke="#322FB7"
        strokeWidth="2.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Dashboard;
