const Slider = () => {
  return (
    <svg
      width="17"
      height="17"
      viewBox="0 0 26 28"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M18 21.5V26.5M24.25 4H15.5H24.25ZM10.5 4H1.75H10.5ZM24.25 14H13H24.25ZM8 14H1.75H8ZM24.25 24H18H24.25ZM13 24H1.75H13ZM15.5 1.5V6.5V1.5ZM8 11.5V16.5V11.5Z"
        stroke="#FF4E58"
        strokeWidth="2.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Slider;
