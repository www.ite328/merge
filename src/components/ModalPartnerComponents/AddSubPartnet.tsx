import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import NextRouter, { useRouter } from "next/router";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { Translation } from "../../model/partner";
import { Parent_Partner_Req } from "../../model/partner";
import { AddParentPartner } from "../../react-query-services/partner";
import { getPartnerCategories } from "../../react-query-services/Partner-gategories";
import { UploadAttachment } from "../../services/Upload-file";
import { useQueryClient } from "react-query";

const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
  getAllPartnerCategories: any;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);
  const query = useQueryClient();

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const getAllPartnerCategories = useQuery(
    "Partner-Categories",
    getPartnerCategories
  );
  const Router = useRouter();
  const { id } = Router.query;

  const AddPartner = useMutation("Add-Partner", AddParentPartner, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Theb Sub partner add successfully",
      });
      setLoading(false);
      props.cancleshowModla();
      props.getAllPartnerCategories.refetch();
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 2 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);

    UploadAttachment.UploadAttachment(formData)
      .then((res: any) => {
        let body: Parent_Partner_Req = {
          attachments: [],
          partnerCategoryId: 0,
          translations: [],
          clientLanguageId: "1",
          parentId: +props.id,
        };
        let objectArabic: Translation = {
          description: e.description_ar,
          name: e.name_ar,
          language: "ar",
        };
        let objectEnglish: Translation = {
          description: e.description_en,
          name: e.name_en,
          language: "en",
        };
        body.partnerCategoryId =
          props?.getAllPartnerCategories?.data?.result?.partnerCategory?.id;
        body.translations.push(objectArabic, objectEnglish);
        body.attachments.push(res?.result?.id as number);

        AddPartner.mutateAsync(body);
      })
      .catch((res) => {});
  };

  return (
    <Row>
      <Modal
        title="ADD SUB PARTNER"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner form"
        style={{ top: "80px" }}
      >
        <Row
          style={{
            backgroundColor: "white",
            width: "100%",
            marginBottom: "30px",
            paddingBottom: 20,
            paddingTop: 30,
          }}
          className="globalFormStyle"
        >
          <Form
            style={{ width: "100%" }}
            onFinish={onFinishHandler}
            form={form}
          >
            <Row
              justify="space-between"
              style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
            >
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"name-en"}
                  name="name_en"
                  style={{ width: "100%" }}
                  labelCol={{ span: 24 }}
                  rules={[
                    {
                      required: true,
                      message: "the name-en Required ",
                    },
                  ]}
                >
                  <Input placeholder="name-en"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"name-ar"}
                  name="name_ar"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the name-ar Required ",
                    },
                  ]}
                >
                  <Input placeholder="name-ar"></Input>
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Description-ar"}
                  name="description_ar"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the Description-ar  Required ",
                    },
                  ]}
                >
                  <TextArea placeholder="Description-ar"></TextArea>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Description-en"}
                  name="description_en"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the Description-en  Required ",
                    },
                  ]}
                >
                  <TextArea placeholder="Description-en"></TextArea>
                </Form.Item>
              </Col>
              <div style={{ width: "100%", position: "relative" }}></div>

              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Partner Image "}
                  name="image"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the partner imageId Required ",
                    },
                  ]}
                >
                  <Upload
                    listType="picture"
                    className="upload-list-inline"
                    accept="image/png, image/jpeg , image/jpeg"
                    multiple={false}
                  >
                    <Button
                      className="button-upload-image-partner"
                      icon={<PlusOutlined style={{ fontSize: 20 }} />}
                    ></Button>
                  </Upload>
                </Form.Item>
              </Col>
              <Col
                xs={24}
                sm={24}
                md={24}
                lg={24}
                style={{
                  width: "100%",
                  justifyContent: "end",
                  display: "flex",
                }}
              >
                <Form.Item style={{ width: "100", justifyContent: "end" }}>
                  <Button
                    style={{
                      padding: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: "white",
                      borderRadius: "4px",
                      border: "none",
                      backgroundColor: "#9fa1a1",
                      position: "relative",
                    }}
                    htmlType={"submit"}
                    className="add-button"
                    loading={loading}
                  >
                    Add Sub Partner
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
