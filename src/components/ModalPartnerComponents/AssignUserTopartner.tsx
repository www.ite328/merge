import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import NextRouter, { useRouter } from "next/router";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { Translation } from "../../model/partner";
import { Parent_Partner_Req } from "../../model/partner";
import { AddParentPartner } from "../../react-query-services/partner";
import { getPartnerWithoutPatination } from "../../react-query-services/partner";
import { UploadAttachment } from "../../services/Upload-file";
import { useQueryClient } from "react-query";
import { categoryService } from "../../services/Partner";

const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
  getPartnerwithPatination: any;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);
  const query = useQueryClient();

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const getAllPartner = useQuery(
    "Partner-withoutpagenation",
    getPartnerWithoutPatination
  );
  const Router = useRouter();
  const { id } = Router.query;

  const AddPartner = useMutation("Add-Partner", AddParentPartner, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Theb Sub partner add successfully",
      });
      setLoading(false);
      props.cancleshowModla();
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    categoryService
      .AssignUsertoPartner(props.id, e.partner)
      .then((res: any) => {
        if (res?.error != null) {
          notification.open({
            message: res?.error?.message + "",
          });
          setLoading(false);
          form.resetFields();
        }
        if (res?.result != null) {
          notification.open({ message: "The User Assgine to Partner" });
          props.cancleshowModla();
          setLoading(false);
          form.resetFields();
          props.getPartnerwithPatination.refetch();
        }
      });
  };

  return (
    <Row>
      <Modal
        title="Assgin User To Partner"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"75%"}
        className="madal-partner "
        style={{ top: "170px" }}
      >
        <Row
          style={{
            backgroundColor: "white",
            width: "100%",
            marginBottom: "30px",
            paddingBottom: 20,
            paddingTop: 30,
          }}
          className="globalFormStyle"
        >
          <Form
            style={{ width: "100%" }}
            onFinish={onFinishHandler}
            form={form}
          >
            <Row
              justify="space-between"
              style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
            >
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"partner"}
                  name="partner"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the partner  Required ",
                    },
                  ]}
                >
                  <Select
                    showSearch={true}
                    optionFilterProp="children"
                    filterOption={(input, option: any) =>
                      option!.children.includes(input)
                    }
                    filterSort={(optionA: any, optionB: any) =>
                      optionA!.children
                        .toLowerCase()
                        .localeCompare(optionB!.children.toLowerCase())
                    }
                  >
                    {getAllPartner?.data?.items?.map((ele: any) => {
                      return (
                        <Option value={ele?.id} key={ele?.id}>
                          {ele?.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col
                xs={24}
                sm={24}
                md={24}
                lg={24}
                style={{
                  width: "100%",
                  justifyContent: "end",
                  display: "flex",
                }}
              >
                <Form.Item style={{ width: "100", justifyContent: "end" }}>
                  <Button
                    style={{
                      padding: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: "white",
                      borderRadius: "4px",
                      border: "none",
                      backgroundColor: "#9fa1a1",
                      position: "relative",
                    }}
                    htmlType={"submit"}
                    className="add-button"
                    loading={loading}
                  >
                    Assgin To Partner
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
