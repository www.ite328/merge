import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddPost } from "../../react-query-services/offers";
import NextRouter from "next/router";
import { UploadAttachment } from "../../services/Upload-file";
//nfjkdnkjfd
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const AddPartner = useMutation("Add-Offers", AddPost, {
    onSuccess: (res: any) => {
      if (res.result != null) {
        form.resetFields();
        notification.open({
          message: "The  Offers add successfully",
        });
        setLoading(false);
        props.cancleshowModla();
        NextRouter.push(`/offersById/${props.id}`);
      } else if (res?.error != null) {
        notification.open({
          message: res.error.message + "",
        });
        setLoading(false);
      }
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 4 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);

    UploadAttachment.UploadAttachment(formData)
      .then((res: any) => {
        let body: Offer_Req = {
          attachment: 0,
          translations: [],
          link: e.attachment,
          partnerId: props?.id,
          amount: e?.amount,
        };
        let objectArabic: Translation = {
          language: "ar",
          description: e.description_ar,
          title: e.title_ar,
        };
        let objectEnglish: Translation = {
          language: "en",
          description: e.description_en,
          title: e.title_en,
        };
        body.translations.push(objectArabic, objectEnglish);
        body.attachment = res?.result?.id as number;
        AddPartner.mutateAsync(body);
      })
      .catch((res) => {});
  };

  return (
    <Row>
      <Modal
        title="ADD OFFERS"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner form"
        style={{ top: "70px" }}
      >
        <Row
          style={{
            backgroundColor: "white",
            width: "100%",
            marginBottom: "30px",
            paddingBottom: 20,
            paddingTop: 30,
          }}
          className="globalFormStyle"
        >
          <Form
            style={{ width: "100%" }}
            onFinish={onFinishHandler}
            form={form}
          >
            <Row
              justify="space-between"
              style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
            >
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"title-en"}
                  name="title_en"
                  style={{ width: "100%" }}
                  labelCol={{ span: 24 }}
                  rules={[
                    {
                      required: true,
                      message: "the title-en Required ",
                    },
                  ]}
                >
                  <Input placeholder="title-en"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"title-ar"}
                  name="title_ar"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the title-ar Required ",
                    },
                  ]}
                >
                  <Input placeholder="title-ar"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Description-ar"}
                  name="description_ar"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the Description-ar  Required ",
                    },
                  ]}
                >
                  <TextArea placeholder="Description-ar"></TextArea>
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Description-en"}
                  name="description_en"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the Description-en  Required ",
                    },
                  ]}
                >
                  <TextArea placeholder="Description-en"></TextArea>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"amount"}
                  name="amount"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the amount  Required ",
                    },
                  ]}
                >
                  <Input type="number" placeholder="amount"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Link"}
                  name="attachment"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the attachment Required ",
                    },
                  ]}
                >
                  <Input placeholder="attachment"></Input>
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Offer Image "}
                  name="image"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the Services imageId Required ",
                    },
                  ]}
                >
                  <Upload
                    listType="picture"
                    className="upload-list-inline"
                    accept="image/png, image/jpeg , image/jpeg"
                  >
                    <Button
                      className="button-upload-image-partner"
                      icon={<PlusOutlined style={{ fontSize: 20 }} />}
                    ></Button>
                  </Upload>
                </Form.Item>
              </Col>
              <Col
                xs={24}
                sm={24}
                md={24}
                lg={24}
                style={{
                  width: "100%",
                  justifyContent: "end",
                  display: "flex",
                }}
              >
                <Form.Item style={{ width: "100", justifyContent: "end" }}>
                  <Button
                    style={{
                      padding: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: "white",
                      borderRadius: "4px",
                      border: "none",
                      backgroundColor: "#9fa1a1 ",
                      position: "relative",
                    }}
                    htmlType={"submit"}
                    className="add-button"
                    loading={loading}
                  >
                    Add Offer
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
