import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import NextRouter from "next/router";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { Translation } from "../../model/Services";
import { Services_Req } from "../../model/Services";
import { AddPost } from "../../react-query-services/services";

import { UploadAttachment } from "../../services/Upload-file";

const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const AddPartner = useMutation("Add-Partner", AddPost, {
    onSuccess: (res: any) => {
      if (res.result != null) {
        form.resetFields();
        notification.open({
          message: "The  Services add successfully",
        });
        setLoading(false);
        props.cancleshowModla();
        NextRouter.push(`/servicesByPartnerId/${props.id}`);
      } else if (res?.error != null) {
        notification.open({
          message: res.error.message + "",
        });
        setLoading(false);
      }
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 3 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);

    UploadAttachment.UploadAttachment(formData)
      .then((res: any) => {
        let body: Services_Req = {
          attachment: 0,
          translations: [],
          link: e.attachment,
          partnerId: props.id,
        };
        let objectArabic: Translation = {
          name: e.name_ar,
          language: "ar",
        };
        let objectEnglish: Translation = {
          name: e.name_en,
          language: "en",
        };
        body.translations.push(objectArabic, objectEnglish);
        body.attachment = res?.result?.id as number;
        AddPartner.mutateAsync(body);
      })
      .catch((res) => {});
  };

  return (
    <Row>
      <Modal
        title="ADD SERVICES"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner form"
        style={{ top: "70px" }}
      >
        <Row
          style={{
            backgroundColor: "white",
            width: "100%",
            marginBottom: "30px",
            paddingBottom: 20,
            paddingTop: 30,
          }}
          className="globalFormStyle"
        >
          <Form
            style={{ width: "100%" }}
            onFinish={onFinishHandler}
            form={form}
          >
            <Row
              justify="space-between"
              style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
            >
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"name-en"}
                  name="name_en"
                  style={{ width: "100%" }}
                  labelCol={{ span: 24 }}
                  rules={[
                    {
                      required: true,
                      message: "the name-en Required ",
                    },
                  ]}
                >
                  <Input placeholder="name-en"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"name-ar"}
                  name="name_ar"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the name-ar Required ",
                    },
                  ]}
                >
                  <Input placeholder="name-ar"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Link"}
                  name="attachment"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the attachment Required ",
                    },
                  ]}
                >
                  <Input placeholder="attachment"></Input>
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  label={"Services Image "}
                  name="image"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the Services imageId Required ",
                    },
                  ]}
                >
                  <Upload
                    listType="picture"
                    className="upload-list-inline"
                    accept="image/png, image/jpeg , image/jpeg"
                  >
                    <Button
                      className="button-upload-image-partner"
                      icon={<PlusOutlined style={{ fontSize: 20 }} />}
                    ></Button>
                  </Upload>
                </Form.Item>
              </Col>
              <Col
                xs={24}
                sm={24}
                md={24}
                lg={24}
                style={{
                  width: "100%",
                  justifyContent: "end",
                  display: "flex",
                }}
              >
                <Form.Item style={{ width: "100", justifyContent: "end" }}>
                  <Button
                    style={{
                      padding: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: "white",
                      borderRadius: "4px",
                      border: "none",
                      backgroundColor: "#9fa1a1",
                      position: "relative",
                    }}
                    htmlType={"submit"}
                    className="add-button"
                    loading={loading}
                  >
                    Add Sub Services
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
