import { LatLngBoundsExpression } from "leaflet";
import React, { Component } from "react";
import { MapContainer, Popup, TileLayer, Marker } from "react-leaflet";
import { EnvironmentOutlined } from "@ant-design/icons";
import { ReactLeafletSearch } from 'react-leaflet-search'
import L from "leaflet";
// class CustomOpenStreetMap {
//   bounds: LatLngBoundsExpression | undefined | any;
//   url: any;
//   constructor(options = { providerKey: null, searchBounds: [], origin }) {
//     let { searchBounds } = options;
//     //Bounds are expected to be a nested array of [[sw_lat, sw_lng],[ne_lat, ne_lng]].
//     // We convert them into a string of 'x1,y1,x2,y2' which is the opposite way around from lat/lng - it's lng/lat
//     let boundsUrlComponent = "";
//     let regionUrlComponent = "";
//     if (searchBounds.length) {
//       const reversed = searchBounds.map((el: any) => {
//         return el.reverse();
//       });
//       this.bounds = [].concat([], ...reversed).join(",");
//       boundsUrlComponent = `&bounded=1&viewbox=${this.bounds}`;
//     }
//     if ("region" in options) {
//       regionUrlComponent = `&countrycodes=${options.origin}`;
//     }
//     this.url = `https://nominatim.openstreetmap.org/search?format=json&addressdetails=1&polygon_svg=1&namedetails=1${boundsUrlComponent}${regionUrlComponent}&q=`;
//   }

//   async search(query: any) {
//     // console.log(this.url + query)
//     const response = await fetch(this.url + query).then((res) => res.json());
//     return this.formatResponse(response);
//   }

//   formatResponse(response: any) {
//     const resources = response;
//     const count = response.length;
//     const info =
//       count > 0
//         ? resources.map((e: any) => ({
//             bounds: e.boundingbox.map((bound: any) => Number(bound)),
//             latitude: 0,
//             longitude: 0,
//             name: "deneme 1",
//           }))
//         : "Not Found";
//     return {
//       info: info,
//       raw: response,
//     };
//   }
// }

const SimpleMap = () => {
  let position = { lat: 33.504516, lng: 36.260741 };
  const Markers = L.icon({
    iconUrl: "/assets/free.png",
    iconSize: [38, 38],
  });
  return (
    <MapContainer
      center={{ lat: 33.504516, lng: 36.260741 }}
      zoom={5}
      scrollWheelZoom={false}
      style={{ width: "100%", height: "100%" }}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={position} icon={Markers}></Marker>
    </MapContainer>
  );
};
export default SimpleMap;
