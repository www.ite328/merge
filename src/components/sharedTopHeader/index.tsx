import { Row, Col, Input, Image, Dropdown, Menu, Button } from "antd";
import { MinusOutlined, BarsOutlined } from "@ant-design/icons";
import { useEffect, useLayoutEffect, useState } from "react";
import Drowers from "../Drower/index";
import NextRouter, { useRouter } from "next/router";
import Link from "next/link";
import { useContext } from "react";
import { Context } from "../../../pages/_app";
const SharedComponents: React.FC<{
  activeKey: number;
  searchHandler: (value: any) => void;
}> = (props) => {
  const { Search } = Input;
  const router = useRouter();
  const [visible, setVisible] = useState<boolean>(false);
  const [name, setName] = useState<any>("");
  const context = useContext(Context);
  console.log(context, "context");
  const changeVisibaleHandler = () => {
    setVisible((prev) => !prev);
  };

  console.log(router.pathname, "router.pathname");

  const LogoutHandler = () => {
    localStorage.setItem("MERGE_ADMIN", "");

    context.setLoginHandler(false);

    NextRouter.push("/login");
  };
  useEffect(() => {
    console.log("jhsj");
    if (
      localStorage.getItem("MERGE_ADMIN") == null ||
      localStorage.getItem("MERGE_ADMIN") == undefined ||
      localStorage.getItem("MERGE_ADMIN") == ""
    ) {
      NextRouter.replace("/login");
    } else {
    }
    setName(localStorage.getItem("NAME"));
  });

  const onChangeHandler = (e: any) => {
    props.searchHandler(e?.target?.value);
  };

  const menu = (
    <Menu
      items={[
        {
          key: "2",
          label: (
            <a
              rel="noopener noreferrer"
              style={{ fontFamily: "Poppins" }}
              className="icon-link"
            >
              User Profile
            </a>
          ),
          icon: (
            <i className="fa fa-user icon-setting" style={{ fontSize: 14 }}></i>
          ),

          disabled: false,
        },
       
        {
          key: "1",
          label: (
            <a
              rel="noopener noreferrer"
              style={{ fontFamily: "Poppins" }}
              className="icon-link"
              onClick={LogoutHandler}
            >
              Log out
            </a>
          ),
          icon: (
            <i
              className="fas fa-sign-out-alt icon-setting"
              style={{ fontSize: 14 }}
            ></i>
          ),

          disabled: false,
        },
      ]}
    ></Menu>
  );

  return (
    <div className="sharedHeader">
      <Row className="header">
        <Row
          justify="space-between"
          style={{
            width: "100%",
            padding: 20,
            display: "flex",
            alignItems: "center",
            height: 70,
            borderBottom: "1px solid rgba(90, 86, 86, 0.788)",
            flexWrap: "nowrap",
          }}
        >
          <Col
            className="Search"
            style={{ display: "flex", alignItems: "center", gap: 8 }}
          >
            <BarsOutlined
              style={{ color: "white", fontSize: 25 }}
              className={"Collapse"}
              onClick={changeVisibaleHandler}
            ></BarsOutlined>
            {!router.pathname.startsWith("/add") && (
              <Search
                className="costume-search"
                type={"search"}
                placeholder="Search"
                onChange={onChangeHandler}
              ></Search>
            )}
          </Col>
          <Col span={12} className="userSection">
            <Dropdown overlay={menu} placement="bottomRight">
              <Button
                style={{
                  backgroundColor: "transparent",
                  border: "none",
                  width: 2,
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 17, color: "white", cursor: "pointer" }}
                ></i>
              </Button>
            </Dropdown>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 8,
              }}
            >
              <Image
                src="../assets/22.jpg"
                preview={false}
                alt=""
                width={40}
                height={40}
                style={{ borderRadius: "50%" }}
              ></Image>
              <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>{name}</p>
            </div>
          </Col>
        </Row>
        <Row
          style={{
            paddingLeft: 20,
            paddingRight: 20,
            width: "100%",
          }}
        >
          {props?.children}
        </Row>
      </Row>
      <Drowers
        activeKey={props.activeKey}
        changeVisibaleHandlers={changeVisibaleHandler}
        visible={visible}
      />
    </div>
  );
};

export default SharedComponents;
