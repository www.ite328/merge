import { Row, Col, notification } from "antd";
import NextRouter from "next/router";
import { useEffect, useLayoutEffect } from "react";
const Content: React.FC<{ name: string; icon: any; content: string }> = (
  props
) => {
  return (
    <Row
      justify="space-between"
      style={{ width: "100%", alignItems: "center", height: 50, gap: 6 }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          gap: 20,
          paddingLeft: 4,
        }}
      >
        {/* <p
          style={{
            marginBottom: 0,
            color: "white",
            fontFamily: "Poppins",
            fontWeight: 400,
            fontSize: 19,
          }}
        >
          {props.content}
        </p> */}
        <div style={{ display: "flex", alignItems: "center", gap: 5 }}>
          {props.icon}
          <span style={{ color: "white" }}>-</span>
          <p
            style={{
              marginBottom: 0,
              color: "white",
              fontFamily: "Poppins",
              fontWeight: 400,
              fontSize: 14,
              marginLeft: 4,
              marginTop: 1,
            }}
          >
            {props.name}
          </p>
        </div>
      </div>
    </Row>
  );
};
export default Content;
