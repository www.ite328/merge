import { Row, Col, Form, Input, Button, Image, notification } from "antd";
import { useQuery, useMutation } from "react-query";
import Logo from "../icon/logo";
import { AddCountry } from "../../react-query-services/login";
import { useContext } from "react";
import { Context } from "../../../pages/_app";
import NextRouter from "next/router";

const Login = () => {
  const context = useContext(Context);

  const loginHandler = useMutation("Login", AddCountry, {
    onSuccess: (res: any) => {
      if (res?.error != null) {
        notification.open({ message: res.error.details + "" });
      } else {
        console.log(res?.result?.accessToken, "huhg");
        localStorage.setItem("MERGE_ADMIN", res?.result?.accessToken);
        localStorage.setItem("NAME", res?.result?.name);

        notification.open({
          message: "Welcome to Merge",
        });
        context.setLoginHandler(true);
        NextRouter.replace("/Partner");
      }
    },
  });

  const onfinishHandler = (e: any) => {
    let body: any = {};
    body.userNameOrEmailAddress = e.userNameOrEmailAddress;
    body.password = e.password;
    body.rememberClient = true;
    loginHandler.mutateAsync(body);
  };
  const [form] = Form.useForm();

  return (
    <Row
      justify="center"
      style={{
        width: "100%",
        height: "100vh",
        alignItems: "center",
        // background: "linear-gradient(to right, #8300cd, #b800c4)",
      }}
      className="login-form"
    >
      <Col xs={24} sm={24} md={12} lg={12} className="left-dev">
        <Row
          style={{ width: "100%", alignItems: "center" }}
          className="row-image"
        >
          <Col
            lg={16}
            md={22}
            sm={20}
            xs={20}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            className="left-media"
          >
            <div
              style={{
                width: "100%",
                backgroundColor: "white",
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                padding: 20,
                height: 500,
              }}
            >
              <Image
                src="/assets/13.jpg"
                alt=""
                width={"450px"}
                height="450px"
                preview={false}
                style={{ borderRadius: "0%", objectFit: "cover" }}
              ></Image>
            </div>
          </Col>
        </Row>
      </Col>
      <Col xs={24} sm={24} md={12} lg={12} className="right-dev">
        <Row
          style={{ width: "100%", alignItems: "center" }}
          className="row-form"
        >
          <Col
            lg={16}
            md={22}
            sm={20}
            xs={20}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            className="rgith-media"
          >
            <div
              style={{
                width: "100%",
                backgroundColor: "white",
                borderTopRightRadius: 8,
                borderBottomRightRadius: 8,
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
                padding: 20,
                height: 500,
              }}
            >
              {" "}
              <div
                style={{
                  width: "100%",
                }}
              >
                <Row justify="center" style={{ width: "100%", height: "60px" }}>
                  <Logo></Logo>
                </Row>
                <Row
                  style={{
                    width: "100%",
                    textAlign: "center",
                    backgroundColor: "white",
                  }}
                  className="div-form"
                >
                  <p>WELCOME TO MERGE</p>
                </Row>
                <Row
                  style={{
                    backgroundColor: "white",
                    width: "100%",
                    paddingLeft: 10,
                    paddingRight: 10,
                    marginTop: 10,
                  }}
                  className="globalForm"
                >
                  <Form
                    style={{ width: "100%" }}
                    onFinish={onfinishHandler}
                    form={form}
                  >
                    <Row
                      justify="space-between"
                      style={{
                        width: "100%",
                        paddingLeft: 20,
                        paddingRight: 20,
                      }}
                    >
                      <Col xs={24} sm={24} md={24} lg={24}>
                        <Form.Item
                          label={"Email"}
                          name="userNameOrEmailAddress"
                          style={{ width: "100%" }}
                          rules={[
                            {
                              required: true,
                              message: "the Email Required ",
                            },
                          ]}
                          labelCol={{ span: 24 }}
                        >
                          <Input placeholder="email" type="email"></Input>
                        </Form.Item>
                      </Col>
                      <Col xs={24} sm={24} md={24} lg={24}>
                        <Form.Item
                          label={"Password"}
                          name="password"
                          style={{ width: "100%" }}
                          rules={[
                            {
                              required: true,
                              message: "the password Required ",
                            },
                          ]}
                          labelCol={{ span: 24 }}
                        >
                          <Input placeholder="Password" type="password"></Input>
                        </Form.Item>
                      </Col>

                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        style={{
                          width: "100%",
                          justifyContent: "center",
                          display: "flex",
                          marginTop: 10,
                        }}
                      >
                        <Form.Item
                          style={{ width: "100", justifyContent: "center" }}
                        >
                          <Button
                            style={{
                              padding: 10,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              color: "white",
                              borderRadius: "8px",
                              border: "none",
                              background:
                                "linear-gradient(to right, #5e9fc9, #3a95aa)",
                              width: "200px",
                              height: "40px",
                            }}
                            // loading={AddPartner.isLoading}
                            htmlType="submit"
                            className="add-button"
                            loading={loginHandler.isLoading}
                          >
                            Login
                          </Button>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </Col>
      {/* <Col
        xs={24}
        sm={24}
        md={12}
        lg={12}
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <div
          style={{ width: "80%", boxShadow: "rgb(0 0 0 / 24%) 0px 3px 8px" }}
        >
          <Row
            style={{
              width: "100%",
              borderBottom: "1px solid #aaa7a7",
              backgroundColor: "white",
            }}
            className="div-form"
          >
            <p>Login to Your Account</p>
          </Row>
          <Row
            style={{
              backgroundColor: "white",
              width: "100%",
              paddingLeft: 10,
              paddingRight: 10,
              marginTop: 10,
            }}
            className="globalForm"
          >
            <Form style={{ width: "100%" }}>
              <Row
                justify="space-between"
                style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
              >
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Email"}
                    name="email"
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the Email Required ",
                      },
                    ]}
                    labelCol={{ span: 24 }}
                  >
                    <Input placeholder="email" type="email"></Input>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Password"}
                    name="namear"
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the password Required ",
                      },
                    ]}
                    labelCol={{ span: 24 }}
                  >
                    <Input placeholder="Password" type="password"></Input>
                  </Form.Item>
                </Col>

                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  style={{
                    width: "100%",
                    justifyContent: "end",
                    display: "flex",
                  }}
                >
                  <Form.Item style={{ width: "100", justifyContent: "end" }}>
                    <Button
                      style={{
                        padding: 10,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: "4px",
                        border: "none",
                        backgroundColor: "#5f9eca",
                      }}
                      // loading={AddPartner.isLoading}
                      htmlType="submit"
                      className="add-button"
                    >
                      Login
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Row>
        </div>
      </Col>
      <Col
        xs={24}
        sm={24}
        md={12}
        lg={12}
        style={{ display: "flex", justifyContent: "center" }}
      >
        <Image
          src="/assets/13.jpg"
          alt=""
          width={500}
          height={500}
          preview={false}
        ></Image>
      </Col> */}
    </Row>
  );
};

export default Login;
