import React from "react";
import { Drawer, Button, Radio, Space } from "antd";
import Sidebar from "../Sidebar/smallSidebar";
interface Placement {
  left: "left";
  right: "right";
}

//ncjkndfjkn

const Drowers: React.FC<{
  activeKey: number;
  changeVisibaleHandlers: () => void;
  visible: boolean;
}> = (props) => {
  const [visible, setVisible] = React.useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    props.changeVisibaleHandlers();
  };

  return (
    <>
      <Drawer
        title="MERGE"
        placement={"left"}
        closable={false}
        onClose={onClose}
        visible={props.visible}
        width={250}
        key={1}
      >
        <Sidebar activeKey={+props.activeKey}></Sidebar>
      </Drawer>
    </>
  );
};
export default Drowers;
