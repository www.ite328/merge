import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddPost } from "../../react-query-services/offers";
import NextRouter from "next/router";
import { getPostById } from "../../react-query-services/Post";
import { UploadAttachment } from "../../services/Upload-file";
import {
  getAllPermissionsWithoutPatination,
  getRoleById,
} from "../../react-query-services/roles";
import { UpdateRole } from "../../react-query-services/roles";
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const query = useQueryClient();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const getAllPermissionsWithoutPatinations = useQuery(
    "getAllPermissionsWithoutPatination",
    getAllPermissionsWithoutPatination
  );
  const getRolesById = useQuery(
    ["getRoleById", props.id],
    () => getRoleById(props.id),
    {
      onError: () => {},
      onSuccess: () => {},
    }
  );

  const Update_Post = useMutation("UpdatUpdateRolee", UpdateRole, {
    onSuccess: (res: any) => {
      if (res.result != null) {
        form.resetFields();
        notification.open({
          message: "The  Role Update successfully",
        });
        query.invalidateQueries("getRoleWithPatination");
        setLoading(false);
        form.resetFields();
        props.cancleshowModla();
      } else if (res?.error != null) {
        notification.open({
          message: res.error.message + "",
        });
        setLoading(false);
      }
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    console.log(e, "elemtnts");
    setLoading(true);
    e.id = props.id;
    Update_Post.mutateAsync(e);
  };

  return (
    <Row>
      <Modal
        title="Edit Role"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner"
        centered={true}
      >
        {(getRolesById.isLoading ||
          getAllPermissionsWithoutPatinations.isLoading) && (
          <Row
            style={{
              width: "100%",
              height: "434px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <i
              className="fas fa-circle-notch fa-spin"
              style={{ fontSize: 25, color: "#9fa1a1" }}
            ></i>
          </Row>
        )}
        {!getRolesById.isLoading &&
          !getAllPermissionsWithoutPatinations.isLoading && (
            <Row
              style={{
                backgroundColor: "white",
                width: "100%",
                marginBottom: "30px",
                paddingBottom: 20,
                paddingTop: 30,
              }}
              className="globalFormStyle"
            >
              <Form
                style={{ width: "100%" }}
                onFinish={onFinishHandler}
                form={form}
                initialValues={{ ...getRolesById?.data?.result?.role }}
              >
                <Row
                  justify="space-between"
                  style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
                >
                  <Col xs={24} sm={24} md={24} lg={24}>
                    <Form.Item
                      label={"Name"}
                      name="name"
                      style={{ width: "100%" }}
                      labelCol={{ span: 24 }}
                      rules={[
                        {
                          required: true,
                          message: "the name Required ",
                        },
                      ]}
                    >
                      <Input placeholder="Name"></Input>
                    </Form.Item>
                  </Col>

                  <Col xs={24} sm={24} md={24} lg={24}>
                    <Form.Item
                      label={"Display Name"}
                      name="displayName"
                      labelCol={{ span: 24 }}
                      style={{ width: "100%" }}
                      rules={[
                        {
                          required: true,
                          message: "the Display Name  Required ",
                        },
                      ]}
                    >
                      <TextArea placeholder="Display Name"></TextArea>
                    </Form.Item>
                  </Col>

                  <Col xs={24} sm={24} md={24} lg={24}>
                    <Form.Item
                      label={"Granted Permissions"}
                      name="grantedPermissions"
                      labelCol={{ span: 24 }}
                      style={{ width: "100%" }}
                      rules={[
                        {
                          required: true,
                          message: "the granted Permissions  Required ",
                        },
                      ]}
                    >
                      <Select
                        mode="multiple"
                        defaultValue={
                          getRolesById?.data?.result != undefined
                            ? [
                                ...getRolesById?.data?.result
                                  ?.grantedPermissionNames,
                              ]
                            : []
                        }
                        showSearch={true}
                        optionFilterProp="children"
                        filterOption={(input, option: any) =>
                          option!.children.includes(input)
                        }
                        filterSort={(optionA: any, optionB: any) =>
                          optionA!.children
                            .toLowerCase()
                            .localeCompare(optionB!.children.toLowerCase())
                        }
                      >
                        {getRolesById?.data?.result?.permissions?.map(
                          (ele: any) => {
                            return (
                              <Option value={ele?.name} key={ele?.id}>
                                {ele?.displayName}
                              </Option>
                            );
                          }
                        )}
                      </Select>
                    </Form.Item>
                  </Col>

                  <Col
                    xs={24}
                    sm={24}
                    md={24}
                    lg={24}
                    style={{
                      width: "100%",
                      justifyContent: "end",
                      display: "flex",
                    }}
                  >
                    <Form.Item style={{ width: "100", justifyContent: "end" }}>
                      <Button
                        style={{
                          padding: "15px 30px",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          color: "white",
                          borderRadius: "4px",
                          border: "none",
                          backgroundColor: "#b1bdbe",
                        }}
                        htmlType={"submit"}
                        className={"add-button"}
                        loading={loading}
                      >
                        Edit Role
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Row>
          )}
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
