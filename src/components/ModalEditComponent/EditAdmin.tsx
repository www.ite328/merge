import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddPost } from "../../react-query-services/offers";
import NextRouter from "next/router";
import { getUserById } from "../../react-query-services/user";
import { UploadAttachment } from "../../services/Upload-file";
import { getPartnerCategories } from "../../react-query-services/Partner-gategories";
import { UpdatePartner } from "../../react-query-services/partner";
import { getAllRolesWithoutPatination } from "../../react-query-services/roles";
import { getCountryWithoutPatination } from "../../react-query-services/cities";
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const getAllCityWithOutPatination = useQuery(
    "getCountryWithoutPatination",
    getCountryWithoutPatination,
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const getAllRolesWithoutPatinations = useQuery(
    "getAllRolesWithoutPatination",
    getAllRolesWithoutPatination,
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const { TextArea } = Input;
  const { Option } = Select;
  const query = useQueryClient();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const getAllPartnerCategories = useQuery(
    "Post-Categories",
    getPartnerCategories
  );
  const getOnePartnerById = useQuery(
    ["getUserById", props.id],
    () => getUserById(props.id),
    {
      onError: () => {},
      onSuccess: () => {},
    }
  );

  useEffect(() => {
    console.log(getOnePartnerById?.data, "getOnePartnerById?.data?.result");
  }, [getOnePartnerById?.data]);

  const Update_Post = useMutation("Update-Post", UpdatePartner, {
    onSuccess: (res: any) => {
      if (res.result != null) {
        form.resetFields();
        notification.open({
          message: "The  Admin Update successfully",
        });
        query.invalidateQueries("getAllPartnerWithPatination");
        setLoading(false);
        form.resetFields();
        props.cancleshowModla();
      } else if (res?.error != null) {
        notification.open({
          message: res.error.message + "",
        });
        setLoading(false);
      }
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    if (e.attachments[0]?.url) {
      // console.log("is past image");
      let body: any = {};
      body.translations = [
        {
          name: e.name,
          description: e.description,
          language: "en",
        },
      ];
      body.partnerCategoryId = e.partner;
      body.attachments = [e.attachments[0].id];
      body.id = props.id;
      Update_Post.mutateAsync(body);
    } else {
      let formData = new FormData();
      formData.append("RefType", 2 + " ");
      formData.append("File", e?.attachments?.fileList[0]?.originFileObj);
      UploadAttachment.UploadAttachment(formData).then((res: any) => {
        let body: any = {};
        body.translations = [
          {
            name: e.name,
            description: e.description,
            language: "en",
          },
        ];
        body.partnerCategoryId = e.partner;
        body.attachments = [res.result.id as number];
        body.id = props.id;
        Update_Post.mutateAsync(body);
      });
    }
  };
  console.log(
    getOnePartnerById?.data?.result?.partnerCategory?.name,
    "fdjkdfjknhfdk"
  );
  return (
    <Row>
      <Modal
        title="Edit Admin"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner form"
        style={{ top: "80px" }}
      >
        {(getOnePartnerById.isLoading || getAllPartnerCategories.isLoading) && (
          <Row
            style={{
              width: "100%",
              height: "434px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <i
              className="fas fa-circle-notch fa-spin"
              style={{ fontSize: 25, color: "#9fa1a1" }}
            ></i>
          </Row>
        )}
        {!getOnePartnerById.isLoading && !getAllPartnerCategories.isLoading && (
          <Row
            style={{
              backgroundColor: "white",
              width: "100%",
              marginBottom: "30px",
              paddingBottom: 20,
              paddingTop: 30,
            }}
            className="globalFormStyle"
          >
            <Form
              style={{ width: "100%" }}
              onFinish={onFinishHandler}
              form={form}
              initialValues={{ ...getOnePartnerById?.data }}
            >
              <Row
                justify="space-between"
                style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
              >
                <Col xs={24} sm={24} md={11} lg={11}>
                  <Form.Item
                    label={"name"}
                    name="name"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "The Name Required ",
                      },
                    ]}
                  >
                    <Input placeholder="Name"></Input>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={11} lg={11}>
                  <Form.Item
                    label={"sur name"}
                    name="surname"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "The surname Required ",
                      },
                    ]}
                  >
                    <Input placeholder="sur name"></Input>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={11} lg={11}>
                  <Form.Item
                    label={"user name"}
                    name="userName"
                    style={{ width: "100%" }}
                    labelCol={{ span: 24 }}
                    rules={[
                      {
                        required: true,
                        message: "the User Name Required ",
                      },
                    ]}
                  >
                    <Input placeholder="User Name"></Input>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={11} lg={11}>
                  <Form.Item
                    label={"phone number"}
                    name="phoneNumber"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "The Phone Number Required ",
                      },
                    ]}
                  >
                    <Input placeholder="Phone Number"></Input>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Gender"}
                    name="gender"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "The Gender Type Required ",
                      },
                    ]}
                  >
                    <Select>
                      <Option value={1}>{"Male"}</Option>
                      <Option value={2}>{"Femal"}</Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Role"}
                    name="roleNames"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "The Role Required",
                      },
                    ]}
                  >
                    <Select
                      showSearch={true}
                      optionFilterProp="children"
                      defaultValue={
                        getOnePartnerById?.data != undefined
                          ? [...getOnePartnerById?.data?.roleNames]
                          : []
                      }
                      filterOption={(input, option: any) =>
                        option!.children.includes(input)
                      }
                      filterSort={(optionA: any, optionB: any) =>
                        optionA!.children
                          .toLowerCase()
                          .localeCompare(optionB!.children.toLowerCase())
                      }
                      mode={"multiple"}
                    >
                      {getAllRolesWithoutPatinations?.data?.items.map(
                        (ele: any) => {
                          return (
                            <Option value={ele.name} key={ele.id}>
                              {ele.displayName}
                            </Option>
                          );
                        }
                      )}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"City"}
                    name="city"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "The City Type Required ",
                      },
                    ]}
                  >
                    <Select
                      defaultValue={
                        getOnePartnerById?.data != undefined
                          ? getOnePartnerById?.data?.city
                          : ""
                      }
                      showSearch={true}
                      optionFilterProp="children"
                      filterOption={(input, option: any) =>
                        option!.children.includes(input)
                      }
                      filterSort={(optionA: any, optionB: any) =>
                        optionA!.children
                          .toLowerCase()
                          .localeCompare(optionB!.children.toLowerCase())
                      }
                    >
                      {getAllRolesWithoutPatinations?.data?.items.map(
                        (ele: any) => {
                          return (
                            <Option value={ele.name} key={ele.id}>
                              {ele.displayName}
                            </Option>
                          );
                        }
                      )}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={" user image "}
                    name="image"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the user image  required ",
                      },
                    ]}
                  >
                    <Upload
                      listType="picture"
                      className="upload-list-inline"
                      accept="image/png, image/jpeg , image/jpeg"
                      multiple={false}
                      maxCount={1}
                      defaultFileList={getOnePartnerById?.data?.result?.attachments?.map(
                        (ele: any) => {
                          return {
                            uid: ele?.id,
                            name: "images",
                            status: "done",
                            url: ele.url,
                            thumbUrl: ele.url,
                          };
                        }
                      )}
                    >
                      <Button
                        className="button-upload-image-partner"
                        icon={<PlusOutlined style={{ fontSize: 20 }} />}
                      ></Button>
                    </Upload>
                  </Form.Item>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  style={{
                    width: "100%",
                    justifyContent: "end",
                    display: "flex",
                  }}
                >
                  <Form.Item style={{ width: "100", justifyContent: "end" }}>
                    <Button
                      style={{
                        padding: "15px 30px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: "4px",
                        border: "none",
                        backgroundColor: "#b1bdbe",
                      }}
                      htmlType={"submit"}
                      className={"add-button"}
                      loading={loading}
                    >
                      Edit Admin
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Row>
        )}
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
