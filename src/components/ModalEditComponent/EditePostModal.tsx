import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddPost } from "../../react-query-services/offers";
import NextRouter from "next/router";
import { getPostById } from "../../react-query-services/Post";
import { UploadAttachment } from "../../services/Upload-file";
import { getPostCategoriesWithOutPatination } from "../../react-query-services/Post-gategories";
import { UpdatePost } from "../../react-query-services/Post";
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const query = useQueryClient();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const getAllPostCategories = useQuery(
    "Post-Categories",
    getPostCategoriesWithOutPatination
  );
  const getOnePostById = useQuery(
    ["PostById", props.id],
    () => getPostById(props.id),
    {
      onError: () => {},
      onSuccess: () => {},
    }
  );

  const Update_Post = useMutation("Update-Post", UpdatePost, {
    onSuccess: (res: any) => {
      if (res.result != null) {
        form.resetFields();
        notification.open({
          message: "The  Post Update successfully",
        });
        query.invalidateQueries("getAllPostCategoriesWithPatination");
        setLoading(false);
        form.resetFields();
        props.cancleshowModla();
      } else if (res?.error != null) {
        notification.open({
          message: res.error.message + "",
        });
        setLoading(false);
      }
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    if (e.attachments[0]?.url) {
      // console.log("is past image");
      let body: any = {};
      body.translations = [
        {
          title: e.title,
          description: e.description,
          language: "en",
        },
      ];
      body.postCategoryId = e.post;
      body.attachments = [e.attachments[0].id];
      body.id = props.id;
      Update_Post.mutateAsync(body);
    } else {
      let formData = new FormData();
      formData.append("RefType", 1 + " ");
      formData.append("File", e?.attachments?.fileList[0]?.originFileObj);
      UploadAttachment.UploadAttachment(formData).then((res: any) => {
        let body: any = {};
        body.translations = [
          {
            title: e.title,
            description: e.description,
            language: "en",
          },
        ];
        body.postCategoryId = e.post;
        body.attachments = [res.result.id as number];
        body.id = props.id;
        Update_Post.mutateAsync(body);
      });
    }
  };
  console.log(getOnePostById?.data?.result?.postCategory?.id, "fdjkdfjknhfdk");
  return (
    <Row>
      <Modal
        title="Edit Post"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner form"
        style={{ top: "80px" }}
      >
        {(getOnePostById.isLoading || getAllPostCategories.isLoading) && (
          <Row
            style={{
              width: "100%",
              height: "434px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <i
              className="fas fa-circle-notch fa-spin"
              style={{ fontSize: 25, color: "#9fa1a1" }}
            ></i>
          </Row>
        )}
        {!getOnePostById.isLoading && !getAllPostCategories.isLoading && (
          <Row
            style={{
              backgroundColor: "white",
              width: "100%",
              marginBottom: "30px",
              paddingBottom: 20,
              paddingTop: 30,
            }}
            className="globalFormStyle"
          >
            <Form
              style={{ width: "100%" }}
              onFinish={onFinishHandler}
              form={form}
              initialValues={{ ...getOnePostById?.data?.result }}
            >
              <Row
                justify="space-between"
                style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
              >
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Title"}
                    name="title"
                    style={{ width: "100%" }}
                    labelCol={{ span: 24 }}
                    rules={[
                      {
                        required: true,
                        message: "the title Required ",
                      },
                    ]}
                  >
                    <Input placeholder="Title"></Input>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Description"}
                    name="description"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the Description  Required ",
                      },
                    ]}
                  >
                    <TextArea placeholder="Description"></TextArea>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Post Category"}
                    name="post"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the PostCategory  Required ",
                      },
                    ]}
                  >
                    <Select
                      defaultValue={[
                        getOnePostById?.data?.result?.postCategory?.name + "",
                      ]}
                      showSearch={true}
                      optionFilterProp="children"
                      filterOption={(input, option: any) =>
                        option!.children.includes(input)
                      }
                      filterSort={(optionA: any, optionB: any) =>
                        optionA!.children
                          .toLowerCase()
                          .localeCompare(optionB!.children.toLowerCase())
                      }
                    >
                      {getAllPostCategories?.data?.map((ele: any) => {
                        return (
                          <Option value={ele?.id} key={ele?.id}>
                            {ele?.name}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Post Image "}
                    name="attachments"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the Image Post  Required ",
                      },
                    ]}
                  >
                    <Upload
                      listType="picture"
                      className="upload-list-inline"
                      accept="image/png, image/jpeg , image/jpeg"
                      multiple={false}
                      maxCount={1}
                      defaultFileList={getOnePostById?.data?.result?.attachments?.map(
                        (ele: any) => {
                          return {
                            uid: ele?.id,
                            name: "images",
                            status: "done",
                            url: ele.url,
                            thumbUrl: ele.url,
                          };
                        }
                      )}
                    >
                      <Button
                        className="button-upload-image-partner"
                        icon={<PlusOutlined style={{ fontSize: 20 }} />}
                      >
                      
                      </Button>
                    </Upload>
                  </Form.Item>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  style={{
                    width: "100%",
                    justifyContent: "end",
                    display: "flex",
                  }}
                >
                  <Form.Item style={{ width: "100", justifyContent: "end" }}>
                    <Button
                      style={{
                        padding: "15px 30px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: "4px",
                        border: "none",
                        backgroundColor: "#b1bdbe",
                      }}
                      htmlType={"submit"}
                      className={"add-button"}
                      loading={loading}
                    >
                      Edit Post
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Row>
        )}
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
