import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddPost } from "../../react-query-services/offers";
import NextRouter from "next/router";
import { getServicesById } from "../../react-query-services/offers";
import { UploadAttachment } from "../../services/Upload-file";

import { UpdateServices } from "../../react-query-services/offers";
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
  getPostwithPatination: any;
  parentId: any;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const query = useQueryClient();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const getOneServiceById = useQuery(
    ["PostOfferId", props.id],
    () => getServicesById(props.id),
    {
      onError: () => {},
      onSuccess: () => {},
    }
  );

  console.log(props.id, props.parentId);

  const Update_Post = useMutation("Update-offers", UpdateServices, {
    onSuccess: (res: any) => {
      if (res.result != null) {
        form.resetFields();
        notification.open({
          message: "The  Offers Update successfully",
        });
        props.getPostwithPatination.refetch();
        setLoading(false);
        form.resetFields();
        props.cancleshowModla();
      } else if (res?.error != null) {
        notification.open({
          message: res.error.message + "",
        });
        setLoading(false);
      }
    },
    onError: (res: any) => {},
  });

  console.log(getOneServiceById?.data?.resulte);

  const onFinishHandler = (e: any) => {
    setLoading(true);
    if (e.attachments[0]?.url) {
      console.log("is past image");
      let body: any = {};
      body.translations = [
        {
          title: e.title,
          description: e.description,
          language: "en",
        },
      ];
      body.attachment = e.attachments[0].id;
      body.id = +props.id;
      body.link = e.link;
      body.amount = e.amount;
      body.partnerId = props.parentId;
      Update_Post.mutateAsync(body);
    } else {
      let formData = new FormData();
      formData.append("RefType", 4 + " ");
      formData.append("File", e?.attachments?.fileList[0]?.originFileObj);
      UploadAttachment.UploadAttachment(formData).then((res: any) => {
        let body: any = {};
        body.translations = [
          {
            title: e.title,
            description: e.description,
            language: "en",
          },
        ];

        body.attachment = res.result.id as number;
        body.id = +props.id;
        body.link = e.link;
        body.partnerId = props.parentId;
        body.amount = e.amount;
        Update_Post.mutateAsync(body);
      });
    }
  };

  return (
    <Row>
      <Modal
        title="Edit Offer"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner form"
        style={{ top: "80px" }}
      >
        {getOneServiceById.isLoading && (
          <Row
            style={{
              width: "100%",
              height: "434px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <i
              className="fas fa-circle-notch fa-spin"
              style={{ fontSize: 25, color: "#9fa1a1" }}
            ></i>
          </Row>
        )}
        {!getOneServiceById.isLoading && (
          <Row
            style={{
              backgroundColor: "white",
              width: "100%",
              marginBottom: "30px",
              paddingBottom: 20,
              paddingTop: 30,
            }}
            className="globalFormStyle"
          >
            <Form
              style={{ width: "100%" }}
              onFinish={onFinishHandler}
              form={form}
              initialValues={{ ...getOneServiceById?.data?.result }}
            >
              <Row
                justify="space-between"
                style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
              >
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Title"}
                    name="title"
                    style={{ width: "100%" }}
                    labelCol={{ span: 24 }}
                    rules={[
                      {
                        required: true,
                        message: "the title Required ",
                      },
                    ]}
                  >
                    <Input placeholder="title"></Input>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Description"}
                    name="description"
                    style={{ width: "100%" }}
                    labelCol={{ span: 24 }}
                    rules={[
                      {
                        required: true,
                        message: "the description Required ",
                      },
                    ]}
                  >
                    <Input placeholder="description"></Input>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Amount"}
                    name="amount"
                    style={{ width: "100%" }}
                    labelCol={{ span: 24 }}
                    rules={[
                      {
                        required: true,
                        message: "the amount Required ",
                      },
                    ]}
                  >
                    <Input placeholder="description" type="number"></Input>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Link"}
                    name="link"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the Link  Required ",
                      },
                    ]}
                  >
                    <TextArea placeholder="Link"></TextArea>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Service Image "}
                    name="attachments"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the Image Offers  Required ",
                      },
                    ]}
                  >
                    <Upload
                      listType="picture"
                      className="upload-list-inline"
                      accept="image/png, image/jpeg , image/jpeg"
                      multiple={false}
                      maxCount={1}
                      defaultFileList={getOneServiceById?.data?.result?.attachments?.map(
                        (ele: any) => {
                          return {
                            uid: ele?.id,
                            name: "images",
                            status: "done",
                            url: ele.url,
                            thumbUrl: ele.url,
                          };
                        }
                      )}
                    >
                      <Button
                        className="button-upload-image-partner"
                        icon={<PlusOutlined style={{ fontSize: 20 }} />}
                      >
                      
                      </Button>
                    </Upload>
                  </Form.Item>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  style={{
                    width: "100%",
                    justifyContent: "end",
                    display: "flex",
                  }}
                >
                  <Form.Item style={{ width: "100", justifyContent: "end" }}>
                    <Button
                      style={{
                        padding: "15px 30px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: "4px",
                        border: "none",
                        backgroundColor: "#b1bdbe",
                      }}
                      htmlType={"submit"}
                      className={"add-button"}
                      loading={loading}
                    >
                      Edit Offer
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Row>
        )}
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
