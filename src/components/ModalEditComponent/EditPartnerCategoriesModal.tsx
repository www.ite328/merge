import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddPost } from "../../react-query-services/offers";
import NextRouter from "next/router";
import { getPartnerCategoriesById } from "../../react-query-services/Partner-gategories";
import { UploadAttachment } from "../../services/Upload-file";

import { UpdatePartnerCategories } from "../../react-query-services/Partner-gategories";
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const query = useQueryClient();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const getOneServiceById = useQuery(
    ["PartnerCategoriesById", props.id],
    () => getPartnerCategoriesById(props.id),
    {
      onError: () => {},
      onSuccess: () => {},
    }
  );

  const Update_Post = useMutation(
    "Partner-Category-updat",
    UpdatePartnerCategories,
    {
      onSuccess: (res: any) => {
        if (res.result != null) {
          form.resetFields();
          notification.open({
            message: "The  Partner Category Update successfully",
          });
          query.invalidateQueries("getAllPostCategoriesWithPatination");
          setLoading(false);
          form.resetFields();
          props.cancleshowModla();
        } else if (res?.error != null) {
          notification.open({
            message: res.error.message + "",
          });
          setLoading(false);
        }
      },
      onError: (res: any) => {},
    }
  );

  console.log(getOneServiceById?.data?.resulte);

  const onFinishHandler = (e: any) => {
    setLoading(true);

    if (e.attachments[0]?.url) {
      console.log("is past image");
      let body: any = {};
      body.translations = [
        {
          name: e.name,
          language: "en",
        },
      ];
      body.attachment = +e.attachments[0].id;
      body.id = +props.id;

      Update_Post.mutateAsync(body);
    } else {
      let formData = new FormData();
      formData.append("RefType", 6 + " ");
      formData.append("File", e?.attachments?.fileList[0]?.originFileObj);
      UploadAttachment.UploadAttachment(formData).then((res: any) => {
        let body: any = {};
        body.translations = [
          {
            name: e.name,

            language: "en",
          },
        ];

        body.attachment = res.result.id as number;
        body.id = +props.id;

        Update_Post.mutateAsync(body);
      });
    }
  };

  return (
    <Row>
      <Modal
        title="  Edit Partner Category"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partner "
        style={{ top: "80px" }}
      >
        {getOneServiceById.isLoading && (
          <Row
            style={{
              width: "100%",
              height: "434px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <i
              className="fas fa-circle-notch fa-spin"
              style={{ fontSize: 25, color: "#9fa1a1" }}
            ></i>
          </Row>
        )}
        {!getOneServiceById.isLoading && (
          <Row
            style={{
              backgroundColor: "white",
              width: "100%",
              marginBottom: "30px",
              paddingBottom: 20,
              paddingTop: 30,
            }}
            className="globalFormStyle"
          >
            <Form
              style={{ width: "100%" }}
              onFinish={onFinishHandler}
              form={form}
              initialValues={{ ...getOneServiceById?.data?.result }}
            >
              <Row
                justify="space-between"
                style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
              >
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Name"}
                    name="name"
                    style={{ width: "100%" }}
                    labelCol={{ span: 24 }}
                    rules={[
                      {
                        required: true,
                        message: "the name Required ",
                      },
                    ]}
                  >
                    <Input placeholder="name"></Input>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={24} md={24} lg={24}>
                  <Form.Item
                    label={"Partner Category Image "}
                    name="attachments"
                    labelCol={{ span: 24 }}
                    style={{ width: "100%" }}
                    rules={[
                      {
                        required: true,
                        message: "the Image Partner Category  Required ",
                      },
                    ]}
                  >
                    <Upload
                      listType="picture"
                      className="upload-list-inline"
                      accept="image/png, image/jpeg , image/jpeg"
                      multiple={false}
                      maxCount={1}
                      defaultFileList={getOneServiceById?.data?.result?.attachments?.map(
                        (ele: any) => {
                          return {
                            uid: ele?.id,
                            name: "images",
                            status: "done",
                            url: ele.url,
                            thumbUrl: ele.url,
                          };
                        }
                      )}
                    >
                      <Button
                        className="button-upload-image-partner"
                        icon={<PlusOutlined style={{ fontSize: 20 }} />}
                      ></Button>
                    </Upload>
                  </Form.Item>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  style={{
                    width: "100%",
                    justifyContent: "end",
                    display: "flex",
                  }}
                >
                  <Form.Item style={{ width: "100", justifyContent: "end" }}>
                    <Button
                      style={{
                        padding: "15px 30px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: "4px",
                        border: "none",
                        backgroundColor: "#b1bdbe",
                      }}
                      htmlType={"submit"}
                      className={"add-button"}
                      loading={loading}
                    >
                      Edit Partner Category
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Row>
        )}
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
