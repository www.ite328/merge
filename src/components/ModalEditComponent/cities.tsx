import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Select,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { Translation } from "../../model/offers";
import { Offer_Req } from "../../model/offers";
import { AddCountry } from "../../react-query-services/cities";
import NextRouter from "next/router";
import { UploadAttachment } from "../../services/Upload-file";
//nfjkdnkjfd
const AddSubPartner: React.FC<{
  isshow: boolean;
  id: any;
  cancleshowModla: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(props.isshow);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    props.cancleshowModla();
    setIsModalVisible(false);
  };
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const AddPartner = useMutation("Add-City", AddCountry, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "The  City add successfully",
      });
      setLoading(false);
      props.cancleshowModla();
      NextRouter.push(`cities/${props.id}`);
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    let body: any = {
      translations: [],
      countryId: props.id,
    };
    let objectArabic: any = {
      language: "ar",
      name: e.namear,
    };
    let objectEnglish: any = {
      language: "en",
      name: e.nameen,
    };
    body.translations.push(objectArabic, objectEnglish);

    AddPartner.mutateAsync(body);
  };

  return (
    <Row>
      <Modal
        title="ADD CITY"
        visible={props.isshow}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"85%"}
        className="madal-partners"
        style={{ top: "150px" }}
      >
        <Row
          style={{
            backgroundColor: "white",
            width: "100%",
            marginBottom: "30px",
            paddingBottom: 20,
            paddingTop: 30,
          }}
          className="globalFormStyle"
        >
          <Form style={{ width: "100%" }} onFinish={onFinishHandler}>
            <Row
              justify="space-between"
              style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
            >
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"name-en"}
                  name="nameen"
                  style={{ width: "100%" }}
                  labelCol={{ span: 24 }}
                  rules={[
                    {
                      required: true,
                      message: "the name-en Required ",
                    },
                  ]}
                >
                  <Input placeholder="name-en"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  label={"name-ar"}
                  name="namear"
                  labelCol={{ span: 24 }}
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                      message: "the name-ar Required ",
                    },
                  ]}
                >
                  <Input placeholder="name-ar"></Input>
                </Form.Item>
              </Col>

              <Col
                xs={24}
                sm={24}
                md={24}
                lg={24}
                style={{
                  width: "100%",
                  justifyContent: "end",
                  display: "flex",
                }}
              >
                <Form.Item style={{ width: "100", justifyContent: "end" }}>
                  <Button
                    style={{
                      padding: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: "white",
                      borderRadius: "4px",
                      border: "none",
                      backgroundColor: "#b1bdbe",
                    }}
                    loading={AddPartner.isLoading}
                    htmlType="submit"
                    className="add-button"
                  >
                    Add new City
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </Modal>
    </Row>
  );
  return <></>;
};
export default AddSubPartner;
