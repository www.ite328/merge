import {
  EditOutlined,
  DeleteOutlined,
  CaretDownOutlined,
  DeleteFilled,
  EditFilled,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Button,
  Dropdown,
  Menu,
  notification,
  Typography,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getSliderWithPatination } from "../../react-query-services/PushNotification";
import { categoryService } from "../../services/Partner";
import Link from "next/link";
import Table from "../GenericTable/index";
import EditPartnerModal from "../ModalEditComponent/EditPartnerModal";
import Paragraph from "antd/lib/skeleton/Paragraph";
const TablePartner: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const [showModalEditPartner, setModalEditPartner] = useState<boolean>(false);
  const [id, setId] = useState<any>();
  const { Paragraph } = Typography;

  const query = useQueryClient();
  const getPartnerwithPatination = useQuery(
    ["getSliderWithPatination", props.search, skipcount],
    () => getSliderWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };
  const closeEditPartnerHandler = () => {
    setModalEditPartner(false);
  };

  const DeletedHandler = (id: any) => {
    categoryService.DeletePostCategories(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Services Deleted Successfully ",
        });
        query.invalidateQueries("getAllPartnerWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };

  const UpdatePartnerHandler = (id: any) => {
    setId(id);
    setModalEditPartner(true);
  };
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => UpdatePartnerHandler(id)}
              >
                Edit
              </a>
            ),
            icon: <i className="fa fa-edit icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
        ]}
      />
    );
  };
  useEffect(() => {
    console.log(getPartnerwithPatination?.data);
    if (
      totalCount < getPartnerwithPatination?.data?.totalCount ||
      totalCount > getPartnerwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPartnerwithPatination?.data?.totalCount);
    }
  }, [getPartnerwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 4;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Meesage-ar</p>,
      dataIndex: "translations",
      key: "translations",
      width: "17%",
      render: (translations: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {translations[0].language != "ar"
              ? translations[1]?.message + ""
              : translations[0]?.message}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Meesage-en</p>,
      dataIndex: "translations",
      key: "translations",
      width: "17%",
      render: (translations: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {translations[0].language == "ar"
              ? translations[1]?.message + ""
              : translations[0]?.message}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Title-ar</p>,
      dataIndex: "arTitle",
      key: "arTitle",
      width: "17%",
      render: (translations: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {translations}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Title-en</p>,
      dataIndex: "enTitle",
      key: "enTitle",
      width: "17%",
      render: (translations: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {translations}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Type</p>,
      dataIndex: "destinationText",
      key: "destinationText",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <Paragraph
            ellipsis={{
              rows: 1,
            }}
            style={{ marginBottom: 0 }}
          >
            <p
              style={{
                width: "100%",
                textAlign: "center",
                marginBottom: 0,
                color: "#525f7f",
              }}
            >
              {isActive + ""}
            </p>
          </Paragraph>
        );
      },
    },

    // {
    //   title: "Operations",
    //   key: "id",
    //   dataIndex: "id",
    //   width: "15%",
    //   render: (id: any, body: any) => {
    //     return (
    //       <Row justify="center" style={{ width: "100%", gap: 20 }}>
    //         <Dropdown
    //           overlay={() => menu(id, body.isActive)}
    //           placement="bottomRight"
    //         >
    //           <Button
    //             style={{
    //               display: "flex",
    //               justifyContent: "space-between",
    //               alignItems: "center",
    //               width: "75%",
    //               padding: "10px 6px",
    //             }}
    //           >
    //             <i
    //               className="fa fa-cog"
    //               aria-hidden="true"
    //               style={{ fontSize: 13, color: "#648dea" }}
    //             ></i>
    //             <CaretDownOutlined
    //               style={{ fontSize: 13, color: "#648dea" }}
    //             ></CaretDownOutlined>
    //           </Button>
    //         </Dropdown>
    //       </Row>
    //     );
    //   },
    // },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>notifications</p>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getPartnerwithPatination?.data?.items}
          columns={columns}
          loading={getPartnerwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={6}
          onChange={(e) => onchangeHandler(e, 6)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditPartnerModal
          isshow={showModalEditPartner}
          id={id}
          cancleshowModla={closeEditPartnerHandler}
          parentId={null}
          getAllPartnerCategories={() => {}}
        ></EditPartnerModal>
      </Row>
    </Row>
  );
};

export default TablePartner;

//fjidfjfjd
