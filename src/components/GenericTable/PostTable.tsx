import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getPostWithPatination } from "../../react-query-services/Post";
import { PostService } from "../../services/Posts";
import Table from "../GenericTable/index";
import EditPostModal from "../ModalEditComponent/EditePostModal";
import { Slider, Typography } from "antd";

const { Paragraph } = Typography;

const TablePost: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const query = useQueryClient();
  const getPostwithPatination = useQuery(
    ["getAllPostCategoriesWithPatination", props.search, skipcount],
    () => getPostWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const [showModalEditPost, setModalEditPost] = useState<boolean>(false);
  const [id, setId] = useState<any>();

  const EditPostHandler = () => {
    setModalEditPost(true);
  };

  const closeEditPostHandler = () => {
    setModalEditPost(false);
  };
  useEffect(() => {
    console.log(getPostwithPatination?.data);
    if (
      totalCount < getPostwithPatination?.data?.totalCount ||
      totalCount > getPostwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPostwithPatination?.data?.totalCount);
    }
  }, [getPostwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };
  const DeletedHandler = (id: any) => {
    PostService.DeletePostCategories(id).then((res) => {
      if (res?.error == null) {
        console.log("ndfjlnjldf");
        notification.open({
          message: " The Post Deleted Successfully ",
        });
        query.invalidateQueries("getAllPostCategoriesWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    PostService.SwitchActivationPost({ id: id, isActive: !isActive }).then(
      (res) => {
        if (res?.result == null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else if (res.result != null) {
          notification.open({
            message: "The Post Switch Activated Successfully",
          });
          query.invalidateQueries("getAllPostCategoriesWithPatination");
        }
      }
    );
  };
  const UpdatePostHandler = (id: any) => {
    setId(id);
    setModalEditPost(true);
  };
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => UpdatePostHandler(id)}
              >
                Edit
              </a>
            ),
            icon: <i className="fa fa-edit icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => SwitchActivationHandler(id, isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "17%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={100}
            height={100}
            preview={false}
            style={{
              borderRadius: "50%",

              padding: 4,
            }}
            className="table-images"
          ></Image>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Title</p>,
      dataIndex: "title",
      key: "title",
      width: "17%",
      render: (title: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              marginBottom: 0,
              color: "#525f7f",
            }}
          >
            {title}
          </p>
        );
      },
    },
    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>Description</p>
      ),
      dataIndex: "description",
      key: "description",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <Paragraph
            ellipsis={{
              rows: 2,
            }}
          >
            <p
              style={{
                width: "100%",
                textAlign: "center",
                marginBottom: 0,
                color: "#525f7f",
              }}
            >
              {isActive + ""}
            </p>
          </Paragraph>
        );
      },
    },

    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>Post Category</p>
      ),
      dataIndex: "postCategory",
      key: "postCategory",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center" }}>
            {isActive?.name + ""}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Active</p>,
      dataIndex: "isActive",
      key: "isActive",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", paddingBottom: 0 }}>
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "address",
      width: "10%",
      dataIndex: "id",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                  marginBottom: 15,
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>posts</p>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getPostwithPatination?.data?.items}
          columns={columns}
          loading={getPostwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={10}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditPostModal
          isshow={showModalEditPost}
          id={id}
          cancleshowModla={closeEditPostHandler}
        ></EditPostModal>
      </Row>
    </Row>
  );
};

export default TablePost;
