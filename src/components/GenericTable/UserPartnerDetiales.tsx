import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Image,
  Pagination,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { Report } from "../../model/Report";
import { getUserWithPatination } from "../../react-query-services/user";
import { ReportService } from "../../services/Report";
import { UserService } from "../../services/user";
import Table from "../GenericTable/index";

const TablePartner: FC<{ UserDetiales: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const query = useQueryClient();
  // const getPartnerwithPatination = useQuery(
  //   ["getAllUserWithPatination", props.search, skipcount],
  //   () => getUserWithPatination(props.search, skipcount),
  //   {
  //     onSuccess: () => {},
  //     onError: () => {},
  //   }
  // );

  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];
  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "17%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={100}
            height={100}
            preview={false}
            style={{
              borderRadius: "50%",

              padding: 4,
            }}
            className="table-images"
          ></Image>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Name</p>,
      dataIndex: "user",
      key: "user",
      width: "17%",
      render: (isActive: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {isActive.name + "" + isActive.surname}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Partner</p>,
      dataIndex: "partner",
      key: "partner",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              paddingLeft: 3,
              marginBottom: 0,
            }}
          >
            {isActive.name + ""}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Parent</p>,
      dataIndex: "partner",
      key: "partner",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive?.isParent + ""}
          </p>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="PartnerTableDetiales"
    >
      <Row
        style={{
          width: "100%",
          marginBottom: 25,
          boxShadow: "none !important",
        }}
        className="globalFormHeader"
      ></Row>

      <Row style={{ width: "100%" }}>
        <Table
          data={props.UserDetiales}
          columns={columns}
          loading={false}
        ></Table>
        {/* <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={4}
          onChange={(e) => onchangeHandler(e, 4)}
          hideOnSinglePage={false}
        /> */}
      </Row>
    </Row>
  );
};

export default TablePartner;
