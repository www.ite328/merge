import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getSliderWithPatination } from "../../react-query-services/slider";
import { SliderService } from "../../services/slider";
import Table from "../GenericTable/index";

const TablePost: FC<{ search: any }> = (props) => {
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const query = useQueryClient();
  const getPostwithPatination = useQuery(
    ["getSliderWithPatination", props.search, skipcount],
    () => getSliderWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  useEffect(() => {
    console.log(getPostwithPatination?.data);
    if (
      totalCount < getPostwithPatination?.data?.totalCount ||
      totalCount > getPostwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPostwithPatination?.data?.totalCount);
    }
  }, [getPostwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 4;
    SetSkipCount(page);
  };
  const DeletedHandler = (id: any) => {
    SliderService.DeleteSlider(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Slider Deleted Successfully ",
        });
        query.invalidateQueries("getSliderWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };

  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
        ]}
      />
    );
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      width: "10%",
    },
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "60%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={200}
            height={125}
            preview={false}
            style={{}}
          ></Image>
        );
      },
    },

    {
      title: "Operations",
      key: "address",
      width: "10%",
      dataIndex: "id",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                  marginBottom: 15,
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>sliders</p>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getPostwithPatination?.data?.items}
          columns={columns}
          loading={getPostwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={4}
          onChange={(e) => onchangeHandler(e, 4)}
          hideOnSinglePage={false}
        />
      </Row>
    </Row>
  );
};

export default TablePost;
