import { CaretDownOutlined } from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { useRouter } from "next/router";

import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getCountryWithPatination } from "../../react-query-services/cities";
import { countryService } from "../../services/cities";
import { Report } from "../../model/Report";
import { ReportService } from "../../services/Report";
import Table from "../GenericTable/index";
const TablePost: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const query = useQueryClient();
  const Router = useRouter();
  const { id } = Router.query;

  const getCountrywithPatination = useQuery(
    ["getAllCityWithPatination", props.search, skipcount, id],
    () => getCountryWithPatination(props.search, skipcount, id),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const ExpoertToCancelHandler = (e: any) => {
    let obj: Report = {
      cityExport: true,
    };
    ReportService.ExportToExcel(obj).then((res: any) => {
      console.log(res, "res");
      window.open(res?.result?.url);
    });
  };

  useEffect(() => {
    if (
      totalCount < getCountrywithPatination?.data?.totalCount ||
      totalCount > getCountrywithPatination?.data?.totalCount
    ) {
      SetTotalCount(getCountrywithPatination?.data?.totalCount);
    }
  }, [getCountrywithPatination?.data, totalCount]);
  const DeletedHandler = (id: any) => {
    countryService.DeleteCountry(id).then((res) => {
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      } else if (res?.result == null && res.error == null) {
        notification.open({
          message: "The City Deleted Successfully",
        });
        query.invalidateQueries("getAllCityWithPatination");
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    console.log(isActive, "IsActive");
    countryService
      .SwitchActivationCountry({ id: id, isActive: isActive })
      .then((res) => {
        if (res?.result == null && res.error != null) {
          notification.open({
            message: res?.error?.message + "",
          });
          query.invalidateQueries("getAllCityWithPatination");
        } else if (res?.result != null && res.error == null) {
          notification.open({
            message: "The City Switch Activated Successfully",
          });
          query.invalidateQueries("getAllCityWithPatination");
        }
      });
  };

  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => SwitchActivationHandler(id, !isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };
  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 3;
    SetSkipCount(page);
  };

  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "left", paddingLeft: 6 }}>
            {isActive + ""}
          </p>
        );
      },
    },
    {
      title: "is Active",
      dataIndex: "isActive",
      key: "isActive",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "left", paddingLeft: 8 }}>
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "address",
      dataIndex: "id",
      width: "4%",
      render: (id: any, body: any) => {
        return (
          <Row justify="start" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: "17px 20px",
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <Row
          style={{ width: "100%", alignItems: "center" }}
          justify="space-between"
        >
          <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>cities</p>
          <Button
            onClick={ExpoertToCancelHandler}
            style={{
              backgroundColor: "#5e72e4",
              color: "white",
              fontWeight: 400,
              borderRadius: 6,
              border: "none",
            }}
          >
            Export To Excel
          </Button>
        </Row>
      </Row>

      <Row style={{ width: "100%" }}>
        <Table
          data={getCountrywithPatination?.data?.items}
          columns={columns}
          loading={getCountrywithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={
            // getPostCategorieswithPatination?.data &&
            // getPostCategorieswithPatination?.data?.totalCount
            totalCount
          }
          pageSize={3}
          onChange={(e) => onchangeHandler(e, 3)}
          hideOnSinglePage={false}
        />
      </Row>
    </Row>
  );
};

export default TablePost;
