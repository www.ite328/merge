import { CaretDownOutlined, DeleteFilled, EditFilled } from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import Cities from "../../components/ModalEditComponent/cities";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getCountryWithPatination } from "../../react-query-services/country";
import { countryService } from "../../services/country";
import Link from "next/link";
import Table from "../GenericTable/index";
const TablePost: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const [showModalAddCity, setModalShowCity] = useState<boolean>(false);
  const [id, setId] = useState<any>(0);
  const query = useQueryClient();
  const getCountrywithPatination = useQuery(
    ["getAllCountryWithPatination", props.search, skipcount],
    () => getCountryWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const addSubPartnerHandler = () => {
    setModalShowCity(true);
  };

  const closeSubPartnerHandler = () => {
    setModalShowCity(false);
  };

  useEffect(() => {
    if (
      totalCount < getCountrywithPatination?.data?.totalCount ||
      totalCount > getCountrywithPatination?.data?.totalCount
    ) {
      SetTotalCount(getCountrywithPatination?.data?.totalCount);
    }
  }, [getCountrywithPatination?.data, totalCount]);
  const DeletedHandler = (id: any) => {
    countryService.DeleteCountry(id).then((res) => {
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      } else if (res?.result == null && res.error == null) {
        notification.open({
          message: "The Country Deleted Successfully",
        });
        query.invalidateQueries("getAllCountryWithPatination");
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    console.log(isActive, "IsActive");
    countryService
      .SwitchActivationCountry({ id: id, isActive: isActive })
      .then((res) => {
        if (res?.result == null && res.error != null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else if (res?.result != null) {
          notification.open({
            message: "The Country Switch Activated Successfully",
          });
          query.invalidateQueries("getAllCountryWithPatination");
        }
      });
  };

  const AddCityHandler = (id: any) => {
    setId(id);
    addSubPartnerHandler();
  };
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
       
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => AddCityHandler(id)}
              >
                Add City
              </a>
            ),
            icon: <i className="fa fa-plus icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
         
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
              
                onClick={() => SwitchActivationHandler(id, !isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };
  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 8;
    SetSkipCount(page);
  };

  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "40%",
      render: (isActive: any, body: any) => {
        `${console.log(isActive)}`;
        return (
          <Link href={`/cities/${body.id}`} passHref={true}>
            <p
              style={{
                width: "100%",
                textAlign: "left",
                paddingLeft: 6,
                cursor: "pointer",
              }}
            >
              {isActive + ""}
            </p>
          </Link>
        );
      },
    },
    {
      title: "is Active",
      dataIndex: "isActive",
      key: "isActive",
      width: "40%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "left", paddingLeft: 8 }}>
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "address",
      dataIndex: "id",
      width: "4%",
      render: (id: any, body: any) => {
        return (
          <Row justify="start" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>country</p>
      </Row>

      <Row style={{ width: "100%" }}>
        <Table
          data={getCountrywithPatination?.data?.items}
          columns={columns}
          loading={getCountrywithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={
            // getPostCategorieswithPatination?.data &&
            // getPostCategorieswithPatination?.data?.totalCount
            totalCount
          }
          pageSize={8}
          onChange={(e) => onchangeHandler(e, 8)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <Cities
          isshow={showModalAddCity}
          id={id}
          cancleshowModla={closeSubPartnerHandler}
        ></Cities>
      </Row>
    </Row>
  );
};

export default TablePost;
