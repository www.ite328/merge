import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Image,
  Pagination,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { Report } from "../../model/Report";
import { getUserWithPatination } from "../../react-query-services/user";
import { ReportService } from "../../services/Report";
import { UserService } from "../../services/user";
import Table from "../GenericTable/index";

const TablePartner: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const query = useQueryClient();
  const getPartnerwithPatination = useQuery(
    ["getAllUserWithPatination", props.search, skipcount],
    () => getUserWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          // {
          //   key: "1",
          //   label: (
          //     <a
          //       rel="noopener noreferrer"
          //       href="#"
          //       style={{ fontFamily: "Poppins", fontWeight: 400 }}
          //       className="icon-link"
          //     >
          //       Edite
          //     </a>
          //   ),
          //   icon: <i className="fa fa-edit icon-setting"></i>,
          //   disabled: false,
          // },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                href="#"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
        ]}
      />
    );
  };

  useEffect(() => {
    console.log(getPartnerwithPatination?.data);
    if (
      totalCount < getPartnerwithPatination?.data?.totalCount ||
      totalCount > getPartnerwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPartnerwithPatination?.data?.totalCount);
    }
  }, [getPartnerwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 4;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];
  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };

  const DeletedHandler = (id: any) => {
    UserService.DeleteUserCategories(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The User Partner Deleted Successfully ",
        });
        query.invalidateQueries("getAllUserWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
        query.invalidateQueries("getAllUserWithPatination");
      }
    });
  };

  const ExpoertToCancelHandler = (e: any) => {
    let obj: Report = {
      agentUserExport: true,
    };
    ReportService.ExportToExcel(obj).then((res: any) => {
      console.log(res, "res");
      window.open(res?.result?.url);
    });
  };

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "17%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={100}
            height={100}
            preview={false}
            style={{
              borderRadius: "50%",

              padding: 4,
            }}
            className="table-images"
          ></Image>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Name</p>,
      dataIndex: "userName",
      key: "userName",
      width: "17%",
      render: (isActive: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Phone</p>,
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              paddingLeft: 3,
              marginBottom: 0,
            }}
          >
            {isActive + ""}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>City</p>,
      dataIndex: "city",
      key: "city",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive?.name + ""}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Active</p>,
      dataIndex: "isActive",
      key: "isActive",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              paddingLeft: 4,
              marginBottom: 0,
            }}
          >
            {isActive + ""}
          </p>
        );
      },
    },
    {
      title: "Operations",
      key: "address",
      dataIndex: "id",
      width: "12%",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                  marginBottom: 15,
                }}
              >
                <i
                  className="fa fa-cog icon-edite"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{
                    fontSize: 13,
                    color: "#648dea",
                  }}
                  className="icon-edite"
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: "17px 20px",
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <Row
          justify="space-between"
          style={{ width: "100%", alignItems: "center" }}
        >
          <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>
            sign interepter
          </p>
          <Button
            onClick={ExpoertToCancelHandler}
            style={{
              backgroundColor: "#5e72e4",
              color: "white",
              fontWeight: 400,
              borderRadius: 6,
              border: "none",
            }}
          >
            Export To Excel
          </Button>
        </Row>
      </Row>

      <Row style={{ width: "100%" }}>
        <Table
          data={getPartnerwithPatination?.data?.items}
          columns={columns}
          loading={getPartnerwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={4}
          onChange={(e) => onchangeHandler(e, 4)}
          hideOnSinglePage={false}
        />
      </Row>
    </Row>
  );
};

export default TablePartner;
