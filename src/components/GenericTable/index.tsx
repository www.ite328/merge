import { Row, Table } from "antd";
import Spin from "./Sping";
const GenericTable: React.FC<{ data: any; columns: any; loading: boolean }> = (
  props
) => {
  return (
    <Row
      style={{
        width: "100%",
        overflowX: "auto",
      }}
    >
      <Table
        style={{ width: "100%", backgroundColor: "#fff" }}
        dataSource={props.data}
        columns={props.columns}
        rowKey={"id"}
        loading={{ indicator: <Spin />, spinning: props.loading }}
        // scroll={{ x: 400, y: "hidden" }}
      ></Table>
    </Row>
  );
};
export default GenericTable;
