import {
  EditOutlined,
  DeleteOutlined,
  CaretDownOutlined,
  DeleteFilled,
  EditFilled,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Button,
  Dropdown,
  Menu,
  notification,
  Typography,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getRoleWithPatination } from "../../react-query-services/roles";
import { PostService } from "../../services/role";
import Link from "next/link";
import Table from "../GenericTable/index";
import EditPartnerModal from "../ModalEditComponent/EditeRole";
import Paragraph from "antd/lib/skeleton/Paragraph";
const TablePartner: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const [showModalEditPartner, setModalEditPartner] = useState<boolean>(false);
  const [id, setId] = useState<any>();
  const { Paragraph } = Typography;

  const query = useQueryClient();
  const getPartnerwithPatination = useQuery(
    ["getRoleWithPatination", props.search, skipcount],
    () => getRoleWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const closeEditPartnerHandler = () => {
    setModalEditPartner(false);
  };

  const DeletedHandler = (id: any) => {
    PostService.DeleteRole(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Role Deleted Successfully ",
        });
        query.invalidateQueries("getRoleWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };

  const UpdatePartnerHandler = (id: any) => {
    setId(id);
    setModalEditPartner(true);
  };
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => UpdatePartnerHandler(id)}
              >
                Edit
              </a>
            ),
            icon: <i className="fa fa-edit icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
        ]}
      />
    );
  };

  useEffect(() => {
    console.log(getPartnerwithPatination?.data);
    if (
      totalCount < getPartnerwithPatination?.data?.totalCount ||
      totalCount > getPartnerwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPartnerwithPatination?.data?.totalCount);
    }
  }, [getPartnerwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: <p style={{ marginBottom: 0, textAlign: "left" }}>Name</p>,
      dataIndex: "name",
      key: "name",
      width: "45%",
      render: (translations: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "left",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {translations}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "left" }}>Display Name</p>,
      dataIndex: "displayName",
      key: "displayName",
      width: "45%",
      render: (translations: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "left",
              cursor: "pointer",

              marginBottom: 0,
            }}
          >
            {translations}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "id",
      dataIndex: "id",
      width: "10%",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>Role</p>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getPartnerwithPatination?.data?.items}
          columns={columns}
          loading={getPartnerwithPatination?.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={10}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditPartnerModal
          isshow={showModalEditPartner}
          id={id}
          cancleshowModla={closeEditPartnerHandler}
          key={Math.random() * Math.random()}
        ></EditPartnerModal>
      </Row>
    </Row>
  );
};

export default TablePartner;

//fjidfjfjd
