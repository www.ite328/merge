import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import {
  Row,
  Col,
  Image,
  Pagination,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getVedioMeetingWithPatination } from "../../react-query-services/vedio-call-meeting";
import { VedioMeeting } from "../../services/vedio-call-meeting";
import Table from "../GenericTable/index";

const TablePartner: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const query = useQueryClient();
  const getPartnerwithPatination = useQuery(
    ["getVedioMeetingWithPatination", props.search, skipcount],
    () => getVedioMeetingWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
        ]}
      />
    );
  };

  useEffect(() => {
    console.log(getPartnerwithPatination?.data);
    if (
      totalCount < getPartnerwithPatination?.data?.totalCount ||
      totalCount > getPartnerwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPartnerwithPatination?.data?.totalCount);
    }
  }, [getPartnerwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];
  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };

  const DeletedHandler = (id: any) => {
    VedioMeeting.DeleteVedioMeeting(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Vedio Meeting Deleted Successfully ",
        });
        query.invalidateQueries("getVedioMeetingWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };

  const columns = [
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Name</p>,
      dataIndex: "client",
      key: "client",
      width: "20%",
      render: (attachment: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {attachment.name + " " + attachment.surname}
          </p>
        );
      },
    },
    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>Interpreter</p>
      ),
      dataIndex: "interpreter",
      key: "interpreter",
      width: "20%",
      render: (attachment: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {attachment != null ? attachment?.name : "-"}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>duration</p>,
      dataIndex: "duration",
      key: "duration",
      width: "20%",
      render: (attachment: any) => {
        let hours: any = Math.floor(attachment / 3600); // get hours
        let minutes: any = Math.floor((attachment - hours * 3600) / 60); // get minutes
        let seconds: any = attachment - hours * 3600 - minutes * 60; //  get seconds
        // add 0 if value < 10; Example: 2 => 02
        if (hours < 10) hours = "0" + hours;
        if (minutes < 10) {
          minutes = "0" + minutes;
        }
        if (seconds < 10) {
          seconds = "0" + seconds;
        }

        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {hours + ":" + minutes + ":" + seconds}
          </p>
        );
      },
    },
    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>date and time</p>
      ),
      dataIndex: "creationTime",
      key: "creationTime",
      width: "20%",
      render: (attachment: Date) => {
        let date = new Date(attachment);
        console.log(date, typeof date, date.toISOString(), date.getFullYear());
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {date.getFullYear() +
              "/" +
              `${date.getUTCMonth() + 1}` +
              "/" +
              date.getDate()}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Operation</p>,
      key: "address",
      dataIndex: "id",
      width: "12%",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                  marginBottom: 15,
                }}
              >
                <i
                  className="fa fa-cog icon-edite"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{
                    fontSize: 13,
                    color: "#648dea",
                  }}
                  className="icon-edite"
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>vedio call</p>
      </Row>

      <Row style={{ width: "100%" }}>
        <Table
          data={getPartnerwithPatination?.data?.items}
          columns={columns}
          loading={getPartnerwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={1}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
    </Row>
  );
};

export default TablePartner;
