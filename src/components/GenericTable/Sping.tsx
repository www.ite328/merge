import { Space, Spin } from "antd";
const Spins = () => {
  return (
    <Space size={"middle"}>
      <Spin size="default" style={{ color: "#5e72e4 !important" }}></Spin>
    </Space>
  );
};

export default Spins;
