import {
  Row,
  Col,
  Pagination,
  Menu,
  Button,
  Dropdown,
  notification,
} from "antd";
import Table from "../GenericTable/index";
import { CaretDownOutlined, DeleteFilled, EditFilled } from "@ant-design/icons";
import { getPostCategoriesWithPatination } from "../../react-query-services/complaint-categories";
import { useQuery, useQueryClient } from "react-query";
import { FC, useEffect, useState } from "react";
import PostCategories from "../../../pages/PostCategories";
import { categoryService } from "../../services/complaint-categories";
let t = 1;
const TablePost: FC<{ search: any }> = (props) => {
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const query = useQueryClient();
  const getPostCategorieswithPatination = useQuery(
    ["getAllComplaintsWithPatination", props.search, skipcount],
    () => getPostCategoriesWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const DeletedHandler = (id: any) => {
    categoryService.DeletecomplaintCategories(id).then((res) => {
      if (res?.result == null && res?.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      } else if (res.result == null && res.result == null) {
        notification.open({
          message: "The Complaint Categories Deleted Successfully",
        });
        query.invalidateQueries("getAllComplaintsWithPatination");
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    categoryService
      .SwitchActivationcomplaintCategories({ id: id, isActive: !isActive })
      .then((res) => {
        if (res?.result == null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else if (res.result != null) {
          notification.open({
            message: "The Complaint Category Switch Activated Successfully",
          });
          query.invalidateQueries("getAllComplaintsWithPatination");
        }
      });
  };
  const menu = (id: any, isActive: boolean) => {
    return (
      <Menu
        items={[
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                href="#"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                href="#"
                onClick={() => SwitchActivationHandler(id, isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };
  useEffect(() => {
    console.log(getPostCategorieswithPatination?.data);
    if (
      totalCount < getPostCategorieswithPatination?.data?.totalCount ||
      totalCount > getPostCategorieswithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPostCategorieswithPatination?.data?.totalCount);
    }
  }, [getPostCategorieswithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };

  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "25%",
    },
    {
      title: "is Active",
      dataIndex: "isActive",
      key: "isActive",
      width: "25%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "address",
      width: "10%",
      dataIndex: "id",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>
          complaints categories
        </p>
      </Row>

      <Row style={{ width: "100%" }} className="getPostCategories">
        <Table
          data={getPostCategorieswithPatination?.data?.items}
          columns={columns}
          loading={getPostCategorieswithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={
            // getPostCategorieswithPatination?.data &&
            // getPostCategorieswithPatination?.data?.totalCount
            totalCount
          }
          pageSize={10}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
    </Row>
  );
};

export default TablePost;
