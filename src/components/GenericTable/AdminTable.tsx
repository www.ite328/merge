import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Image,
  Pagination,
  Menu,
  Dropdown,
  Button,
  notification,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { Report } from "../../model/Report";
import { getAdminWithPatination } from "../../react-query-services/user";
import { ReportService } from "../../services/Report";
import { UserService } from "../../services/user";
import Table from "../GenericTable/index";
import EditAdminModal from "../ModalEditComponent/EditAdmin";
const TablePartner: FC<{ search: any }> = (props) => {
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const [showModalEditAdmin, setModalEditAdmin] = useState<boolean>(false);
  const [id, setId] = useState<any>();
  const query = useQueryClient();

  const getPartnerwithPatinations = useQuery(
    ["getAdminWithPatination", props.search, skipcount],
    () => getAdminWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  useEffect(() => {
    console.log(getPartnerwithPatinations?.data);
    if (
      totalCount < getPartnerwithPatinations?.data?.totalCount ||
      totalCount > getPartnerwithPatinations?.data?.totalCount
    ) {
      SetTotalCount(getPartnerwithPatinations?.data?.totalCount);
    }
  }, [getPartnerwithPatinations?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];
  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };
  const closeEditPartnerHandler = () => {
    setModalEditAdmin(false);
  };
  const DeletedHandler = (id: any) => {
    UserService.DeleteUserCategories(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Admin Deleted Successfully ",
        });
        query.invalidateQueries("getAdminWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    console.log(isActive, "isActive");
    if (isActive) {
      UserService.DeActivationUser({ id: id }).then((res) => {
        if (res?.result == null && res.error != null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else {
          notification.open({
            message: "The Admin  DeActivated Successfully",
          });
          query.invalidateQueries("getAdminWithPatination");
        }
      });
    } else {
      UserService.ActivationUser({ id: id }).then((res) => {
        if (res?.result == null && res.error != null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else {
          notification.open({
            message: "The Admin  Activated Successfully",
          });
          query.invalidateQueries("getAdminWithPatination");
        }
      });
    }
  };
  const UpdatePartnerHandler = (id: any) => {
    console.log(id, "Admin id ");
    setId(id);
    setModalEditAdmin(true);
  };
  const menu = (id: any, isActive: any) => {
    console.log();
    return (
      <Menu
        items={[
          //   {
          //     key: "1",
          //     label: (
          //       <a
          //         rel="noopener noreferrer"
          //         style={{ fontFamily: "Poppins", fontWeight: 400 }}
          //         className="icon-link"
          //         onClick={() => UpdatePartnerHandler(id)}
          //       >
          //         Edit
          //       </a>
          //     ),
          //     icon: <i className="fa fa-edit icon-setting"></i>,
          //     disabled: false,
          //   },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => SwitchActivationHandler(id, isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };

  const ExpoertToCancelHandler = (e: any) => {
    let obj: Report = {
      agentUserExport: true,
    };
    ReportService.ExportToExcel(obj).then((res: any) => {
      console.log(res, "res");
      window.open(res?.result?.url);
    });
  };

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "17%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={100}
            height={100}
            preview={false}
            style={{
              borderRadius: "50%",

              padding: 4,
            }}
            className="table-images"
          ></Image>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Name</p>,
      dataIndex: "userName",
      key: "userName",
      width: "17%",
      render: (isActive: any, id: any) => {
        `${console.log(id)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Phone</p>,
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              paddingLeft: 3,
              marginBottom: 0,
            }}
          >
            {isActive + ""}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>City</p>,
      dataIndex: "city",
      key: "city",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive?.name + ""}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Active</p>,
      dataIndex: "isActive",
      key: "isActive",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              paddingLeft: 4,
              marginBottom: 0,
            }}
          >
            {isActive + ""}
          </p>
        );
      },
    },
    {
      title: "Operations",
      key: "address",
      dataIndex: "id",
      width: "12%",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                  marginBottom: 15,
                }}
              >
                <i
                  className="fa fa-cog icon-edite"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{
                    fontSize: 13,
                    color: "#648dea",
                  }}
                  className="icon-edite"
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: "20px 20px",
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <Row
          justify="space-between"
          style={{ width: "100%", alignItems: "center" }}
        >
          <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>admin</p>
        </Row>
      </Row>

      <Row style={{ width: "100%" }}>
        <Table
          data={getPartnerwithPatinations?.data?.items}
          columns={columns}
          loading={getPartnerwithPatinations.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={10}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditAdminModal
          isshow={showModalEditAdmin}
          id={id}
          cancleshowModla={closeEditPartnerHandler}
          key={Math.random()}
        ></EditAdminModal>
      </Row>
    </Row>
  );
};

export default TablePartner;
