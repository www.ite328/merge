import {
  Row,
  Col,
  Pagination,
  Menu,
  Button,
  Dropdown,
  notification,
  Image,
} from "antd";
import Table from "../GenericTable/index";
import EditPartnerCategoriesModal from "../ModalEditComponent/EditPartnerCategoriesModal";
import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  getPostCategoriesWithPatination,
  getDeleteCategories,
} from "../../react-query-services/Partner-gategories";
import { PartnerService } from "../../services/Partner-categories";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { FC, useEffect, useState } from "react";
import PostCategories from "../../../pages/PostCategories";
let t = 1;
const TablePost: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const [showModalEditPartnerCategories, setModalEditPartnerCategories] =
    useState<boolean>(false);
  const [id, setId] = useState<any>();

  const closeEditPartnerCategories = () => {
    setModalEditPartnerCategories(false);
  };
  const UpdatePostHandler = (id: any) => {
    setId(id);
    setModalEditPartnerCategories(true);
  };

  const query = useQueryClient();
  const getPostCategorieswithPatination = useQuery(
    ["getAllPostCategoriesWithPatination", props.search, skipcount],
    () => getPostCategoriesWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const DeletedHandler = (id: any) => {
    PartnerService.DeletePartnerCategories(id).then((res) => {
      if (res?.result == null && res?.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      } else if (res?.result == null && res.error == null) {
        notification.open({
          message: "The Partner Categories Deleted Successfully",
        });
        query.invalidateQueries("getAllPostCategoriesWithPatination");
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    PartnerService.SwitchActivationUser({ id: id, isActive: !isActive }).then(
      (res) => {
        if (res?.result == null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else if (res.result != null) {
          notification.open({
            message: "The Partner  Category Switch Activated Successfully",
          });
          query.invalidateQueries("getAllPostCategoriesWithPatination");
        }
      }
    );
  };

  useEffect(() => {
    if (
      totalCount < getPostCategorieswithPatination?.data?.totalCount ||
      totalCount > getPostCategorieswithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPostCategorieswithPatination?.data?.totalCount);
    }
  }, [getPostCategorieswithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 4;
    SetSkipCount(page);
  };
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => UpdatePostHandler(id)}
              >
                Edit
              </a>
            ),
            icon: <i className="fa fa-edit icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => SwitchActivationHandler(id, isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "25%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={100}
            height={100}
            preview={false}
            style={{
              borderRadius: "50%",

              padding: 4,
            }}
            className="table-images"
          ></Image>
        );
      },
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "25%",
    },
    {
      title: "is Active",
      dataIndex: "isActive",
      key: "isActive",
      width: "25%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "id",
      dataIndex: "id",
      width: "10%",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>
          partner categories
        </p>
      </Row>

      <Row style={{ width: "100%" }} className="getPostCategories">
        <Table
          data={getPostCategorieswithPatination?.data?.items}
          columns={columns}
          loading={getPostCategorieswithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={
            // getPostCategorieswithPatination?.data &&
            // getPostCategorieswithPatination?.data?.totalCount
            totalCount
          }
          pageSize={4}
          onChange={(e) => onchangeHandler(e, 4)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditPartnerCategoriesModal
          isshow={showModalEditPartnerCategories}
          id={id}
          cancleshowModla={closeEditPartnerCategories}
        ></EditPartnerCategoriesModal>
      </Row>
    </Row>
  );
};

export default TablePost;
