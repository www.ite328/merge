import {
  EditOutlined,
  DeleteOutlined,
  CaretDownOutlined,
  DeleteFilled,
  EditFilled,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Button,
  Dropdown,
  Menu,
  notification,
  Typography,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getComplaintsWithPatination  } from "../../react-query-services/Complaints";

import Link from "next/link";
import Table from "../GenericTable/index";
import EditPartnerModal from "../ModalEditComponent/EditPartnerModal";
import Paragraph from "antd/lib/skeleton/Paragraph";
const TablePartner: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const [showModalEditPartner, setModalEditPartner] = useState<boolean>(false);
  const [id, setId] = useState<any>();
  const { Paragraph } = Typography;

  const query = useQueryClient();
  const getComplaintsWithPatinations = useQuery(
    ["getComplaintsWithPatination", props.search, skipcount],
    () => getComplaintsWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };
  const closeEditPartnerHandler = () => {
    setModalEditPartner(false);
  };

  useEffect(() => {
    console.log(getComplaintsWithPatinations?.data);
    if (
      totalCount < getComplaintsWithPatinations?.data?.totalCount ||
      totalCount > getComplaintsWithPatinations?.data?.totalCount
    ) {
      SetTotalCount(getComplaintsWithPatinations?.data?.totalCount);
    }
  }, [getComplaintsWithPatinations?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "user name",
      dataIndex: "user",
      key: "user",
      width: "20%",
      render: (attachment: any, id: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {attachment?.name}
          </p>
        );
      },
    },
    {
      title: "phoneNumber",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      width: "20%",
      render: (phoneNumber: any, id: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {phoneNumber == null ? "-" : phoneNumber}
          </p>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Email</p>,
      dataIndex: "email",
      key: "email",
      width: "20%",
      render: (email: any, id: any) => {
        return (
          <p
            style={{
              width: "100%",
              textAlign: "center",
              cursor: "pointer",
              marginBottom: 0,
            }}
          >
            {email == null ? "-" : email}
          </p>
        );
      },
    },
    // {
    //   title: <p style={{ marginBottom: 0, textAlign: "center" }}>Email</p>,
    //   dataIndex: "email",
    //   key: "email",
    //   width: "17%",
    //   render: (isActive: Date, id: any) => {
    //     let date = new Date(isActive);
    //     return (
    //       <p
    //         style={{
    //           width: "100%",
    //           textAlign: "center",
    //           cursor: "pointer",
    //           marginBottom: 0,
    //         }}
    //       >
    //         {date.getFullYear() + "/" + date.getMonth() + "/" + date.getDay()}
    //       </p>
    //     );
    //   },
    // },
    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>Description</p>
      ),
      dataIndex: "description",
      key: "description",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <Paragraph
            ellipsis={{
              rows: 2,
            }}
            style={{ marginBottom: 0 }}
          >
            <p
              style={{
                width: "100%",
                textAlign: "center",
                marginBottom: 0,
                color: "#525f7f",
              }}
            >
              {isActive + ""}
            </p>
          </Paragraph>
        );
      },
    },

    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>
          complaint Catgory
        </p>
      ),
      dataIndex: "complaintCatgory",
      key: "complaintCatgory",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive?.name + ""}
          </p>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>complaits</p>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getComplaintsWithPatinations?.data?.items}
          columns={columns}
          loading={getComplaintsWithPatinations.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={10}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
    </Row>
  );
};

export default TablePartner;

//fjidfjfjd
