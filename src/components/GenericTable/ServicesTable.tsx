import {
  CaretDownOutlined,
  DeleteFilled,
  DeleteOutlined,
  EditFilled,
  EditOutlined,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Menu,
  Button,
  Dropdown,
  notification,
} from "antd";
import { useRouter } from "next/router";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getPostWithPatination } from "../../react-query-services/services";
import { PostService } from "../../services/services";
import EditServices from "../../components/ModalEditComponent/services";
import Table from "../GenericTable/index";
const TablePost: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);
  const query = useQueryClient();
  const Router = useRouter();
  const { id } = Router.query;

  const getPostwithPatination = useQuery(
    ["getAllServicesWithPatination", props.search, skipcount, id],
    () => getPostWithPatination(props.search, skipcount, id),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const [showModalEditServices, setModalEditServices] =
    useState<boolean>(false);
  const [ids, setId] = useState<any>();

  const closeEditServicesHandler = () => {
    setModalEditServices(false);
  };

  useEffect(() => {
    console.log(getPostwithPatination?.data);
    if (
      totalCount < getPostwithPatination?.data?.totalCount ||
      totalCount > getPostwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPostwithPatination?.data?.totalCount);
    }
  }, [getPostwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 4;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];
  const UpdateServicesHandler = (id: any) => {
    setId(id);
    setModalEditServices(true);
  };
  const DeletedHandler = (id: any) => {
    PostService.DeleteServicesCategories(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Services Deleted Successfully ",
        });
        getPostwithPatination.refetch();
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });

        getPostwithPatination.refetch();
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    PostService.SwitchActivationServices({ id: id, isActive: !isActive }).then(
      (res) => {
        if (res?.result == null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else if (res.result != null) {
          notification.open({
            message: "The Service Switch Activated Successfully",
          });
          getPostwithPatination.refetch();
        }
      }
    );
  };

  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => UpdateServicesHandler(id)}
              >
                Edit
              </a>
            ),
            icon: <i className="fa fa-edit icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => SwitchActivationHandler(id, isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "20%",
      render: (attachment: any) => {
        return (
          <Image
            src={
              attachment?.length > 0
                ? attachment[0]?.url + ""
                : "../assets/22.jpg"
            }
            alt=""
            width={100}
            height={100}
            preview={false}
            style={{
              borderRadius: "50%",

              padding: 4,
            }}
            className="table-images"
          ></Image>
        );
      },
    },
    {
      title: "  Name",
      dataIndex: "name",
      key: "title",
      width: "20%",
    },

    {
      title: "Active",
      dataIndex: "isActive",
      key: "isActive",
      width: "15%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center" }}>{isActive + ""}</p>
        );
      },
    },

    {
      title: "Operations",
      key: "address",
      width: "5%",
      dataIndex: "id",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>
                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>services</p>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getPostwithPatination?.data?.items}
          columns={columns}
          loading={getPostwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={4}
          onChange={(e) => onchangeHandler(e, 4)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditServices
          isshow={showModalEditServices}
          id={ids}
          cancleshowModla={closeEditServicesHandler}
          parentId={id}
          getPostwithPatination={getPostwithPatination}
        ></EditServices>
      </Row>
    </Row>
  );
};

export default TablePost;
