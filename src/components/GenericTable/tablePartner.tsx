import {
  EditOutlined,
  DeleteOutlined,
  CaretDownOutlined,
  DeleteFilled,
  EditFilled,
} from "@ant-design/icons";
import {
  Row,
  Col,
  Pagination,
  Image,
  Button,
  Dropdown,
  Menu,
  notification,
  Typography,
} from "antd";
import { FC, useEffect, useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import { getPartnerWithPatination } from "../../react-query-services/partner";
import { categoryService } from "../../services/Partner";
import Link from "next/link";
import Table from "../GenericTable/index";
import EditPartnerModal from "../ModalEditComponent/EditPartnerModal";
import Paragraph from "antd/lib/skeleton/Paragraph";
import { Report } from "../../model/Report";
import { ReportService } from "../../services/Report";
const TablePartner: FC<{ search: any }> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [skipcount, SetSkipCount] = useState<number>(0);
  const [totalCount, SetTotalCount] = useState<any>(1);

  const [showModalEditPartner, setModalEditPartner] = useState<boolean>(false);
  const [id, setId] = useState<any>();
  const { Paragraph } = Typography;

  const query = useQueryClient();
  const getPartnerwithPatination = useQuery(
    ["getAllPartnerWithPatination", props.search, skipcount],
    () => getPartnerWithPatination(props.search, skipcount),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const EditHandler = (id: any) => {
    console.log(id, " id  id ");
  };
  const closeEditPartnerHandler = () => {
    setModalEditPartner(false);
  };

  const ExpoertToCancelHandler = (e: any) => {
    let obj: Report = {
      partnersExport: true,
    };
    ReportService.ExportToExcel(obj).then((res: any) => {
      console.log(res, "res");
      window.open(res?.result?.url);
    });
  };

  const DeletedHandler = (id: any) => {
    categoryService.DeletePostCategories(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Services Deleted Successfully ",
        });
        query.invalidateQueries("getAllPartnerWithPatination");
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    console.log(isActive, "partner");
    categoryService
      .SwitchActivationPartner({ id: id, isActive: !isActive })
      .then((res) => {
        if (res?.result == null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else {
          notification.open({
            message: "The Partner Switch Activated Successfully",
          });
          query.invalidateQueries("getAllPartnerWithPatination");
        }
      });
  };
  const UpdatePartnerHandler = (id: any) => {
    setId(id);
    setModalEditPartner(true);
  };
  const menu = (id: any, isActive: any) => {
    return (
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => UpdatePartnerHandler(id)}
              >
                Edit
              </a>
            ),
            icon: <i className="fa fa-edit icon-setting"></i>,
            disabled: false,
          },
          {
            key: "2",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => DeletedHandler(id)}
              >
                Delete
              </a>
            ),
            icon: <i className="fa fa-trash icon-setting"></i>,

            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins", fontWeight: 400 }}
                className="icon-link"
                onClick={() => SwitchActivationHandler(id, isActive)}
              >
                Switch Activation
              </a>
            ),
            icon: (
              <i className="fa fa-undo icon-setting" aria-hidden="true"></i>
            ),

            disabled: false,
          },
        ]}
      />
    );
  };
  useEffect(() => {
    console.log(getPartnerwithPatination?.data);
    if (
      totalCount < getPartnerwithPatination?.data?.totalCount ||
      totalCount > getPartnerwithPatination?.data?.totalCount
    ) {
      SetTotalCount(getPartnerwithPatination?.data?.totalCount);
    }
  }, [getPartnerwithPatination?.data, totalCount]);

  const onchangeHandler = (e: any, d: any) => {
    let page = (e - 1) * 10;
    SetSkipCount(page);
  };
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "Attachments",
      dataIndex: "attachments",
      key: "attachments",
      width: "15%",
      render: (attachment: any, id: any) => {
        return (
          <Link href={`/partnerById/${id.id}`} passHref={true}>
            <Image
              src={
                attachment?.length > 0
                  ? attachment[0]?.url + ""
                  : "../assets/22.jpg"
              }
              alt=""
              width={100}
              height={100}
              preview={false}
              style={{
                borderRadius: "50%",
                cursor: "pointer",
                padding: 4,
              }}
              className="table-images"
            ></Image>
          </Link>
        );
      },
    },
    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Name</p>,
      dataIndex: "name",
      key: "name",
      width: "17%",
      render: (isActive: any, id: any) => {
        `${console.log(id)}`;
        return (
          <Link href={`/partnerById/${id.id}`} passHref={true}>
            <p
              style={{
                width: "100%",
                textAlign: "center",
                cursor: "pointer",
                marginBottom: 0,
              }}
            >
              {isActive + ""}
            </p>
          </Link>
        );
      },
    },
    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>Description</p>
      ),
      dataIndex: "description",
      key: "description",
      width: "20%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <Paragraph
            ellipsis={{
              rows: 2,
            }}
            style={{ marginBottom: 0 }}
          >
            <p
              style={{
                width: "100%",
                textAlign: "center",
                marginBottom: 0,
                color: "#525f7f",
              }}
            >
              {isActive + ""}
            </p>
          </Paragraph>
        );
      },
    },
    {
      title: (
        <p style={{ marginBottom: 0, textAlign: "center" }}>Partner Category</p>
      ),
      dataIndex: "partnerCatgory",
      key: "partnerCatgory",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive?.name + ""}
          </p>
        );
      },
    },

    {
      title: <p style={{ marginBottom: 0, textAlign: "center" }}>Activated</p>,
      dataIndex: "isActive",
      key: "isActive",
      width: "17%",
      render: (isActive: any) => {
        `${console.log(isActive)}`;
        return (
          <p style={{ width: "100%", textAlign: "center", marginBottom: 0 }}>
            {isActive + ""}
          </p>
        );
      },
    },

    {
      title: "Operations",
      key: "id",
      dataIndex: "id",
      width: "15%",
      render: (id: any, body: any) => {
        return (
          <Row justify="center" style={{ width: "100%", gap: 20 }}>
            <Dropdown
              overlay={() => menu(id, body.isActive)}
              placement="bottomRight"
            >
              <Button
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  width: "75%",
                  padding: "10px 6px",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 13, color: "#648dea" }}
                ></i>

                <CaretDownOutlined
                  style={{ fontSize: 13, color: "#648dea" }}
                ></CaretDownOutlined>
              </Button>
            </Dropdown>
          </Row>
        );
      },
    },
  ];
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <Row
          justify="space-between"
          style={{ width: "100%", alignItems: "center" }}
        >
          <p style={{ marginBottom: 0, fontFamily: "Poppins" }}>partners</p>
          <Button
            onClick={ExpoertToCancelHandler}
            style={{
              backgroundColor: "#5e72e4",
              color: "white",
              fontWeight: 400,
              borderRadius: 6,
              border: "none",
            }}
          >
            Export To Excel
          </Button>
        </Row>
      </Row>

      <Row style={{ width: "100%" }} className="post">
        <Table
          data={getPartnerwithPatination?.data?.items}
          columns={columns}
          loading={getPartnerwithPatination.isLoading}
        ></Table>
        <Pagination
          defaultCurrent={1}
          total={totalCount}
          pageSize={10}
          onChange={(e) => onchangeHandler(e, 10)}
          hideOnSinglePage={false}
        />
      </Row>
      <Row>
        <EditPartnerModal
          isshow={showModalEditPartner}
          id={id}
          cancleshowModla={closeEditPartnerHandler}
          parentId={null}
          getAllPartnerCategories={() => {}}
        ></EditPartnerModal>
      </Row>
    </Row>
  );
};

export default TablePartner;

//fjidfjfjd
