import {
  Row,
  Col,
  Form,
  Input,
  Button,
  notification,
  Upload,
  message,
} from "antd";
import { useMutation } from "react-query";
import { AddPartnerCategories } from "../../react-query-services/Partner-gategories";
import {
  PartnerCategories_Req,
  Translation,
} from "../../model/Partner-Categories";
import { UploadAttachment } from "../../services/Upload-file";
import NextRouter from "next/router";
import { PlusOutlined } from "@ant-design/icons";
import { useState } from "react";
const PartnerCategoriesForm = () => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const PartnerCategories = useMutation(AddPartnerCategories, {
    onSuccess: (res: any) => {
      setLoading(false);
      form.resetFields();
      if (res?.error == null) {
        notification.open({
          message: "Add Partner Categories Successfully",
        });
        NextRouter.push("/PartnerCategories");
      } else if (res?.error != null) {
        notification.open({
          message: res.error?.message,
        });
      }
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  const onFinish = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 6 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);
    UploadAttachment.UploadAttachment(formData).then((res: any) => {
      let body: PartnerCategories_Req;
      let objen: Translation;
      let objar: Translation;
      body = {
        translations: [],
        attachment: 0,
      };
      objen = {
        language: "en",
        name: e.nameen,
      };
      objar = {
        language: "ar",
        name: e.namear,
      };
      body.translations.push(objar);
      body.translations.push(objen);
      body.attachment = res.result.id as number;
      PartnerCategories.mutateAsync(body);
    });
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new partner categories </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name-en"}
              name="nameen"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the name-en Required ",
                },
              ]}
            >
              <Input placeholder="name-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name-ar"}
              name="namear"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the name-ar Required ",
                },
              ]}
            >
              <Input placeholder="name-ar"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Partner Categorie Image "}
              name="image"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the partner Categorie image Required ",
                },
              ]}
            >
              <Upload
                listType="picture"
                className="upload-list-inline"
                accept="image/png, image/jpeg , image/jpeg"
                multiple={false}
              >
                <Button
                  className="button-upload-image-partner"
                  icon={<PlusOutlined style={{ fontSize: 20 }} />}
                ></Button>
              </Upload>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#2dce89",
                }}
                htmlType={"submit"}
                loading={PartnerCategories.isLoading}
                className="add-button"
              >
                Add new Partner Category
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PartnerCategoriesForm;
