import { Row, Col, Form, Input, Button } from "antd";
import { useMutation } from "react-query";
import { AddPost } from "../../react-query-services/TermsAndConditions";
import { TermsAndConditions_Req } from "../../model/TermsAndConditions";
import { notification } from "antd";
import NextRouter from "next/router";
// import JoditEditor from "jodit-react";
import Spiner from "../GenericTable/Sping";

import dynamic, { noSSR } from "next/dynamic";

// import JoditEditor from "./jodit";

const JoditEditor = dynamic(() => import("jodit-react"), {
  ssr: false,
});

import { ReactElement, Suspense, useEffect, useMemo, useState } from "react";
import React from "react";
const PostForm = () => {
  const { TextArea } = Input;
  const [content, setContent] = useState("");
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(true);
  const PostCategories = useMutation(AddPost, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Add Terms And Conditions Successfully",
      });
      //   NextRouter.push("/complaintCategories");
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  const onFinish = (e: TermsAndConditions_Req) => {
    // let e: Privacy_Req;
    // body = {
    //   arPrivacyPolicy: "",
    //   enPrivacyPolicy: "",
    // };

    console.log("value", e);

    PostCategories.mutateAsync(e);
  };

  useEffect(() => {
    setLoading(false);
  }, []);

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new terms and conditions </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish} form={form}>
        {loading ? (
          <Spiner></Spiner>
        ) : (
          <Row
            justify="space-between"
            style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
          >
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={"terms and conditions en :"}
                name="enTermsAndConditions"
                labelCol={{ span: 24 }}
                style={{ width: "100%", zIndex: 1000 }}
                rules={[
                  {
                    required: true,
                    message: "the privacypolicy-en Required ",
                  },
                ]}
              >
                <React.Fragment>
                  <JoditEditor
                    value={content}
                    onBlur={(newContent) => setContent(newContent)}
                  />
                </React.Fragment>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={"terms and Conditions ar :"}
                name="arTermsAndConditions"
                labelCol={{ span: 24 }}
                style={{ width: "100%", zIndex: 1000 }}
                rules={[
                  {
                    required: true,
                    message: "the TermsAndConditions  ar Required ",
                  },
                ]}
              >
                <Suspense fallback={`Loading...`}>
                  <React.Fragment>
                    <JoditEditor
                      value={content}
                      onBlur={(newContent) => setContent(newContent)}
                    />
                  </React.Fragment>
                </Suspense>
              </Form.Item>
            </Col>

            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              style={{ width: "100%", justifyContent: "end", display: "flex" }}
            >
              <Form.Item style={{ width: "100", justifyContent: "end" }}>
                <Button
                  style={{
                    padding: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    color: "white",
                    borderRadius: "4px",
                    border: "none",
                    backgroundColor: "#2dce89",
                  }}
                  htmlType="submit"
                  loading={PostCategories.isLoading}
                  className="add-button"
                >
                  Add new Terms And Conditions
                </Button>
              </Form.Item>
            </Col>
          </Row>
        )}
      </Form>
    </Row>
  );
};
export default PostForm;
