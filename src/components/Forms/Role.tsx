import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Select,
  notification,
  message,
} from "antd";
import { AddRole } from "../../react-query-services/roles";
import { useMutation, useQuery } from "react-query";
import { Translation, lang_req } from "../../model/lang";
import { Role_Req } from "../../model/role";
import { getAllPermissionsWithoutPatination } from "../../react-query-services/roles";
import NextRouter from "next/router";
const UserForm = () => {
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();

  const getAllPermissionsWithoutPatinations = useQuery(
    "getAllPermissionsWithoutPatination",
    getAllPermissionsWithoutPatination,
    {
      onError: () => {},
      onSuccess: () => {},
    }
  );

  const addLang = useMutation("add-Role", AddRole, {
    onSuccess: (res: any) => {
      if (res.error == null) {
        notification.open({ message: "The Role add seccessfully" });
        form.resetFields();
        NextRouter.push("/Role");
      } else {
        notification.open({ message: res.error.message + "" });
      }
    },
  });

  const onFinisHandler = (e: Role_Req) => {
    console.log(e, "events events");
    addLang.mutateAsync(e);
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new role</p>
      </Row>
      <Form style={{ width: "100%" }} form={form} onFinish={onFinisHandler}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name"}
              name="name"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the name Required ",
                },
              ]}
            >
              <Input placeholder=" name-ar"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"display Name"}
              name="displayName"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The display Name Required ",
                },
              ]}
            >
              <Input placeholder="display Name"></Input>
            </Form.Item>
          </Col>
          {/* <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"normalized Name"}
              name="normalizedName"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The normalized Name Required ",
                },
              ]}
            >
              <Input placeholder="normalized Name"></Input>
            </Form.Item>
          </Col> */}
          {/* <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"description"}
              name="Description"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The Description Required ",
                },
              ]}
            >
              <Input placeholder="Description"></Input>
            </Form.Item>
          </Col> */}
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Granted Permissions"}
              name="grantedPermissions"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The Granted Permissions Required ",
                },
              ]}
            >
              <Select
                mode="multiple"
                showSearch={true}
                optionFilterProp="children"
                filterOption={(input, option: any) =>
                  option!.children.includes(input)
                }
                filterSort={(optionA: any, optionB: any) =>
                  optionA!.children
                    .toLowerCase()
                    .localeCompare(optionB!.children.toLowerCase())
                }
              >
                {getAllPermissionsWithoutPatinations?.data?.items?.map(
                  (ele: any) => {
                    return (
                      <Option value={ele.name} key={ele?.id}>
                        {ele.displayName}
                      </Option>
                    );
                  }
                )}
              </Select>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#f3a4b5",
                }}
                className={"add-button"}
                htmlType="submit"
                loading={addLang.isLoading}
              >
                Add New Role
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default UserForm;
