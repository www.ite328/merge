import { Row } from "antd";
import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import JoditEditor from "jodit-react";
const Jodit = () => {
  const [content, setContent] = useState("");
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
  }, []);
  return (
    <Row style={{ width: "100%" }}>
      <React.Fragment>
        <JoditEditor
          value={content}
          onBlur={(newContent) => setContent(newContent)}
        />
      </React.Fragment>
    </Row>
  );
};

export default  Jodit;
