import { PlusOutlined } from "@ant-design/icons";
import { Row, Col, Form, Button, Upload, notification } from "antd";
import { AddSlider } from "../../react-query-services/slider";
import { useMutation } from "react-query";
import { UploadAttachment } from "../../services/Upload-file";

import { useState } from "react";
import NextRouter from "next/router";
const PostForm = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const addPost = useMutation("Add_Slider", AddSlider, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "The Slider add successfully",
      });
      NextRouter.push("/slider");
      setLoading(false);
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("refType", 7 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);
    UploadAttachment.UploadAttachment(formData).then((res: any) => {
      console.log(res?.result);
      let body: any = {
        attachments: [],
      };
      body.attachments.push(res?.result?.id as number);
      addPost.mutateAsync(body);
    });
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new Slider</p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinishHandler} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Slider Image "}
              name="image"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Image Slider  Required ",
                },
              ]}
            >
              <Upload
                listType="picture"
                className="upload-list-inline"
                accept="image/png, image/jpeg , image/jpeg"
                multiple={false}
              >
                <Button
                  className="button-upload-image-partner"
                  icon={<PlusOutlined style={{ fontSize: 20 }} />}
                ></Button>
              </Upload>
            </Form.Item>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: "15px 30px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "rgb(94, 114, 228)",
                }}
                htmlType={"submit"}
                className={"add-button"}
                loading={loading}
              >
                Add New Slider
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
