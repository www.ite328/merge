import { Row, Col, Form, Input, Button, notification } from "antd";
import { useMutation } from "react-query";
import { AddCountry } from "../../react-query-services/country";
import { Translation, country_req } from "../../model/Countries";
import NextRouter from "next/router";
const PostForm = () => {
  const AddCountries = useMutation(AddCountry, {
    onSuccess: (res: any) => {
      notification.open({
        message: "the Country add successfully",
      });
      NextRouter.push("/Countries");
    },
    onError: (res: any) => {
      notification.open({
        message: res + "",
      });
    },
  });

  const onFinishHandler = (e: any) => {
    let body: country_req;
    let objen: Translation;
    let objar: Translation;
    body = {
      translations: [],
    };
    objen = {
      language: "en",
      name: e.nameen,
    };
    objar = {
      language: "ar",
      name: e.namear,
    };
    body.translations.push(objar);
    body.translations.push(objen);

    AddCountries.mutateAsync(body);
  };

  const { TextArea } = Input;
  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add New Country </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinishHandler}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name-en"}
              name="nameen"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the name-en Required ",
                },
              ]}
            >
              <Input placeholder="name-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name-ar"}
              name="namear"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the name-ar Required ",
                },
              ]}
            >
              <Input placeholder="name-ar"></Input>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "rgb(245, 54, 92)",
                }}
                loading={AddCountries.isLoading}
                htmlType="submit"
                className="add-button"
              >
                Add new Country
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
