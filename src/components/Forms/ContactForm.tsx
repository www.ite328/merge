import { Row, Col, Form, Input, Button } from "antd";
import { useMutation } from "react-query";
import { AddPost } from "../../react-query-services/ContactUs";
import { ContactUs_Req } from "../../model/ContactUs";
import { notification } from "antd";
import NextRouter from "next/router";
import JoditEditor from "jodit-react";
import { useMemo, useState } from "react";
const PostForm = () => {
  const { TextArea } = Input;
  const [content, setContent] = useState("");
  const [form] = Form.useForm();

  const PostCategories = useMutation(AddPost, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Add Contact Us Successfully",
      });
      //   NextRouter.push("/complaintCategories");
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  const onFinish = (e: ContactUs_Req) => {
    PostCategories.mutateAsync(e);
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new contact us </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"About Application en :"}
              name="enAboutApplication"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the AboutApplication-en Required ",
                },
              ]}
            >
              <Input placeholder="About Application-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"About Application ar :"}
              name="arAboutApplication"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the About Application-ar Required ",
                },
              ]}
            >
              <Input placeholder=" About Application-ar"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Email Link"}
              name="emailLink"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the  Email Link Required ",
                },
              ]}
            >
              <Input placeholder=" Email Link"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Facbook Link"}
              name="facbookLink"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the  Facbook Link Required ",
                },
              ]}
            >
              <Input placeholder="Facbook Link"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Instgram Link"}
              name="instgramLink"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the  Instgram Link Required ",
                },
              ]}
            >
              <Input placeholder="Instgram Link"></Input>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#2dce89",
                }}
                htmlType="submit"
                loading={PostCategories.isLoading}
                className="add-button"
              >
                Add new Contact Us
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
