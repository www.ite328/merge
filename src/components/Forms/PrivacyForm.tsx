import { Row, Col, Form, Input, Button } from "antd";
import { useMutation } from "react-query";
import { AddPost } from "../../react-query-services/PrivacyPolicy";
import { Privacy_Req } from "../../model/PrivacyPolicy";
import { notification } from "antd";
import Spiner from "../GenericTable/Sping";
import SunEditorCore from "suneditor/src/lib/core";
import NextRouter from "next/router";
// import JoditEditor from "jodit-react";
//
import { buttonList } from "suneditor-react";
import dynamic from "next/dynamic";

const JoditEditor = dynamic(() => import("jodit-react"), {
  ssr: false,
});
import { Suspense } from "react";
import {
  ComponentClass,
  MutableRefObject,
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import React from "react";

const PostForm = () => {
  const { TextArea } = Input;
  const [content, setContent] = useState("");
  const [loading, setLoading] = useState<boolean>(true);
  const [form] = Form.useForm();

  const editor = useRef<SunEditorCore>();

  const getSunEditorInstance = (sunEditor: any) => {
    editor.current = sunEditor;
  };
  const PostCategories = useMutation(AddPost, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Add Privacy Policy Successfully",
      });
      //   NextRouter.push("/complaintCategories");
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  useEffect(() => {
    setLoading(false);
  }, []);

  const onFinish = (e: Privacy_Req) => {
    // let e: Privacy_Req;
    // body = {
    //   arPrivacyPolicy: "",
    //   enPrivacyPolicy: "",
    // };

    console.log("value", e);

    PostCategories.mutateAsync(e);
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new privacy </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish} form={form}>
        {loading ? (
          <Spiner></Spiner>
        ) : (
          <Row
            justify="space-between"
            style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
          >
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={"privacy policy en :"}
                name="enPrivacyPolicy"
                labelCol={{ span: 24 }}
                style={{ width: "100%", zIndex: 1000 }}
                rules={[
                  {
                    required: true,
                    message: "the privacypolicy-en Required ",
                  },
                ]}
              >
                <Suspense fallback={`Loading...`}>
                  <React.Fragment>
                    <JoditEditor
                      value={content}
                      onBlur={(newContent) => setContent(newContent)}
                    />
                  </React.Fragment>
                </Suspense>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={"privacy policy ar :"}
                name="arPrivacyPolicy"
                labelCol={{ span: 24 }}
                style={{ width: "100%", zIndex: 1000 }}
                rules={[
                  {
                    required: true,
                    message: "the privacypolicy-ar Required ",
                  },
                ]}
              >
                <React.Fragment>
                  <JoditEditor
                    value={content}
                    onBlur={(newContent) => setContent(newContent)}
                  />
                </React.Fragment>
              </Form.Item>
            </Col>

            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              style={{ width: "100%", justifyContent: "end", display: "flex" }}
            >
              <Form.Item style={{ width: "100", justifyContent: "end" }}>
                <Button
                  style={{
                    padding: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    color: "white",
                    borderRadius: "4px",
                    border: "none",
                    backgroundColor: "#2dce89",
                  }}
                  htmlType="submit"
                  loading={PostCategories.isLoading}
                  className="add-button"
                >
                  Add new Privacy
                </Button>
              </Form.Item>
            </Col>
          </Row>
        )}
      </Form>
    </Row>
  );
};
export default PostForm;
