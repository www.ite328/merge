import { PlusOutlined } from "@ant-design/icons";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Select,
  Upload,
  notification,
} from "antd";
import { UploadAttachment } from "../../services/Upload-file";
import { User_Req } from "../../model/user";
import { AddUser } from "../../react-query-services/user";
import { getCountryWithoutPatination } from "../../react-query-services/cities";
import { getAllRolesWithoutPatination } from "../../react-query-services/roles";
import { useMutation, useQuery } from "react-query";
import React, { useState } from "react";
import NextRouter from "next/router";
import PhoneInput from "react-phone-input-2";

const UserForm = () => {
  const { TextArea } = Input;
  const { Option } = Select;
  const [loading, setLoading] = useState<boolean>(false);
  const [form] = Form.useForm();
  const [phone, setPhone] = useState<any>();
  const getAllCityWithOutPatination = useQuery(
    "getCountryWithoutPatination",
    getCountryWithoutPatination,
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );
  const getAllRolesWithoutPatinations = useQuery(
    "getAllRolesWithoutPatination",
    getAllRolesWithoutPatination,
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const addUser = useMutation(AddUser, {
    onSuccess: (res: any) => {
      console.log(addUser.data, addUser.isError, res);
      if (res.result != null) {
        notification.open({
          message: "Add Admin successfully",
        });
        form.resetFields();
        NextRouter.push("/admins");
        if (res.result.userType == 2) {
          // NextRouter.push("/userPartner");
        } else if (res.result.userType == 4) {
          // NextRouter.push("/user");
        }
      } else {
        notification.open({
          message: res.error.message + "",
        });
      }
      setLoading(false);
    },
    onError: (res: any) => {
      console.log(res);
      setLoading(false);
      notification.open({
        message: res + "",
      });
    },
  });
  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 5 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);
    UploadAttachment.UploadAttachment(formData).then((res: any) => {
      let body: User_Req = {
        attachments: [],
        isActive: true,
        name: e.name,
        password: e.password,
        phoneNumber: e.phonenumber,
        surname: e.surname,
        userName: e.username,
        userType: 0,
        cityId: e.cityId,
        gender: e.gender,
        partnerId: null,
        dialCode: "",
        roleNames: e.roleNames,
      };
      body.attachments.push(res?.result?.id as number);
      const dial = e.phonenumber.slice(0, 3);
      const phoneNumber = e.phonenumber.slice(3);
      console.log(phoneNumber, dial);
      body.dialCode = dial;
      body.phoneNumber = phoneNumber;
      // let objectArabic: Translation = {
      //   description: e.description_ar,
      //   name: e.name_ar,
      //   language: "ar",
      // };
      // let objectEnglish: Translation = {
      //   description: e.description_en,
      //   name: e.name_en,
      //   language: "en",
      // };
      // body.translations.push(objectArabic, objectEnglish);
      addUser.mutateAsync(body);
    });
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new admin</p>
      </Row>
      <Form style={{ width: "100%" }} form={form} onFinish={onFinishHandler}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name"}
              name="name"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The Name Required ",
                },
              ]}
            >
              <Input placeholder="Name"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"sur name"}
              name="surname"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The surname Required ",
                },
              ]}
            >
              <Input placeholder="sur name"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"user name"}
              name="username"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the User Name Required ",
                },
              ]}
            >
              <Input placeholder="User Name"></Input>
            </Form.Item>
          </Col>

          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"password"}
              name="password"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The password Required ",
                },
              ]}
            >
              <Input placeholder="password" type="password"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"phone number"}
              name="phonenumber"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The Phone Number Required ",
                },
              ]}
            >
              <PhoneInput
                country={"kw"}
                value={phone}
                onChange={(phone) => setPhone({ phone })}
                inputStyle={{ width: "100%" }}
              />
            </Form.Item>
          </Col>

          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Gender"}
              name="gender"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The Gender Type Required ",
                },
              ]}
            >
              <Select>
                <Option value={1}>{"Male"}</Option>
                <Option value={2}>{"Femal"}</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"City"}
              name="cityId"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The City Type Required ",
                },
              ]}
            >
              <Select
                showSearch={true}
                optionFilterProp="children"
                filterOption={(input, option: any) =>
                  option!.children.includes(input)
                }
                filterSort={(optionA: any, optionB: any) =>
                  optionA!.children
                    .toLowerCase()
                    .localeCompare(optionB!.children.toLowerCase())
                }
              >
                {getAllCityWithOutPatination?.data?.items.map((ele: any) => {
                  return (
                    <Option value={ele.id} key={ele.id}>
                      {ele.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Role"}
              name="roleNames"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "The Role Required ",
                },
              ]}
            >
              <Select
                showSearch={true}
                mode="multiple"
                optionFilterProp="children"
                filterOption={(input, option: any) =>
                  option!.children.includes(input)
                }
                filterSort={(optionA: any, optionB: any) =>
                  optionA!.children
                    .toLowerCase()
                    .localeCompare(optionB!.children.toLowerCase())
                }
              >
                {getAllRolesWithoutPatinations?.data?.items.map((ele: any) => {
                  return (
                    <Option value={ele.name} key={ele.id}>
                      {ele.displayName}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={" user image "}
              name="image"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the user image  required ",
                },
              ]}
            >
              <Upload
                listType="picture"
                className="upload-list-inline"
                accept="image/png, image/jpeg , image/jpeg"
                multiple={false}
              >
                <Button
                  className="button-upload-image-partner"
                  icon={<PlusOutlined style={{ fontSize: 20 }} />}
                ></Button>
              </Upload>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#f3a4b5",
                }}
                className="add-button"
                loading={loading}
                htmlType={"submit"}
              >
                Add New Admin
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default UserForm;
