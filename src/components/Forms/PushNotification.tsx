import { Row, Col, Form, Input, Button, Select } from "antd";
import { useMutation } from "react-query";
import { AddSlider } from "../../react-query-services/PushNotification";
import { PostCategories_Req, Translation } from "../../model/post-categories";
import { notification } from "antd";
import NextRouter from "next/router";
const PostForm = () => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const { Option } = Select;
  const PostCategories = useMutation(AddSlider, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Add Notification  Successfully",
      });
      NextRouter.push("/notification");
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  const onFinish = (e: any) => {
    let body: any;
    let objen: any;
    let objar: any;
    body = {
      translations: [],
      destination: +e.destination,
    };
    objen = {
      language: "en",
      message: e.message_en,
    };
    objar = {
      language: "ar",
      message: e.message_ar,
    };
    body.translations.push(objar);
    body.translations.push(objen);

    PostCategories.mutateAsync(body);
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new notification </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Message-en"}
              name="message_en"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the message-en Required ",
                },
              ]}
            >
              <Input placeholder="Message-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Message-ar"}
              name="message_ar"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Message-ar Required ",
                },
              ]}
            >
              <Input placeholder="Message-ar"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Destination"}
              name="destination"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the destination Required ",
                },
              ]}
            >
              <Select>
                {" "}
                <Option value={0}>All User</Option>
                <Option value={1}>Client</Option>
                <Option value={2}>Partner</Option>
                <Option value={4}>Sign Interpreter </Option>
                <Option value={5}>FreelanceInterpreter </Option>
              </Select>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#2dce89",
                }}
                htmlType="submit"
                loading={PostCategories.isLoading}
                className="add-button"
              >
                Add new Notification
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
