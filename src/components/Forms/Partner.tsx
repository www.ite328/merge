import { UploadOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Select,
  Upload,
  notification,
} from "antd";

import { getPartnerCategories } from "../../react-query-services/Partner-gategories";
import { UploadAttachment } from "../../services/Upload-file";
import "leaflet/dist/leaflet.css";
import { useQuery, useMutation } from "react-query";

import { useEffect, useState } from "react";
import { Parent_Partner_Req, Translation } from "../../model/partner";
import dynamic from "next/dynamic";
import { AddParentPartner } from "../../react-query-services/partner";
import NextRouter from "next/router";
const AddPartner = () => {
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const [showMap, setShowMap] = useState<boolean>(false);
  const MapWithOutSSR = dynamic(() => import("../Map"), {
    ssr: false,
  });
  const getAllPartnerCategories = useQuery(
    "Partner-Categories",
    getPartnerCategories
  );
  useEffect(() => {
    setShowMap(true);
  }, [getAllPartnerCategories]);
  const AddPartner = useMutation("Add-Partner", AddParentPartner, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "The partner add successfully",
      });
      NextRouter.push("/Partner");
      setLoading(false);
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 2 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);

    UploadAttachment.UploadAttachment(formData)
      .then((res: any) => {
        let body: Parent_Partner_Req = {
          attachments: [],
          partnerCategoryId: e.partnerCategoriesId,
          translations: [],
          clientLanguageId: "1",
        };
        let objectArabic: Translation = {
          description: e.description_ar,
          name: e.name_ar,
          language: "ar",
        };
        let objectEnglish: Translation = {
          description: e.description_en,
          name: e.name_en,
          language: "en",
        };
        body.translations.push(objectArabic, objectEnglish);
        body.attachments.push(res?.result?.id as number);
        AddPartner.mutateAsync(body);
      })
      .catch((res) => {});
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new partner</p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinishHandler} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name-en"}
              name="name_en"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the name-en Required ",
                },
              ]}
            >
              <Input placeholder="name-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"name-ar"}
              name="name_ar"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the name-ar Required ",
                },
              ]}
            >
              <Input placeholder="name-ar"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Description-ar"}
              name="description_ar"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Description-ar  Required ",
                },
              ]}
            >
              <TextArea placeholder="Description-ar"></TextArea>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Description-en"}
              name="description_en"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Description-en  Required ",
                },
              ]}
            >
              <TextArea placeholder="Description-en"></TextArea>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"partner Category"}
              name="partnerCategoriesId"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the partnerCategory  Required ",
                },
              ]}
            >
              <Select
                showSearch={true}
                optionFilterProp="children"
                filterOption={(input, option: any) =>
                  option!.children.includes(input)
                }
                filterSort={(optionA: any, optionB: any) =>
                  optionA!.children
                    .toLowerCase()
                    .localeCompare(optionB!.children.toLowerCase())
                }
              >
                {getAllPartnerCategories?.data?.map((ele: any) => {
                  return (
                    <Option value={ele?.id} key={ele?.id}>
                      {ele?.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>
          {/* <div style={{ width: "100%", height: "100vh" }}>
            <MapWithOutSSR></MapWithOutSSR>
          </div> */}
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Partner Image "}
              name="image"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the partner imageId Required ",
                },
              ]}
            >
              <Upload
                listType="picture"
                className="upload-list-inline"
                accept="image/png, image/jpeg , image/jpeg"
                multiple={false}
              >
                <Button
                  className="button-upload-image-partner"
                  icon={<PlusOutlined style={{ fontSize: 20 }} />}
                ></Button>
              </Upload>
            </Form.Item>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "rgb(251, 99, 64)",
                  position: "relative",
                }}
                htmlType={"submit"}
                className="add-button"
                loading={loading}
              >
                Add New Partner
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default AddPartner;
