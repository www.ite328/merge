import { Row, Col, Form, Input, Button } from "antd";
import { useMutation } from "react-query";
import { complaintCategories } from "../../react-query-services/complaint-categories";
import {
  complaintCategories_Req,
  Translation,
} from "../../model/complaint-categories";
import { notification } from "antd";
import NextRouter from "next/router";
import JoditEditor from "jodit-react";
import { useMemo, useState } from "react";
const PostForm = () => {
  const { TextArea } = Input;
  const [content, setContent] = useState("");
  const [form] = Form.useForm();

  const PostCategories = useMutation(complaintCategories, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Add complaint Categories Successfully",
      });
      NextRouter.push("/complaintsCategories");
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  const onFinish = (e: any) => {
    let body: complaintCategories_Req;
    let objen: Translation;
    let objar: Translation;
    body = {
      translations: [],
    };
    objen = {
      language: "en",
      name: e.nameen,
    };
    objar = {
      language: "ar",
      name: e.namear,
    };
    console.log(e, "value");
    body.translations.push(objar);
    body.translations.push(objen);
    PostCategories.mutateAsync(body);
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new complaint categories </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Name-en"}
              name="nameen"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the Name-en Required ",
                },
              ]}
            >
              <Input placeholder="Name-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Name-ar"}
              name="namear"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Name-ar Required ",
                },
              ]}
            >
              <Input placeholder="Name-ar"></Input>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#2dce89",
                }}
                htmlType="submit"
                loading={PostCategories.isLoading}
                className="add-button"
              >
                Add new Complaint Categories
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
