import { PlusOutlined, UploadOutlined } from "@ant-design/icons";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Upload,
  Select,
  notification,
} from "antd";
import { getPostCategoriesWithOutPatination } from "../../react-query-services/Post-gategories";
import { useMutation, useQuery } from "react-query";
import { UploadAttachment } from "../../services/Upload-file";
import { Post_Req, Translation } from "../../model/posts";
import { AddPost } from "../../react-query-services/Post";
import { useState } from "react";
import NextRouter from "next/router";
const PostForm = () => {
  const { TextArea } = Input;
  const { Option } = Select;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const [showModalEditPost, setModalEditPost] = useState<boolean>(false);
  const [id, setId] = useState<any>();

  const EditPostHandler = () => {
    setModalEditPost(true);
  };

  const closeEditPostHandler = () => {
    setModalEditPost(false);
  };

  const getAllPostCategories = useQuery(
    "Post-Categories",
    getPostCategoriesWithOutPatination
  );

  const addPost = useMutation("Add_Post_Categories", AddPost, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "The post add successfully",
      });
      NextRouter.push("/Post");
      setLoading(false);
    },
    onError: (res: any) => {},
  });

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 1 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);

    UploadAttachment.UploadAttachment(formData).then((res: any) => {
      let body: Post_Req = {
        attachments: [],
        postCategoryId: e.postCategoryId,
        translations: [],
      };
      let objectArabic: Translation = {
        description: e.description_ar,
        title: e.title_ar,
        language: "ar",
      };
      let objectEnglish: Translation = {
        description: e.description_en,
        title: e.title_en,
        language: "en",
      };
      body.translations.push(objectArabic, objectEnglish);
      body.attachments.push(res.result.id as number);
      addPost.mutateAsync(body);
    });
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new post</p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinishHandler} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Title-en"}
              name="title_en"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the title-en Required ",
                },
              ]}
            >
              <Input placeholder="Title-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Title-ar"}
              name="title_ar"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Title-ar Required ",
                },
              ]}
            >
              <Input placeholder="Title-en"></Input>
            </Form.Item>
          </Col>

          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Description-ar"}
              name="description_ar"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Description-ar  Required ",
                },
              ]}
            >
              <TextArea placeholder="Description-ar"></TextArea>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Description-en"}
              name="description_en"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Description-en  Required ",
                },
              ]}
            >
              <TextArea placeholder="Description-en"></TextArea>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Post Category"}
              name="postCategoryId"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the PostCategory  Required ",
                },
              ]}
            >
              <Select
                showSearch={true}
                optionFilterProp="children"
                filterOption={(input, option: any) =>
                  option!.children.includes(input)
                }
                filterSort={(optionA: any, optionB: any) =>
                  optionA!.children
                    .toLowerCase()
                    .localeCompare(optionB!.children.toLowerCase())
                }
              >
                {getAllPostCategories?.data?.map((ele: any) => {
                  return (
                    <Option value={ele?.id} key={ele?.id}>
                      {ele?.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item
              label={"Post Image "}
              name="image"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Image Post  Required ",
                },
              ]}
            >
              <Upload
                listType="picture"
                className="upload-list-inline"
                accept="image/png, image/jpeg , image/jpeg"
                multiple={false}
              >
                <Button
                  className="button-upload-image-partner"
                  icon={<PlusOutlined style={{ fontSize: 20 }} />}
                ></Button>
              </Upload>
            </Form.Item>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: "15px 30px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "rgb(94, 114, 228)",
                }}
                htmlType={"submit"}
                className={"add-button"}
                loading={loading}
              >
                Add New Post
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
