import { Row, Col, Form, Input, Button } from "antd";
import { useMutation } from "react-query";
import { AddPostCategories } from "../../react-query-services/Post-gategories";
import { PostCategories_Req, Translation } from "../../model/post-categories";
import { notification } from "antd";
import NextRouter from "next/router";
const PostForm = () => {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const PostCategories = useMutation(AddPostCategories, {
    onSuccess: (res: any) => {
      form.resetFields();
      notification.open({
        message: "Add post Categories Successfully",
      });
      NextRouter.push("/PostCategories");
    },
    onError: (res: any) => {
      console.log(res);
    },
  });

  const onFinish = (e: any) => {
    let body: PostCategories_Req;
    let objen: Translation;
    let objar: Translation;
    body = {
      translations: [],
    };
    objen = {
      language: "en",
      name: e.titleen,
    };
    objar = {
      language: "ar",
      name: e.titlear,
    };
    body.translations.push(objar);
    body.translations.push(objen);
    PostCategories.mutateAsync(body);
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
      }}
      className="globalFormStyle"
    >
      <Row
        style={{
          padding: 20,
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <p style={{ marginBottom: 0 }}>add new post categories </p>
      </Row>
      <Form style={{ width: "100%" }} onFinish={onFinish} form={form}>
        <Row
          justify="space-between"
          style={{ width: "100%", paddingLeft: 20, paddingRight: 20 }}
        >
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Title-en"}
              name="titleen"
              style={{ width: "100%" }}
              labelCol={{ span: 24 }}
              rules={[
                {
                  required: true,
                  message: "the title-en Required ",
                },
              ]}
            >
              <Input placeholder="Title-en"></Input>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={11} lg={11}>
            <Form.Item
              label={"Title-ar"}
              name="titlear"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
              rules={[
                {
                  required: true,
                  message: "the Title-ar Required ",
                },
              ]}
            >
              <Input placeholder="Title-ar"></Input>
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={24}
            md={24}
            lg={24}
            style={{ width: "100%", justifyContent: "end", display: "flex" }}
          >
            <Form.Item style={{ width: "100", justifyContent: "end" }}>
              <Button
                style={{
                  padding: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "white",
                  borderRadius: "4px",
                  border: "none",
                  backgroundColor: "#2dce89",
                }}
                htmlType="submit"
                loading={PostCategories.isLoading}
                className="add-button"
              >
                Add new PostCategories
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Row>
  );
};
export default PostForm;
