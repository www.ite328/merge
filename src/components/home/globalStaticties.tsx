import { Row, Col } from "antd";
import { TwitterOutlined, ArrowUpOutlined } from "@ant-design/icons";
const GlobalStaticties = (props: any) => {
  return (
    <Row justify="space-between" className="global" style={{ rowGap: 1 }}>
      <div className="global-div">
        <div className="section-one">
          <div className="section-one-Content">
            <p
              style={{
                marginBottom: 0,
                color: "#8898aa !important",
                fontWeight: 400,
                fontSize: 16,
                fontFamily: "Poppins",
              }}
            >
              Total Partners
            </p>
            <p
              style={{
                marginBottom: 0,
                lineHeight: 1.5,
                color: "#32325d",
                fontSize: "1.25rem",
                fontWeight: 600,
              }}
            >
              350,897
            </p>
          </div>

          <div className="section-one-icon">
            <TwitterOutlined style={{ color: "white", fontSize: 22 }} />
          </div>
        </div>
        <div className="section-two">
          <div style={{ display: "flex", alignItems: "center", gap: 6 }}>
            <i
              className="fa fa-arrow-up"
              style={{ color: "#2dce89", fontWeight: 900, fontSize: 14 }}
            ></i>
            <p
              style={{
                marginBottom: 0,
                color: "#2dce89",
                fontWeight: 400,
                fontSize: ".875rem",
              }}
            >
              31.1%
            </p>
          </div>
          <div>
            <p
              style={{
                marginBottom: 0,
                color: "#525f7f",
                lineHeight: 1.8,
                fontWeight: 300,
                fontFamily: "Poppins",
                fontSize: 15,
              }}
            >
              Since last month
            </p>
          </div>
        </div>
      </div>

      <div className="global-div">
        <div className="section-one">
          <div className="section-one-Content">
            <p
              style={{
                marginBottom: 0,
                color: "#8898aa !important",
                fontWeight: 400,
                fontSize: 16,
                fontFamily: "Poppins",
              }}
            >
              Total Partners
            </p>
            <p
              style={{
                marginBottom: 0,
                lineHeight: 1.5,
                color: "#32325d",
                fontSize: "1.25rem",
                fontWeight: 600,
              }}
            >
              350,897
            </p>
          </div>
          <div className="section-two-icon">
            <TwitterOutlined style={{ color: "white", fontSize: 22 }} />
          </div>
        </div>
        <div className="section-two">
          <div style={{ display: "flex", alignItems: "center", gap: 6 }}>
            <i
              className="fa fa-arrow-up"
              style={{ color: "#2dce89", fontWeight: 900, fontSize: 14 }}
            ></i>
            <p
              style={{
                marginBottom: 0,
                color: "#2dce89",
                fontWeight: 400,
                fontSize: ".875rem",
              }}
            >
              31.1%
            </p>
          </div>
          <div>
            <p
              style={{
                marginBottom: 0,
                color: "#525f7f",
                lineHeight: 1.8,
                fontWeight: 300,
                fontFamily: "Poppins",
                fontSize: 15,
              }}
            >
              Since last month
            </p>
          </div>
        </div>
      </div>

      <div className="global-div">
        <div className="section-one">
          <div className="section-one-Content">
            <p
              style={{
                marginBottom: 0,
                color: "#8898aa !important",
                fontWeight: 400,
                fontSize: 16,
                fontFamily: "Poppins",
              }}
            >
              Total Partners
            </p>
            <p
              style={{
                marginBottom: 0,
                lineHeight: 1.5,
                color: "#32325d",
                fontSize: "1.25rem",
                fontWeight: 600,
              }}
            >
              350,897
            </p>
          </div>
          <div className="section-three-icon">
            <TwitterOutlined style={{ color: "white", fontSize: 22 }} />
          </div>
        </div>
        <div className="section-two">
          <div style={{ display: "flex", alignItems: "center", gap: 6 }}>
            <i
              className="fa fa-arrow-up"
              style={{ color: "#2dce89", fontWeight: 900, fontSize: 14 }}
            ></i>
            <p
              style={{
                marginBottom: 0,
                color: "#2dce89",
                fontWeight: 400,
                fontSize: ".875rem",
              }}
            >
              31.1%
            </p>
          </div>
          <div>
            <p
              style={{
                marginBottom: 0,
                color: "#525f7f",
                lineHeight: 1.8,
                fontWeight: 300,
                fontFamily: "Poppins",
                fontSize: 15,
              }}
            >
              Since last month
            </p>
          </div>
        </div>
      </div>

      <div className="global-div">
        <div className="section-one">
          <div className="section-one-Content">
            <p
              style={{
                marginBottom: 0,
                color: "#8898aa !important",
                fontWeight: 400,
                fontSize: 16,
                fontFamily: "Poppins",
              }}
            >
              Total Partners
            </p>
            <p
              style={{
                marginBottom: 0,
                lineHeight: 1.5,
                color: "#32325d",
                fontSize: "1.25rem",
                fontWeight: 600,
              }}
            >
              350,897
            </p>
          </div>
          <div className="section-four-icon">
            <TwitterOutlined style={{ color: "white", fontSize: 22 }} />
          </div>
        </div>
        <div className="section-two">
          <div style={{ display: "flex", alignItems: "center", gap: 6 }}>
            <i
              className="fa fa-arrow-up"
              style={{ color: "#2dce89", fontWeight: 900, fontSize: 14 }}
            ></i>
            <p
              style={{
                marginBottom: 0,
                color: "#2dce89",
                fontWeight: 400,
                fontSize: ".875rem",
              }}
            >
              31.1%
            </p>
          </div>
          <div>
            <p
              style={{
                marginBottom: 0,
                color: "#525f7f",
                lineHeight: 1.8,
                fontWeight: 400,
                fontFamily: "Poppins",
                fontSize: 15,
              }}
            >
              Since last month
            </p>
          </div>
        </div>
      </div>
    </Row>
  );
};
export default GlobalStaticties;
