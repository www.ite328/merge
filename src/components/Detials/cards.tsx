import { Button, Card, Col, Image, notification, Row, Tooltip } from "antd";
import { categoryService } from "../../services/Partner";
import { useQueryClient } from "react-query";
import NextRouter, { useRouter } from "next/router";
import AddServices from "../ModalPartnerComponents/AddServices";
import AddOffers from "../ModalPartnerComponents/AddOffers";
import { useState } from "react";

import Link from "next/link";
import EditPartnerModal from "../ModalEditComponent/EditPartnerModal";

const Cards = (props: any) => {
  const query = useQueryClient();
  const Router = useRouter();
  const { ids } = Router.query;
  console.log(ids, "fdnkjdfhn");

  const [id, setId] = useState<any>(0);

  const [showModalAddSubServices, setModalShowServices] =
    useState<boolean>(false);
  const [showModalAddSuboffers, setModalShowOffers] = useState<boolean>(false);

  const [showModalEditPartner, setModalEditPartner] = useState<boolean>(false);

  const addServicesHandler = (id: any) => {
    setId(id);
    setModalShowServices(true);
  };
  const addOffersHandler = (id: any) => {
    setModalShowOffers(true);
    setId(id);
  };
  const closeAddServicesHandler = () => {
    setModalShowServices(false);
  };
  const closeAddOffersHandler = () => {
    setModalShowOffers(false);
  };

  const UpdatePartnerHandler = (id: any) => {
    setId(id);
    setModalEditPartner(true);
  };
  const closeEditPartnerHandler = () => {
    setModalEditPartner(false);
  };

  const DeletedHandler = (id: any) => {
    categoryService.DeletePostCategories(id).then((res) => {
      if (res?.error == null) {
        notification.open({
          message: " The Childe partner Deleted Successfully ",
        });
        props.getAllPartnerCategories.refetch();
      }
      if (res?.result == null && res.error != null) {
        notification.open({
          message: res?.error?.message + "",
        });
      }
    });
  };
  const SwitchActivationHandler = (id: any, isActive: boolean) => {
    console.log(isActive, "partner");
    categoryService
      .SwitchActivationPartner({ id: id, isActive: !isActive })
      .then((res) => {
        if (res?.result == null) {
          notification.open({
            message: res?.error?.message + "",
          });
        } else {
          notification.open({
            message: "The childe Partner Switch Activated Successfully",
          });
          props.getAllPartnerCategories.refetch();

          //   NextRouter.push(`/partnerById/${props.parentId}`);
        }
      });
  };
  return (
    <>
      <Card
        style={{
          width: "100%",
          marginTop: 0,
        }}
        className="Card-detiales"
      >
        <Image
          src={"../assets/13.jpg"}
          alt=""
          preview={false}
          width={"100%"}
          height="250px"
        ></Image>
        <Row
          className="card-partner-children-body"
          justify="start"
          style={{
            marginTop: 0,
            flexDirection: "column",
            position: "relative",
            // height: "86.6px",
            // overflow: "auto",
            marginBottom: 15,

            flexWrap: "wrap",
          }}
        >
          <p
            className={
              props?.childe?.isActive
                ? "card-partner-children-body-p-active"
                : "card-partner-children-body-p-notAcitve"
            }
          >
            {props?.childe?.isActive ? "Active" : "not Active"}
          </p>

          <div className="icon-edit">
            <Tooltip placement="top" title="Edit Partner">
              <Button
                style={{
                  width: "0px",
                  backgroundColor: "transparent",
                  border: "none",
                }}
                onClick={() => UpdatePartnerHandler(props?.childe?.id)}
              >
                <i
                  className="fa fa-edit"
                  style={{ color: "#2d6a98", fontSize: 16, cursor: "pointer" }}
                  // onClick={() => UpdatePartnerHandler(id)}
                ></i>
              </Button>
            </Tooltip>
          </div>
          <div
            style={{
              maxHeight: 86.6,
              overflowY: "auto",
              marginTop: 35,

              transition: " all 0.3 0.3 ease !important",
            }}
          >
            <p className="name-partner-childe">{props?.childe?.name}</p>
            <p className="title-partner-childe">{props.childe.description}</p>
          </div>
        </Row>
        <Row justify="space-between" gutter={[4, 3]} style={{ width: "100%" }}>
          <Col xs={12} sm={8} md={8} lg={12}>
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Tooltip placement="top" title="Switch Activate">
                {/* <p></p>
              <i
                className="fa fa-undo icon-setting"
                aria-hidden="true"
                style={{ fontSize: 20 }}
              ></i> */}
                <Button
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                  className="card-patern-children-button button-section-one button-switch"
                  onClick={() =>
                    SwitchActivationHandler(
                      props?.childe?.id,
                      props?.childe?.isActive
                    )
                  }
                >
                  <i
                    className="fa fa-undo "
                    aria-hidden="true"
                    style={{ fontSize: 12, color: "white" }}
                  ></i>
                  Switch
                </Button>
              </Tooltip>
            </div>
          </Col>
          <Col xs={12} sm={12} md={12} lg={12}>
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Tooltip placement="top" title="Delete">
                {/* <p></p>
              <i
                className="fa fa-undo icon-setting"
                aria-hidden="true"
                style={{ fontSize: 20 }}
              ></i> */}
                <Button
                  style={{
                    display: "flex",

                    alignItems: "center",
                  }}
                  className="card-patern-children-button button-section-one button-Delete"
                  onClick={() => DeletedHandler(props?.childe?.id)}
                >
                  {" "}
                  <i
                    className="fa fa-trash "
                    aria-hidden="true"
                    style={{ fontSize: 12, color: "white" }}
                  ></i>
                  Delete{" "}
                </Button>
              </Tooltip>
            </div>
          </Col>
          <Col xs={12} sm={12} md={12} lg={12}>
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Tooltip placement="top" title="Add Services">
                {/* <p></p>
              <i
                className="fa fa-undo icon-setting"
                aria-hidden="true"
                style={{ fontSize: 20 }}
              ></i> */}
              </Tooltip>
            </div>
          </Col>
        </Row>
      </Card>
      <Row>
        <AddServices
          isshow={showModalAddSubServices}
          id={id}
          cancleshowModla={closeAddServicesHandler}
        ></AddServices>
        <AddOffers
          isshow={showModalAddSuboffers}
          id={id}
          cancleshowModla={closeAddOffersHandler}
        ></AddOffers>
        <EditPartnerModal
          isshow={showModalEditPartner}
          id={id}
          cancleshowModla={closeEditPartnerHandler}
          parentId={props.parentId}
          getAllPartnerCategories={props.getAllPartnerCategories}
        ></EditPartnerModal>
      </Row>
    </>
  );
};

export default Cards;
