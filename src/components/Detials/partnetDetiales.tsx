import {
  UploadOutlined,
  PlusOutlined,
  SettingOutlined,
  EditOutlined,
  EllipsisOutlined,
  SmileOutlined,
} from "@ant-design/icons";
import Logo from "../../components/icon/logo";
const { TabPane } = Tabs;
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Select,
  Upload,
  notification,
  Avatar,
  Card,
  Skeleton,
  Image,
  Dropdown,
  Menu,
  Tooltip,
  Tabs,
} from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { getPartnerCategories } from "../../react-query-services/Partner-gategories";
import { UploadAttachment } from "../../services/Upload-file";
import { useQuery, useMutation } from "react-query";
import { FC, useEffect, useState } from "react";
import { Parent_Partner_Req, Translation } from "../../model/partner";
import { useRouter } from "next/router";
import UserPartnerDetiales from "../GenericTable/UserPartnerDetiales";
import { GetPartnerById } from "../../react-query-services/partner";
import Meta from "antd/lib/card/Meta";
import AddSubPartner from "../ModalPartnerComponents/AddSubPartnet";
import AddServcies from "../ModalPartnerComponents/AddServices";
import AddOffers from "../ModalPartnerComponents/AddOffers";
import Cards from "./cards";
import Link from "next/link";
const AddPartner = () => {
  const { TextArea } = Input;
  const { Option } = Select;
  const Router = useRouter();
  const { id } = Router.query;
  console.log(id);
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const [showModalAddSubPartner, setModalShowPartner] =
    useState<boolean>(false);
  const [showModalAddSubServices, setModalShowServices] =
    useState<boolean>(false);
  const [showModalAddSuboffers, setModalShowOffers] = useState<boolean>(false);

  const getAllPartnerCategories = useQuery(
    ["Partner-Detiales", id],
    () => GetPartnerById(id),
    {
      onSuccess: () => {},
      onError: () => {},
    }
  );

  const addSubPartnerHandler = () => {
    setModalShowPartner(true);
  };
  const addServicesHandler = () => {
    setModalShowServices(true);
  };
  const addOffersHandler = () => {
    setModalShowOffers(true);
  };

  const closeSubPartnerHandler = () => {
    setModalShowPartner(false);
  };
  const closeAddServicesHandler = () => {
    setModalShowServices(false);
  };
  const closeAddOffersHandler = () => {
    setModalShowOffers(false);
  };

  useEffect(() => {
    console.log(getAllPartnerCategories.data, "Data");
  }, [getAllPartnerCategories.data]);

  const menu = getAllPartnerCategories?.data?.result?.isParent ? (
    <Menu
      items={[
        {
          key: "1",
          label: (
            <a
              rel="noopener noreferrer"
              href="#"
              style={{ fontFamily: "Poppins" }}
              className="icon-link"
              onClick={addSubPartnerHandler}
            >
              Add Sub Partner
            </a>
          ),
          icon: <i className="fa fa-plus icon-setting"></i>,
          disabled: false,
        },
        {
          key: "2",
          label: (
            <a
              rel="noopener noreferrer"
              style={{ fontFamily: "Poppins" }}
              className="icon-link"
              href="#"
              onClick={addServicesHandler}
            >
              Add Services
            </a>
          ),
          icon: <i className="fa fa-plus icon-setting"></i>,

          disabled: false,
        },
        {
          key: "3",
          label: (
            <Link href={`/servicesByPartnerId/${id}`} passHref={true}>
              <p
                style={{ fontFamily: "Poppins", marginBottom: 0 }}
                className="icon-setting"
              >
                Show Services
              </p>
            </Link>
          ),

          icon: <i className="fa fa-eye icon-setting"></i>,
          disabled: false,
        },
        {
          key: "4",
          label: (
            <a
              rel="noopener noreferrer"
              style={{ fontFamily: "Poppins" }}
              className="icon-link"
              onClick={addOffersHandler}
            >
              Add Offers
            </a>
          ),
          icon: <i className="fa fa-plus icon-setting"></i>,
          disabled: false,
        },
        {
          key: "5",
          label: (
            <Link href={`/offersById/${id}`} passHref={true}>
              <p
                style={{ fontFamily: "Poppins", marginBottom: 0 }}
                className="icon-setting"
              >
                Show Offers
              </p>
            </Link>
          ),
          icon: <i className="fa fa-eye icon-setting"></i>,
          disabled: false,
        },
      ]}
    />
  ) : (
    getAllPartnerCategories?.data?.isParent(
      <Menu
        items={[
          {
            key: "1",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins" }}
                className="icon-link"
                href="#"
                onClick={addServicesHandler}
              >
                Add Services
              </a>
            ),
            icon: <i className="fa fa-plus icon-setting"></i>,

            disabled: false,
          },
          {
            key: "2",
            label: (
              <Link href={`/servicesByPartnerId/${id}`} passHref={true}>
                <p
                  style={{ fontFamily: "Poppins", marginBottom: 0 }}
                  className="icon-setting"
                >
                  Show Services
                </p>
              </Link>
            ),
            icon: <i className="fa fa-eye icon-setting"></i>,
            disabled: false,
          },
          {
            key: "3",
            label: (
              <a
                rel="noopener noreferrer"
                style={{ fontFamily: "Poppins" }}
                className="icon-link"
                onClick={addOffersHandler}
              >
                Add Offers
              </a>
            ),
            icon: <i className="fa fa-plus icon-setting"></i>,
            disabled: false,
          },
          {
            key: "4",
            label: (
              <Link href={`/offersById/${id}`} passHref={true}>
                <p
                  style={{ fontFamily: "Poppins", marginBottom: 0 }}
                  className="icon-setting"
                >
                  Show Offers
                </p>
              </Link>
            ),
            icon: <i className="fa fa-eye icon-setting"></i>,
            disabled: false,
          },
        ]}
      />
    )
  );

  const onFinishHandler = (e: any) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("RefType", 2 + " ");
    formData.append("File", e?.image?.fileList[0]?.originFileObj);
    UploadAttachment.UploadAttachment(formData)
      .then((res: any) => {
        let body: Parent_Partner_Req = {
          attachments: [],
          partnerCategoryId: e.partnerCategoriesId,
          translations: [],
          clientLanguageId: "1",
          parentId: 0,
        };
        let objectArabic: Translation = {
          description: e.description_ar,
          name: e.name_ar,
          language: "ar",
        };
        let objectEnglish: Translation = {
          description: e.description_en,
          name: e.name_en,
          language: "en",
        };
        body.translations.push(objectArabic, objectEnglish);
        body.attachments.push(res?.result?.id as number);
      })
      .catch((res) => {});
  };

  return (
    <Row
      style={{
        backgroundColor: "white",
        width: "100%",
        marginBottom: "30px",
        minHeight: 300,
        padding: 0,
      }}
      className="globalFormStyles"
    >
      <Row
        style={{
          padding: "10px 0px",
          width: "100%",
          marginBottom: 25,
        }}
        className="globalFormHeader"
      >
        <Row style={{ width: "100%" }}>
          <p style={{ marginBottom: 0, width: "100%", textAlign: "center" }}>
            Partner Detials
          </p>
        </Row>
        <Row
          style={{
            width: "100%",
            marginTop: 10,
            height: 250,
            zIndex: 2,
            position: "relative",
          }}
          className="image-cover"
        >
          <Image
            src={getAllPartnerCategories?.data?.result.attachments[0]?.url}
            preview={false}
            alt=""
            className="image-cover-profile"
          ></Image>
          <Row style={{ position: "absolute", right: 25, top: 30 }}>
            <Dropdown overlay={menu} placement="bottomLeft">
              <Button
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: "50%",
                  width: 32,
                  height: 32,
                  borderRight: "1px solid #648dea",
                }}
              >
                <i
                  className="fa fa-cog"
                  aria-hidden="true"
                  style={{ fontSize: 16, color: "#648dea" }}
                ></i>
              </Button>
            </Dropdown>
          </Row>
        </Row>
        <Row
          style={{
            marginBottom: 0,
            marginTop: 70,
            paddingLeft: 35,
            paddingRight: 35,
            flexDirection: "row",
            width: "100%",
          }}
          justify="space-between"
        >
          <Col
            sm={12}
            xs={12}
            md={12}
            lg={12}
            style={{ display: "flex", flexDirection: "column" }}
          >
            <p style={{ fontWeight: 600, marginBottom: 0 }}>
              {getAllPartnerCategories?.data?.result?.name}
            </p>
            <p style={{ fontWeight: 400 }}>
              {getAllPartnerCategories?.data?.result?.description}
            </p>
          </Col>
          <Col
            sm={12}
            xs={12}
            md={12}
            lg={12}
            style={{
              display: "flex",
              flexDirection: "column",
              alignContent: "center",
            }}
          >
            <p
              style={{
                fontWeight: 600,
                marginBottom: 0,
                width: "100%",
              }}
              className="p-partner-Detiales"
            >
              {getAllPartnerCategories?.data?.result?.partnerCategory.name}
            </p>
          </Col>
        </Row>
        <Row
          style={{
            width: "100%",
            flexWrap: "wrap",
            paddingLeft: 30,
            paddingRight: 30,
          }}
          gutter={[5, 5]}
          className="partner-detials-card"
        >
          <Tabs
            defaultActiveKey="1"
            style={{ width: "100%", marginTop: "5px" }}
            centered
          >
            <TabPane tab="Branches" key="1" style={{ width: "100%" }}>
              <Row
                style={{
                  width: "100%",
                  flexWrap: "wrap",
                  paddingLeft: 30,
                  paddingRight: 30,
                }}
                gutter={[5, 5]}
                className="partner-detials-card"
              >
                {getAllPartnerCategories.isLoading && (
                  <>
                    <Col xs={24} sm={24} md={11} lg={8}>
                      <Card
                        style={{
                          width: "100%",
                          marginTop: 10,
                        }}
                        className="Card"
                        actions={[
                          <Row
                            justify="space-between"
                            gutter={[4, 3]}
                            key={12}
                            style={{ marginTop: 12 }}
                          >
                            <Col xs={12} sm={8} md={8} lg={12}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Tooltip
                                  placement="top"
                                  title="Switch Activate"
                                >
                                  {/* <p></p>
                        <i
                          className="fa fa-undo icon-setting"
                          aria-hidden="true"
                          style={{ fontSize: 20 }}
                        ></i> */}
                                  <Button
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                    className="card-patern-children-button button-section-one button-switch"
                                    loading={getAllPartnerCategories.isLoading}
                                  >
                                    <i
                                      className="fa fa-undo "
                                      aria-hidden="true"
                                      style={{ fontSize: 12, color: "white" }}
                                    ></i>
                                    Switch
                                  </Button>
                                </Tooltip>
                              </div>
                            </Col>
                            <Col xs={12} sm={8} md={8} lg={12}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Tooltip placement="top" title="Delete">
                                  {/* <p></p>
                        <i
                          className="fa fa-undo icon-setting"
                          aria-hidden="true"
                          style={{ fontSize: 20 }}
                        ></i> */}
                                  <Button
                                    style={{
                                      display: "flex",

                                      alignItems: "center",
                                    }}
                                    className="card-patern-children-button button-section-one button-Delete"
                                    loading={getAllPartnerCategories.isLoading}
                                  >
                                    {" "}
                                    <i
                                      className="fa fa-trash "
                                      aria-hidden="true"
                                      style={{ fontSize: 12, color: "white" }}
                                    ></i>
                                    Delete{" "}
                                  </Button>
                                </Tooltip>
                              </div>
                            </Col>
                          </Row>,
                        ]}
                      >
                        <Skeleton
                          loading={getAllPartnerCategories.isLoading}
                          avatar
                          active
                        >
                          <Meta
                            avatar={
                              <Avatar src="https://joeschmoe.io/api/v1/random" />
                            }
                            title="Card title"
                            description="This is the description"
                          />
                        </Skeleton>
                      </Card>
                    </Col>
                    <Col xs={24} sm={24} md={11} lg={8}>
                      <Card
                        style={{
                          width: "100%",
                          marginTop: 10,
                        }}
                        className="Card"
                        actions={[
                          <Row justify="space-between" gutter={[4, 3]} key={12}>
                            <Col xs={12} sm={8} md={8} lg={12}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Tooltip
                                  placement="top"
                                  title="Switch Activate"
                                >
                                  {/* <p></p>
                        <i
                          className="fa fa-undo icon-setting"
                          aria-hidden="true"
                          style={{ fontSize: 20 }}
                        ></i> */}
                                  <Button
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                    className="card-patern-children-button button-section-one button-switch"
                                    loading={getAllPartnerCategories.isLoading}
                                  >
                                    <i
                                      className="fa fa-undo "
                                      aria-hidden="true"
                                      style={{ fontSize: 12, color: "white" }}
                                    ></i>
                                    Switch
                                  </Button>
                                </Tooltip>
                              </div>
                            </Col>
                            <Col xs={12} sm={8} md={8} lg={12}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Tooltip placement="top" title="Delete">
                                  {/* <p></p>
                        <i
                          className="fa fa-undo icon-setting"
                          aria-hidden="true"
                          style={{ fontSize: 20 }}
                        ></i> */}
                                  <Button
                                    style={{
                                      display: "flex",

                                      alignItems: "center",
                                    }}
                                    className="card-patern-children-button button-section-one button-Delete"
                                    loading={getAllPartnerCategories.isLoading}
                                  >
                                    {" "}
                                    <i
                                      className="fa fa-trash "
                                      aria-hidden="true"
                                      style={{ fontSize: 12, color: "white" }}
                                    ></i>
                                    Delete{" "}
                                  </Button>
                                </Tooltip>
                              </div>
                            </Col>
                          </Row>,
                        ]}
                      >
                        <Skeleton
                          loading={getAllPartnerCategories.isLoading}
                          avatar
                          active
                        >
                          <Meta
                            avatar={
                              <Avatar src="https://joeschmoe.io/api/v1/random" />
                            }
                            title="Card title"
                            description="This is the description"
                          />
                        </Skeleton>
                      </Card>
                    </Col>
                    <Col xs={24} sm={24} md={11} lg={8}>
                      <Card
                        style={{
                          width: "100%",
                          marginTop: 10,
                        }}
                        className="Card"
                        actions={[
                          <Row justify="space-between" gutter={[4, 3]} key={12}>
                            <Col xs={12} sm={8} md={8} lg={12}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Tooltip
                                  placement="top"
                                  title="Switch Activate"
                                >
                                  {/* <p></p>
                        <i
                          className="fa fa-undo icon-setting"
                          aria-hidden="true"
                          style={{ fontSize: 20 }}
                        ></i> */}
                                  <Button
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                    className="card-patern-children-button button-section-one button-switch"
                                    loading={getAllPartnerCategories.isLoading}
                                  >
                                    <i
                                      className="fa fa-undo "
                                      aria-hidden="true"
                                      style={{ fontSize: 12, color: "white" }}
                                    ></i>
                                    Switch
                                  </Button>
                                </Tooltip>
                              </div>
                            </Col>
                            <Col xs={12} sm={8} md={8} lg={12}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Tooltip placement="top" title="Delete">
                                  {/* <p></p>
                        <i
                          className="fa fa-undo icon-setting"
                          aria-hidden="true"
                          style={{ fontSize: 20 }}
                        ></i> */}
                                  <Button
                                    style={{
                                      display: "flex",

                                      alignItems: "center",
                                    }}
                                    className="card-patern-children-button button-section-one button-Delete"
                                    loading={getAllPartnerCategories.isLoading}
                                  >
                                    {" "}
                                    <i
                                      className="fa fa-trash "
                                      aria-hidden="true"
                                      style={{ fontSize: 12, color: "white" }}
                                    ></i>
                                    Delete{" "}
                                  </Button>
                                </Tooltip>
                              </div>
                            </Col>
                          </Row>,
                        ]}
                      >
                        <Skeleton
                          loading={getAllPartnerCategories.isLoading}
                          avatar
                          active
                        >
                          <Meta
                            avatar={
                              <Avatar src="https://joeschmoe.io/api/v1/random" />
                            }
                            title="Card title"
                            description="This is the description"
                          />
                        </Skeleton>
                      </Card>
                    </Col>
                  </>
                )}
                {!getAllPartnerCategories.isLoading &&
                  getAllPartnerCategories?.data?.result?.partners?.map(
                    (ele: any) => {
                      console.log(ele);
                      return (
                        <Col
                          xs={24}
                          sm={24}
                          md={11}
                          lg={8}
                          key={ele.id}
                          style={{ marginTop: 30 }}
                        >
                          <Cards
                            childe={ele}
                            parentId={id}
                            getAllPartnerCategories={getAllPartnerCategories}
                          ></Cards>
                        </Col>
                      );
                    }
                  )}
              </Row>
            </TabPane>
            <TabPane tab="Users" key="2">
              <UserPartnerDetiales
                UserDetiales={
                  getAllPartnerCategories?.data?.result?.partnerUsers
                }
              ></UserPartnerDetiales>
            </TabPane>
          </Tabs>
        </Row>
        <Row>
          <AddSubPartner
            isshow={showModalAddSubPartner}
            id={id}
            cancleshowModla={closeSubPartnerHandler}
            getAllPartnerCategories={getAllPartnerCategories}
          ></AddSubPartner>
          <AddServcies
            isshow={showModalAddSubServices}
            id={id}
            cancleshowModla={closeAddServicesHandler}
          ></AddServcies>
          <AddOffers
            isshow={showModalAddSuboffers}
            id={id}
            cancleshowModla={closeAddOffersHandler}
          ></AddOffers>
        </Row>
      </Row>
    </Row>
  );
};
export default AddPartner;
