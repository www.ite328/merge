import {
  ShopOutlined,
  UsergroupAddOutlined,
  TranslationOutlined,
  UserOutlined,
  HomeOutlined,
  NotificationOutlined,
  DropboxOutlined,
  PaperClipOutlined,
  CameraOutlined,
  SlidersFilled,
} from "@ant-design/icons";
import { Row, Col } from "antd";
import Dashboard from "../../icon/dashboard";
import Partner from "../../icon/partner";
import Countries from "../../icon/countries";
import User from "../../icon/user";
import PartnerCategories from "../../icon/PartnerCategories";
import PostCategories from "../../icon/PostCategories";
import Post from "../../icon/post";
import VedioMetting from "../../icon/vedioMetting";
import Slider from "../../icon/slider";
import Notifications from "../../icon/notifications";
export const header1 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <Dashboard></Dashboard>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Dashboard
    </p>
  </Row>
);
export const header2 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <Partner
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#fb6340 ",
    // }}
    // className="UsergroupAddOutlined"
    ></Partner>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Partners
    </p>
  </Row>
);
export const header3 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <TranslationOutlined
      style={{
        minWidth: "2rem",
        fontSize: "1.1rem",
        lineHeight: "1.5rem",
        color: "#11cdef",
      }}
      className="TranslationOutlined"
    ></TranslationOutlined>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Translaters
    </p>
  </Row>
);
export const header4 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <PartnerCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#f3a4b5 ",
    // }}
    // className="UserOutlined"
    ></PartnerCategories>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Users
    </p>
  </Row>
);
export const header21 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <PartnerCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#f3a4b5 ",
    // }}
    // className="UserOutlined"
    ></PartnerCategories>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Admins
    </p>
  </Row>
);
export const header5 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    <Countries
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#11cdef ",
    // }}
    // className="HomeOutlined"
    ></Countries>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Countries
    </p>
  </Row>
);
export const header6 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <Notifications></Notifications>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Notifications
    </p>
  </Row>
);
export const header7 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <Post
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#5e72e4",
    //   fontWeight: "bold",
    // }}
    // className="PostOutlined"
    ></Post>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Posts
    </p>
  </Row>
);
export const header8 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <PostCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#2dce89",
    //   fontWeight: "bold",
    // }}
    // className="PostCategoriesOutlined"
    ></PostCategories>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      PostCategories
    </p>
  </Row>
);

export const header9 = (
  <Row style={{ width: "100%", alignItems: "center" }}>
    {" "}
    <Slider></Slider>
    <p style={{ marginBottom: 0 }} className="sidebare-p">
      Role
    </p>
  </Row>
);
export const header10 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <PartnerCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#f3a4b5 ",
    // }}
    // className="UserOutlined"
    ></PartnerCategories>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      PartnerCategories
    </p>
  </Row>
);
export const header11 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <VedioMetting></VedioMetting>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      VideoMeetings
    </p>
  </Row>
);
export const header12 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <Slider></Slider>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      Slider images
    </p>
  </Row>
);
export const header15 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <Post
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#2dce89 ",
    //   fontWeight: "bold",
    // }}
    // className="NotificationOutlined"
    ></Post>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      Complaints
    </p>
  </Row>
);
export const header16 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <PostCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#2dce89 ",
    //   fontWeight: "bold",
    // }}
    // className="NotificationOutlined"
    ></PostCategories>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      ComplaintCategories
    </p>
  </Row>
);
export const header17 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <Dashboard
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#2dce89 ",
    //   fontWeight: "bold",
    // }}
    // className="NotificationOutlined"
    ></Dashboard>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      Privacy Policy
    </p>
  </Row>
);
export const header18 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <PostCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#2dce89 ",
    //   fontWeight: "bold",
    // }}
    // className="NotificationOutlined"
    ></PostCategories>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      Terms And Conditions
    </p>
  </Row>
);
export const header19 = (
  <Row style={{ width: "100%", alignItems: "center", flexWrap: "nowrap" }}>
    {" "}
    <PartnerCategories
    // style={{
    //   minWidth: "2rem",
    //   fontSize: "1.1rem",
    //   lineHeight: "1.5rem",
    //   color: "#2dce89 ",
    //   fontWeight: "bold",
    // }}
    // className="NotificationOutlined"
    ></PartnerCategories>
    <p style={{ marginBottom: 0, flexWrap: "nowrap" }} className="sidebare-p">
      Contact Us
    </p>
  </Row>
);
////
