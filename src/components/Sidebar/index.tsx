import { Drawer, Row, Col, Collapse, Image } from "antd";
import {
  RightOutlined,
  ShopOutlined,
  UsergroupAddOutlined,
  TranslationOutlined,
  UserOutlined,
  HomeOutlined,
  NotificationOutlined,
} from "@ant-design/icons";

import {
  header1,
  header10,
  header2,
  header3,
  header4,
  header5,
  header6,
  header7,
  header8,
  header9,
  header11,
  header12,
  header15,
  header16,
  header17,
  header18,
  header19,
  header21,
} from "./header";
import { useState, useEffect, useLayoutEffect } from "react";
import Link from "next/link";
import Logo from "../icon/logo";
import NextRouter from "next/router";
const SidePar: React.FC<{ activeKey: number }> = (props: any) => {
  const [visible, setVisibble] = useState<boolean>(true);

  const { Panel } = Collapse;
  let showDrawer = (isvisible: boolean) => {
    setVisibble(isvisible);
  };

  let onClose = () => {
    setVisibble(false);
  };
  useLayoutEffect(() => {
    console.log("jhsj");
    if (
      localStorage.getItem("MERGE_ADMIN") == null ||
      localStorage.getItem("MERGE_ADMIN") == undefined ||
      localStorage.getItem("MERGE_ADMIN") == ""
    ) {
      NextRouter.replace("/login");
    } else {
    }
  });
  return (
    <div
      style={{
        position: "relative",
        display: "block",
        backgroundColor: "white",
      }}
    >
      <div className="home" style={{ backgroundColor: "white" }}>
        <Row
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Logo></Logo>
        </Row>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            height: "calc(100vh - 79px)",
          }}
        >
          <div className="Drawer">
            <Collapse
              defaultActiveKey={props.activeKey}
              expandIcon={({ isActive }) => (
                <RightOutlined
                  rotate={isActive ? 90 : 0}
                  style={
                    isActive
                      ? { color: "#5e72e4", fontWeight: "bold", fontSize: 12 }
                      : { fontWeight: "bold", fontSize: 12, color: "#ced4da" }
                  }
                />
              )}
              expandIconPosition="right"
            >
              <Panel
                header={header1}
                key={1}
                className={`${
                  props.activeKey === 1 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/" passHref={true}>
                  <p className=" sidebare-p sidebar-content"> Dashboard</p>
                </Link>
              </Panel>
              <Panel
                header={header2}
                key={2}
                className={`${
                  props.activeKey === 2 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/Partner" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Partners</p>
                </Link>
                <Link href="/addPartner" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Add Partner</p>
                </Link>
              </Panel>
              <Panel
                header={header10}
                key={9}
                className={`${
                  props.activeKey === 9 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/PartnerCategories" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    Partner Categories
                  </p>
                </Link>
                <Link href="/addPartnerCategories" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    Add Partner Category
                  </p>
                </Link>
              </Panel>

              <Panel
                header={header4}
                key={4}
                className={`${
                  props.activeKey === 4 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/userPartner" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Partner Users</p>
                </Link>

                <Link href="/user" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Agents</p>
                </Link>
                <Link href="/client" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Clients</p>
                </Link>
                <Link href="/addUser" passHref={true}>
                  <p className="sidebare-p sidebar-content"> add Agents</p>
                </Link>
                <Link href="/addUserToPartner" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    add Partner User
                  </p>
                </Link>
              </Panel>
              <Panel
                header={header21}
                key={21}
                className={`${
                  props.activeKey === 21 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/admins" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Admins</p>
                </Link>
                <Link href="/addadmins" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Add Admin</p>
                </Link>
              </Panel>
              <Panel
                header={header5}
                key={5}
                className={`${
                  props.activeKey === 5 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/Countries" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Countries</p>
                </Link>
                <Link href="/addCountry" passHref={true}>
                  <p className="sidebare-p sidebar-content">Add Countries</p>
                </Link>
              </Panel>
              {/* <Panel
                header={header6}
                key={6}
                className={`${
                  props.activeKey === 6 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/notification" passHref={true}>
                  <p className="sidebare-p sidebar-content"> notification</p>
                </Link>
                <Link href="/addNotification" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    Send notification
                  </p>
                </Link>
              </Panel> */}
              <Panel
                header={header7}
                key={7}
                className={`${
                  props.activeKey === 7 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/Post" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Posts</p>
                </Link>
                <Link href="/addPost" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Add Post</p>
                </Link>
              </Panel>
              <Panel
                header={header8}
                key={8}
                className={`${
                  props.activeKey === 8 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/PostCategories" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Post Categories</p>
                </Link>
                <Link href="/addPostCategories" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    Add Post Category
                  </p>
                </Link>
              </Panel>

              <Panel
                header={header11}
                key={11}
                className={`${
                  props.activeKey === 11 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/VideoCalls" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    User to Intrepter
                  </p>
                </Link>
                <Link href="/VedioConferance" passHref={true}>
                  <p className="sidebare-p sidebar-content">User to Partner</p>
                </Link>
                <Link href="/useruser" passHref={true}>
                  <p className="sidebare-p sidebar-content">User to User</p>
                </Link>
              </Panel>
              <Panel
                header={header12}
                key={12}
                className={`${
                  props.activeKey === 12 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/slider" passHref={true}>
                  <p className="sidebare-p sidebar-content">Sliders</p>
                </Link>
                <Link href="/addSlider" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Add Slider</p>
                </Link>
              </Panel>
              <Panel
                header={header6}
                key={13}
                className={`${
                  props.activeKey === 13 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/notification" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Notifications</p>
                </Link>
                <Link href="/addNotification" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    Send Notification
                  </p>
                </Link>
              </Panel>
              <Panel
                header={header9}
                key={20}
                className={`${
                  props.activeKey === 20 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/Role" passHref={true}>
                  <p className="sidebare-p sidebar-content">Roles</p>
                </Link>
                <Link href="/addRole" passHref={true}>
                  <p className="sidebare-p sidebar-content">Add Raole</p>
                </Link>
              </Panel>
              <Panel
                header={header15}
                key={15}
                className={`${
                  props.activeKey === 15 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/complaints" passHref={true}>
                  <p className="sidebare-p sidebar-content"> Complaints</p>
                </Link>
              </Panel>
              <Panel
                header={header16}
                key={16}
                className={`${
                  props.activeKey === 16 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/complaintsCategories" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    Complaint Categories
                  </p>
                </Link>
                <Link href="/addcomplaints" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    {" "}
                    add Complaint Category
                  </p>
                </Link>
              </Panel>
              <Panel
                header={header17}
                key={17}
                className={`${
                  props.activeKey === 17 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/addprivacy" passHref={true}>
                  <p className="sidebare-p sidebar-content"> add Privacy</p>
                </Link>
              </Panel>
              <Panel
                header={header18}
                key={18}
                className={`${
                  props.activeKey === 18 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/addTermsAndConditions" passHref={true}>
                  <p className="sidebare-p sidebar-content">
                    add Terms And Conditions
                  </p>
                </Link>
              </Panel>
              <Panel
                header={header19}
                key={19}
                className={`${
                  props.activeKey === 19 ? "PanelActive" : "panelnotActive"
                }`}
              >
                <Link href="/addContactus" passHref={true}>
                  <p className="sidebare-p sidebar-content">add contact us</p>
                </Link>
              </Panel>
            </Collapse>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
            }}
          >
            <Image
              src="/assets/13.jpg"
              alt=""
              preview={false}
              style={{ marginTop: 10 }}
              width={"90%"}
            ></Image>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SidePar;

//
