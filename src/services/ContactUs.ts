import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {ContactUs_Req} from '../model/ContactUs';

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPrivacy = async (body:ContactUs_Req):Promise<any> => {
    return  this.post('/services/app/Configuration/SetContactUs',body);
}

 

  
}

export const SliderService = new CategoryService();
