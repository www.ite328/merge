import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";


class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddCountries = async (body:any):Promise<any> => {
        return await this.post('/services/app/City/Create',body);
  }
  public  getCountryWithPatination = async (keyword:any , SkipCount:any ,id:any):Promise<any> => {
    return await this.get(`/services/app/City/GetAll?MaxResultCount=${3}&SkipCount=${SkipCount}&Keyword=${keyword}&Countryid${id}`);
}
public  DeleteCountry = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/City/Delete?Id=${id}`);
}
public  SwitchActivationCountry = async (body:any):Promise<any> => {
  return await this.put(`/services/app/City/SwitchActivation`, body);
}
public  getPartnerCategoriesWithOutPatination = async ():Promise<any> => {
  return  this.get(`/services/app/City/GetAll?MaxResultCount=${1000}`);
}
  
}

export const countryService = new CategoryService();
