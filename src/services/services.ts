import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Services_Req} from '../model/Services';

class PosgtService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddServices = async (body:Services_Req):Promise<any> => {
        return await this.post('/services/app/Service/Create',body);
  }
  
  public  getServicesWithPatination = async (keyword:any , SkipCount:any , id:any):Promise<any> => {
    return await this.get(`/services/app/Service/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}&PartnerId=${id}`);
}
public  DeleteServicesCategories = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/Service/Delete?Id=${id}`);
}
public  SwitchActivationServices = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Service/SwitchActivation`, body);
}
public  getServicesById = async (id:any):Promise<any> => {
  return await this.get(`/services/app/Service/Get?Id=${id}`);
}    
public  UpdateServices = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Service/Update`, body);
}  
}

export const PostService = new PosgtService();
