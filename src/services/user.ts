import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {User_Req , Add_User_Res} from '../model/user';

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddUser = async (body:User_Req):Promise<any> => {
        return  this.post('/services/app/User/Create',body);
  }
  public  getUserWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/User/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}&Type=4`);
}
  public  getClientWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/User/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}&Type=1`);
}
  public  getPartnerWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/User/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}&Type=2`);
}
  public  getAdminWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/User/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}&Type=0`);
}
  public  getAdminById = async (id:any):Promise<any> => {
    return await this.get(`/services/app/User/Get?Id=${id}`);
}

public  DeleteUserCategories = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/User/Delete?Id=${id}`);
}

public ActivationUser = async (body:any):Promise<any> => {
  return await this.post(`/services/app/User/Activate`, body);
}  
public DeActivationUser = async (body:any):Promise<any> => {
  return await this.post(`/services/app/User/DeActivate`, body);
}  
  
}

export const UserService = new CategoryService();
