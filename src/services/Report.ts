import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Report} from '../model/Report';

class ReportServices extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  ExportToExcel = async (body:Report):Promise<any> => {
        return await this.post('/services/app/Report/GenerateReport',body);
  }

}

export const ReportService = new ReportServices();
