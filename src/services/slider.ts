import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {User_Req , Add_User_Res} from '../model/user';

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddSlider = async (body:any):Promise<any> => {
    return  this.post('/services/app/SilderImage/Create',body);
}

  public  getSliderWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/SilderImage/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}

public  DeleteSlider = async (id:any):Promise<any> => {
  return await this.delete(`services/app/SilderImage/Delete?Id=${id}`);
}

  
}

export const SliderService = new CategoryService();
