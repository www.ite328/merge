import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Post_Req , Post_Res} from '../model/posts';

class PosgtService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPost = async (body:Post_Req):Promise<any> => {
        return await this.post('/services/app/Posts/Create',body);
  }
  
  public  getPostWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/Posts/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}
 
public  DeletePostCategories = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/Posts/Delete?Id=${id}`);
}
public  SwitchActivationPost = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Posts/SwitchActivation`, body);
}  
public  UpdatePost = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Posts/Update`, body);
}  
public  getPostById = async (id:any):Promise<any> => {
  return await this.get(`/services/app/Posts/Get?Id=${id}`);
}  
}

export const PostService = new PosgtService();
