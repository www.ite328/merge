import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Privacy_Req} from '../model/PrivacyPolicy';

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPrivacy = async (body:Privacy_Req):Promise<any> => {
    return  this.post('/services/app/Configuration/SetPrivacyPolicy',body);
}

 

  
}

export const SliderService = new CategoryService();
