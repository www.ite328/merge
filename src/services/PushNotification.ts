import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";


class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddNotification = async (body:any):Promise<any> => {
    return  this.post('/services/app/PushNotification/Create',body);
}

  public  getSliderWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/PushNotification/GetAll?MaxResultCount=${6}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}


  
}

export const SliderService = new CategoryService();
