import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {complaintCategories_Req} from '../model/complaint-categories'

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddcomplaintCategories = async (body:complaintCategories_Req):Promise<any> => {
        return await this.post('/services/app/ComplaintCategory/Create',body);
  }
  public  getPostCategoriesWithOutPatination = async ():Promise<any> => {
    return await this.get(`/services/app/ComplaintCategory/GetAll?MaxResultCount=${1000}`);
}
  public  getcomplaintCategoriesWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/ComplaintCategory/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}
  public  DeletecomplaintCategories = async (id:any):Promise<any> => {
    return await this.delete(`/services/app/ComplaintCategory/Delete?Id=${id}`);
}
public  SwitchActivationcomplaintCategories = async (body:any):Promise<any> => {
  return await this.put(`/services/app/ComplaintCategory/SwitchActivation`, body);
}  
  
}

export const categoryService = new CategoryService();
