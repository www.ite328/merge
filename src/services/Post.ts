import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {PostCategories_Req} from '../model/post-categories'

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPostCategories = async (body:PostCategories_Req):Promise<any> => {
        return await this.post('/services/app/PostCategory/Create',body);
  }
  public  getPostCategoriesWithOutPatination = async ():Promise<any> => {
    return await this.get(`/services/app/PostCategory/GetAll?MaxResultCount=${1000}`);
}
  public  getPostCategoriesWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/PostCategory/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}
  public  DeletePostCategories = async (id:any):Promise<any> => {
    return await this.delete(`/services/app/PostCategory/Delete?Id=${id}`);
}
public  SwitchActivationPostCategories = async (body:any):Promise<any> => {
  return await this.put(`/services/app/PostCategory/SwitchActivation`, body);
}  
public  getPartnerById = async (id:any):Promise<any> => {
  return await this.get(`/services/app/Partner/Get?Id=${id}`);
}  

  
}

export const categoryService = new CategoryService();
