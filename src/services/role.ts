import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Role_Req} from '../model/role';

class PosgtService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddRole = async (body:Role_Req):Promise<any> => {
        return await this.post('/services/app/Role/Create',body);
  }
  
  public  getRoleWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/Role/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}
  
  public  getPermissionWithPatination = async ():Promise<any> => {
    return await this.get(`/services/app/Role/GetAllPermissions?MaxResultCount=${10000}`);
}
  public  getRoleWithoutPatination = async ():Promise<any> => {
    return await this.get(`/services/app/Role/GetAll?MaxResultCount=${10000}`);
}
 
public  DeleteRole = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/Role/Delete?Id=${id}`);
}


public  UpdateRole = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Role/Update`, body);
}  
public  getRoleId = async (id:any):Promise<any> => {
  return await this.get(`/services/app/Role/GetRoleForEdit?Id=${id}`);
}  
}

export const PostService = new PosgtService();
