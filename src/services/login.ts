import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Login_Req} from '../model/login'

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  login = async (body:Login_Req):Promise<any> => {
        return await this.post('/TokenAuth/Authenticate',body);
  }
  
  
}

export const categoryService = new CategoryService();
