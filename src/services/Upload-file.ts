import { AxiosRequestConfig } from "axios";
import ApiService from "../utils/api-services";


class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  UploadAttachment = async (body:any):Promise<any> => {
        return await this.post('/services/app/Attachment/Upload',body);
  }
  
  
}

export const UploadAttachment = new CategoryService();
