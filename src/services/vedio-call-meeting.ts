import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {User_Req , Add_User_Res} from '../model/user';

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
 
  public  getVedioMeeting = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/VideoMeeting/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}&CallType=1`);
}
  public  getVedioConferance = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/VideoMeeting/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}&CallType=2`);
}
  public  getUserUser = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/VideoMeeting/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}&CallType=3`);
}

public  DeleteVedioMeeting = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/VideoMeeting/Delete?Id=${id}`);
}

  
}

export const VedioMeeting = new CategoryService();
