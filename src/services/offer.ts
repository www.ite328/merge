import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Offer_Req} from '../model/offers';

class PosgtService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddServices = async (body:Offer_Req):Promise<any> => {
        return await this.post('/services/app/Offer/Create',body);
  }
  
  public  getServicesWithPatination = async (keyword:any , SkipCount:any , id:any):Promise<any> => {
    return await this.get(`/services/app/Offer/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}&PartnerId=${id}`);
}
public  DeleteOfferCategories = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/Offer/Delete?Id=${id}`);
}
public  SwitchActivationOffer = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Offer/SwitchActivation`, body);
}  

public  getOffersById = async (id:any):Promise<any> => {
  return await this.get(`/services/app/Offer/Get?Id=${id}`);
} 
public  UpdateOffers = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Offer/Update`, body);
} 
}

export const PostService = new PosgtService();
