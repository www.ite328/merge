import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";


class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  
  public  getComplaintsWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/Complaint/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}`);
}
// public  DeleteCountry = async (id:any):Promise<any> => {
//   return await this.delete(`/services/app/Country/Delete?Id=${id}`);
// }
// public  SwitchActivationCountry = async (body:any):Promise<any> => {
//   return await this.put(`/services/app/Country/SwitchActivation`, body);
// }
  
}

export const countryService = new CategoryService();
