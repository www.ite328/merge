import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {lang_req} from '../model/lang'

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddLang = async (body:lang_req):Promise<any> => {
        return await this.post('/services/app/ApplicationLanguge/Create',body);
  }
  
  
}

export const langService = new CategoryService();
