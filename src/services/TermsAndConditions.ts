import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {TermsAndConditions_Req} from '../model/TermsAndConditions';

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPrivacy = async (body:TermsAndConditions_Req):Promise<any> => {
    return  this.post('/services/app/Configuration/SetTermsAndConditions',body);
}

 

  
}

export const SliderService = new CategoryService();
