import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {Parent_Partner_Req} from '../model/partner'

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPostCategories = async (body:Parent_Partner_Req):Promise<any> => {
        return await this.post('/services/app/Partner/Create',body);
  }
  public  GetPartnerById = async (id:any):Promise<any> => {
        return await this.get(`/services/app/Partner/Get?id=${id}`);
  }

  public  getPartnerWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/Partner/GetAll?MaxResultCount=${10}&SkipCount=${SkipCount}&Keyword=${keyword}&IsParent=${true}`);
}
  public  getPartnerWithoutPatination = async ():Promise<any> => {
    return await this.get(`/services/app/Partner/GetAll?MaxResultCount=${1000} &IsParent=${true}`);
}
  public  getPartnerBranchWithoutPatination = async ():Promise<any> => {
    return await this.get(`/services/app/Partner/GetAll?MaxResultCount=${1000}`);
}

public  UpdatePartner = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Partner/Update`, body);
}  

public  DeletePostCategories = async (id:any):Promise<any> => {
  return await this.delete(`/services/app/Partner/Delete?Id=${id}`);
}
  
public  SwitchActivationPartner = async (body:any):Promise<any> => {
  return await this.put(`/services/app/Partner/SwitchActivation`, body);
}  
public  AssignUsertoPartner = async (userId:any , partnerId:any):Promise<any> => {
  return await this.post(`/services/app/Partner/AssigndUserToPartner?userId=${userId}&partnerId=${partnerId}`);
}  
}

export const categoryService = new CategoryService();
