import { AxiosRequestConfig } from "axios";

import ApiService from "../utils/api-services";
import {PartnerCategories_Req, PartnerCategories_Res} from '../model/Partner-Categories'

class CategoryService extends ApiService {
  constructor(config?: AxiosRequestConfig) {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api/`, ...config });
  }
  
  public  AddPartnerCategories = async (body:PartnerCategories_Req):Promise<any> => {
        return  this.post('/services/app/PartnerCategory/Create',body);
  }
  
  public  getPartnerCategoriesWithOutPatination = async ():Promise<any> => {
        return  this.get(`/services/app/PartnerCategory/GetAll?MaxResultCount=${1000}`);
  }
  public  GetPartnerCategoriesById = async (id:any):Promise<any> => {
    return await this.get(`/services/app/PartnerCategory/Get?id=${id}`);
}

  public  getPartnerCategoriesWithPatination = async (keyword:any , SkipCount:any):Promise<any> => {
    return await this.get(`/services/app/PartnerCategory/GetAll?MaxResultCount=${4}&SkipCount=${SkipCount}&Keyword=${keyword}`);
  }
  public  DeletePartnerCategories = async (id:any):Promise<any> => {
    return await this.delete(`services/app/PartnerCategory/Delete?id=${id}`);
}
public  SwitchActivationUser = async (body:any):Promise<any> => {
  return await this.put(`/services/app/PartnerCategory/SwitchActivation`, body);
}  
public  UpdatePartnerCategories = async (body:any):Promise<any> => {
  return await this.put(`/services/app/PartnerCategory/Update`, body);
}  
  
}

export const PartnerService = new CategoryService();
