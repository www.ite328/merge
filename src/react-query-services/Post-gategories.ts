import {categoryService} from '../services/Post'
import {PostCategories_Req , PostCategories_Res} from '../model/post-categories'

export const AddPostCategories =  async(body:PostCategories_Req) => {
          console.log(body);
         const result =  await  categoryService.AddPostCategories(body);
          console.log(body , result);
          return result.data;
}
export const getPostCategoriesWithOutPatination =  async() => {
    
         const result =  await  categoryService.getPostCategoriesWithOutPatination();
          return result?.result.items;
}
export const getPostCategoriesWithPatination =  async(Keyword:any , skipcount:any) => {
    
         const result =  await  categoryService.getPostCategoriesWithPatination(Keyword ,skipcount);
          return result?.result;
}
export const getDeleteCategories =  async(id:any) => {
    
    const result =  await  categoryService.DeletePostCategories(id);
     return result?.result;
  }