import {countryService} from '../services/cities';


export const AddCountry =  async(body:any) => {
          console.log(body);
         const result =  await  countryService.AddCountries(body);
          console.log(body , result);
          return result.data;
}
export const getCountryWithPatination =  async(Keyword:any , skipcount:any , id:any) => {
    
    const result =  await  countryService.getCountryWithPatination(Keyword ,skipcount , id);
     return result?.result;
  }
export const getCountryWithoutPatination =  async() => {
    
    const result =  await  countryService.getPartnerCategoriesWithOutPatination();
     return result?.result;
  }
  export const getDeleteCountry =  async(id:any) => {
    
    const result =  await  countryService.DeleteCountry(id);
    return result?.result;

 }