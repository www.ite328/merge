import {UserService} from '../services/user';
import {User_Req} from '../model/user';
import { message } from 'antd';

export const AddUser =  async(body:User_Req) => {
   
         const result =  await  UserService.AddUser(body);
        console.log(result);
            return result ;
    
}
export const getUserWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  UserService.getUserWithPatination(Keyword ,skipcount);
     return result?.result;
}
export const getClientWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  UserService.getClientWithPatination(Keyword ,skipcount);
     return result?.result;
}
export const getPartnerWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  UserService.getPartnerWithPatination(Keyword ,skipcount);
     return result?.result;
}
export const getAdminWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  UserService.getAdminWithPatination(Keyword ,skipcount);
     return result?.result;
}

export const DeletePartnerWithPatination =  async(id:any) => {
    
    const result =  await  UserService.DeleteUserCategories(id);
     return result?.result;
}


export const getUserById = async(id:any)=> {
   
    const result =  await  UserService.getAdminById(id);
     return result?.result;
}