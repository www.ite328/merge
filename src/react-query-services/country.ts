import {countryService} from '../services/country';
import {country_req} from '../model/Countries';

export const AddCountry =  async(body:country_req) => {
          console.log(body);
         const result =  await  countryService.AddCountries(body);
          console.log(body , result);
          return result.data;
}
export const getCountryWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  countryService.getCountryWithPatination(Keyword ,skipcount);
     return result?.result;
  }
  export const getDeleteCountry =  async(id:any) => {
    
    const result =  await  countryService.DeleteCountry(id);
    return result?.result;

 }