import {SliderService} from '../services/slider'


export const AddSlider =  async(body:any) => {
          console.log(body);
         const result =  await  SliderService.AddSlider(body);
          console.log(body , result);
          return result;
}
export const getSliderWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  SliderService.getSliderWithPatination(Keyword ,skipcount);
     return result?.result;
}
export const getDeleteServices =  async(id:any) => {
    
    const result =  await  SliderService.DeleteSlider(id);
     return result?.result;
  }

