import {PartnerService} from '../services/Partner-categories'
import {PartnerCategories_Req , PartnerCategories_Res} from '../model/Partner-Categories'

export const AddPartnerCategories =  async(body:PartnerCategories_Req) => {
          console.log(body);
         const result =  await  PartnerService.AddPartnerCategories(body);
     
          return result;
}
export const getPartnerCategories =  async() => {
        
         const result =  await  PartnerService.getPartnerCategoriesWithOutPatination();
    
          return result?.result.items;
}
export const getPostCategoriesWithPatination =  async(Keyword:any , skipcount:any) => {
    
  const result =  await  PartnerService.getPartnerCategoriesWithPatination(Keyword ,skipcount);
   return result?.result;
}
export const getDeleteCategories =  async(id:any) => {
    
  const result =  await  PartnerService.DeletePartnerCategories(id);
   return result?.result;
}
export const getPartnerCategoriesById = async (id:any)=> {
  const result = await PartnerService.GetPartnerCategoriesById(id);
    return result;
}
export const UpdatePartnerCategories = async (body:any)=> {
  const result = await PartnerService.UpdatePartnerCategories(body);
    return result;
}
