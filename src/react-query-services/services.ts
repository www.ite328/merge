import {PostService} from '../services/services'
import {Services_Req} from '../model/Services'

export const AddPost =  async(body:Services_Req) => {
          console.log(body);
         const result =  await  PostService.AddServices(body);
          console.log(body , result);
          return result;
}
export const getPostWithPatination =  async(Keyword:any , skipcount:any,id:any) => {
    
    const result =  await  PostService.getServicesWithPatination(Keyword ,skipcount,id);
     return result?.result;
}
export const getDeleteServices =  async(id:any) => {
    
    const result =  await  PostService.DeleteServicesCategories(id);
     return result?.result;
  }

  export const getServicesById = async (id:any)=> {
    const result = await PostService.getServicesById(id);
      return result;
}

export const UpdateServices = async (body:any)=> {
    const result = await PostService.UpdateServices(body);
      return result;
}