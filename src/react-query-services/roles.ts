import {PostService} from '../services/role'
import {Role_Req} from '../model/role'

export const AddRole =  async(body:Role_Req) => {
          console.log(body);
         const result =  await  PostService.AddRole(body);
          console.log(body , result);
          return result;
}
export const getRoleWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  PostService.getRoleWithPatination(Keyword ,skipcount);
     return result?.result;
}
export const getAllPermissionsWithoutPatination =  async() => {
    
    const result =  await  PostService.getPermissionWithPatination();
     return result?.result;
}
export const getAllRolesWithoutPatination =  async() => {
    
    const result =  await  PostService.getRoleWithoutPatination();
     return result?.result;
}
export const getDeleteRole =  async(id:any) => {
    
    const result =  await  PostService.DeleteRole(id);
     return result?.result;
  }

  export const getRoleById = async (id:any)=> {
    const result = await PostService.getRoleId(id);
      return result;
}

export const UpdateRole = async (body:any)=> {
    const result = await PostService.UpdateRole(body);
      return result;
}