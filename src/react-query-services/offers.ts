import {PostService} from '../services/offer'
import {Offer_Req} from '../model/offers'

export const AddPost =  async(body:Offer_Req) => {
          console.log(body);
         const result =  await  PostService.AddServices(body);
          console.log(body , result);
          return result;
}
export const getPostWithPatination =  async(Keyword:any , skipcount:any , id:any) => {
    
    const result =  await  PostService.getServicesWithPatination(Keyword ,skipcount ,id);
     return result?.result;
}

export const getServicesById = async (id:any)=> {
    const result = await PostService.getOffersById(id);
      return result;
}

export const UpdateServices = async (body:any)=> {
    const result = await PostService.UpdateOffers(body);
      return result;
}