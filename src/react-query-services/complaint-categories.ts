import {categoryService} from '../services/complaint-categories'
import {complaintCategories_Req} from '../model/complaint-categories'

export const complaintCategories =  async(body:complaintCategories_Req) => {
          console.log(body);
         const result =  await  categoryService.AddcomplaintCategories(body);
          console.log(body , result);
          return result.data;
}
export const getPostCategoriesWithOutPatination =  async() => {
    
         const result =  await  categoryService.getPostCategoriesWithOutPatination();
          return result?.result.items;
}
export const getPostCategoriesWithPatination =  async(Keyword:any , skipcount:any) => {
    
         const result =  await  categoryService.getcomplaintCategoriesWithPatination(Keyword ,skipcount);
          return result?.result;
}
export const getDeleteCategories =  async(id:any) => {
    
    const result =  await  categoryService.DeletecomplaintCategories(id);
     return result?.result;
  }