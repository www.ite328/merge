import {PostService} from '../services/Posts'
import {Post_Req} from '../model/posts'

export const AddPost =  async(body:Post_Req) => {
          console.log(body);
         const result =  await  PostService.AddPost(body);
          console.log(body , result);
          return result?.result?.items;
}
export const getPostWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  PostService.getPostWithPatination(Keyword ,skipcount);
     return result?.result;
}

export const getPostById = async (id:any)=> {
    const result = await PostService.getPostById(id);
      return result;
}

export const UpdatePost = async (body:any)=> {
    const result = await PostService.UpdatePost(body);
      return result;
}
export const getDeletePost =  async(id:any) => {
    
    const result =  await  PostService.DeletePostCategories(id);
     return result?.result;
  }