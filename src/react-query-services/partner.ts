import {categoryService} from '../services/Partner'
import {Parent_Partner_Req} from '../model/partner'

export const AddParentPartner =  async(body:Parent_Partner_Req) => {
         const result =  await  categoryService.AddPostCategories(body);        
}
export const GetPartnerById =  async(id:any) => {
         const result =  await  categoryService.GetPartnerById(id);    
         return result;    
}

export const getPartnerWithPatination =  async(Keyword:any , skipcount:any) => {
    
    const result =  await  categoryService.getPartnerWithPatination(Keyword ,skipcount);
     return result?.result;
}
export const getPartnerWithoutPatination =  async() => {
    
    const result =  await  categoryService.getPartnerWithoutPatination();
     return result?.result;
}

export const UpdatePartner = async (body:any)=> {
     const result = await categoryService.UpdatePartner(body);
       return result;
 }

export const getDeleteCategories =  async(id:any) => {
    
     const result =  await  categoryService.DeletePostCategories(id);
     return result?.result;

  }
  export const getPostById = async (id:any)=> {
       
     const result = await categoryService.GetPartnerById(id);
       console.log(result.result, 'result.result');
       return result?.result;
 }

